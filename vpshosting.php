<?php
 
use WHMCS\ClientArea;
use WHMCS\Database\Capsule;

define('CLIENTAREA', true);
define('FORCESSL', true);

require __DIR__ . '/init.php';
 
$ca = new ClientArea();
 
$ca->setPageTitle('VPS');
 
$ca->addToBreadCrumb('index.php', Lang::trans('globalsystemname'));
$ca->addToBreadCrumb('vpshosting.php', 'VPS');
 
$ca->initPage();
 
$ca->requireLogin();
 
// vps product
// set API vars
$command = "getclientsproducts";
$adminuser = "ilyasdeckers";
$values["clientid"] = $_SESSION['uid'];

// pid
//compute
$values["pid"] = '51';
$results1 = localAPI($command,$values,$adminuser);
$values["pid"] = '52';
$results2 = localAPI($command,$values,$adminuser);
$values["pid"] = '53';
$results3 = localAPI($command,$values,$adminuser);
$values["pid"] = '55';
$results4 = localAPI($command,$values,$adminuser);
$values["pid"] = '54';
$results5 = localAPI($command,$values,$adminuser);
//windows
$values["pid"] = '79';
$results6 = localAPI($command,$values,$adminuser);
$values["pid"] = '82';
$results7 = localAPI($command,$values,$adminuser);
$values["pid"] = '85';
$results8 = localAPI($command,$values,$adminuser);
$values["pid"] = '88';
$results9 = localAPI($command,$values,$adminuser);
//one-click
$values["pid"] = '73';
$results10 = localAPI($command,$values,$adminuser);
$values["pid"] = '76';
$results11 = localAPI($command,$values,$adminuser);
$values["pid"] = '91';
$results12 = localAPI($command,$values,$adminuser);
$values["pid"] = '94';
$results13 = localAPI($command,$values,$adminuser);
//storage
$values["pid"] = '100';
$results14 = localAPI($command,$values,$adminuser);
$values["pid"] = '103';
$results15 = localAPI($command,$values,$adminuser);
$values["pid"] = '106';
$results16 = localAPI($command,$values,$adminuser);
$values["pid"] = '112';
$results17 = localAPI($command,$values,$adminuser);
$values["pid"] = '109';
$results18 = localAPI($command,$values,$adminuser);

$results = array_merge_recursive($results1,$results2,$results3,$results4,$results5,$results6,$results7,$results8,$results9,$results10,$results11,$results12,$results13,$results14,$results15,$results16,$results17,$results18);

$ca->assign('webHosting', $results);

// Define the template filename

$ca->setTemplate('vpshosting');
 
$ca->output();