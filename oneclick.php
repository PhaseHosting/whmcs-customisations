<?php
 
use WHMCS\ClientArea;
use WHMCS\Database\Capsule;

define('CLIENTAREA', true);
define('FORCESSL', true);

require __DIR__ . '/init.php';
 
$ca = new ClientArea();
 
$ca->setPageTitle('One-Click Apps');
 
$ca->addToBreadCrumb('index.php', Lang::trans('globalsystemname'));
$ca->addToBreadCrumb('oneclick.php', 'One-Click Apps');
 
$ca->initPage();
 
//$ca->requireLogin();
 

// Define the template filename

$ca->setTemplate('oneclick');
 
$ca->output();