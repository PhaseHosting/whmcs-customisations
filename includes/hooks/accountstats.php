<?php
use GuzzleHttp;
use Exception;

if (!defined("WHMCS"))
die("This file cannot be accessed directly");

	function formatBytes($bytes)
    {
        $bytes = number_format($bytes / 1048576, 0, ',', ' ');

        return $bytes;
	}

	function error(){
		return array(
	        'accountstats' => array(
	            'cpu' => 'error',
	            'amem' => 'error',
	            'mmem' => 'error',
	        ),
		);
	}

	function getLveStats($service) 
	{
		$cp_user = $service['service']['username'];
		$client = new GuzzleHttp\Client();
		$url = 'http://admin.phasehosting.be/api/LVEinfo/' . $cp_user;

		try {

			$response = $client->get($url);

		} catch (Exception $e) {
			return error();
		}

		$data = $response->getBody()->getContents();
		$data = json_decode($data);

		if ($data->cpanelresult->data == null || $data->cpanelresult->event->result == 0) {
			return error();
		}

		$data = $data->cpanelresult->data[0];
		// Bytes to MB
		$maxMem = formatBytes($data->lPMem);
		$aMem = formatBytes($data->aMem);
		// RAM %
		$pmem = number_format($data->aMem/$data->lPMem*100, 0);

		return array(
	        'accountstats' => array(
	            'cpu' => $data->acpu,
	            'amem' => $aMem,
	            'mmem' => $maxMem,
	            'pmem' => $pmem,
	        ),
    	);
	}

add_hook("ClientAreaProductDetailsOutput",1,"getLveStats");
