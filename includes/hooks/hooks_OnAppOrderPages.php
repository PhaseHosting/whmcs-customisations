<?php

/**********************************************************************
 *  OnApp 1.0 (2013-08-30)
 * *
 *
 *  CREATED BY MODULESGARDEN       ->        http://modulesgarden.com
 *  CONTACT                        ->       contact@modulesgarden.com
 *
 *
 *
 *
 * This software is furnished under a license and may be used and copied
 * only  in  accordance  with  the  terms  of such  license and with the
 * inclusion of the above copyright notice.  This software  or any other
 * copies thereof may not be provided or otherwise made available to any
 * other person.  No title to and  ownership of the  software is  hereby
 * transferred.
 *
 *
 **********************************************************************/

 
/**
 * @author Mariusz Miodowski <mariusz@modulesgarden.com>
 */



function OnAppOrderPages_ClientAreaPage($vars)
{
    global $orderform, $orderfrm, $CONFIG;
    
    if($CONFIG['OrderFormTemplate'] == 'OnAppOnePage' && $vars['filename'] == 'cart')
    {
        if(!isset($_REQUEST['ajax']) && !empty($_SERVER['QUERY_STRING']))
        {   
            if(!isset($_GET['gid']) && !in_array($_GET['a'], array('add')))
            {
                ob_clean();
                header("Location: cart.php");
                die();
            }
        }
    }
    
    
    $onapp_templates = array
    (
        'OnAppOnePage',
        'OnAppStepCart'
    );

    if(!isset($orderform) || !isset($orderfrm))
    {
        return;
    }

    if(isset($_REQUEST['custom_template']) && in_array($_REQUEST['custom_template'], $onapp_templates))
    {
        $orderfrm->setTemplate($_REQUEST['custom_template']);
    }
    
    
    
}

add_hook('ClientAreaPage', 1, 'OnAppOrderPages_ClientAreaPage');