<?php
//test 
use WHMCS\ClientArea;
use WHMCS\Database\Capsule;

define('CLIENTAREA', true);
define('FORCESSL', true);

require __DIR__ . '/init.php';
 
$ca = new ClientArea();
 
$ca->setPageTitle('Your Websites');
 
$ca->addToBreadCrumb('index.php', Lang::trans('globalsystemname'));
$ca->addToBreadCrumb('webhosting.php', 'Web Hosting');
 
$ca->initPage();
 
$ca->requireLogin();
 
// webhosting product
// set API vars
$command = "getclientsproducts";
$adminuser = "ilyasdeckers";
$values["clientid"] = $_SESSION['uid'];

// web hosting
$values["pid"] = '2';
$results1 = localAPI($command,$values,$adminuser);
$values["pid"] = '5';
$results2 = localAPI($command,$values,$adminuser);
$values["pid"] = '6';
$results3 = localAPI($command,$values,$adminuser);
$values["pid"] = '7';
$results4 = localAPI($command,$values,$adminuser);

//wordpress
$values["pid"] = '121';
$results5 = localAPI($command,$values,$adminuser);
$values["pid"] = '122';
$results6 = localAPI($command,$values,$adminuser);
$values["pid"] = '123';
$results7 = localAPI($command,$values,$adminuser);
$values["pid"] = '125';
$results8 = localAPI($command,$values,$adminuser);

//Ghost
$values["pid"] = '126';
$results9 = localAPI($command,$values,$adminuser);
$values["pid"] = '131';
$results10 = localAPI($command,$values,$adminuser);
$values["pid"] = '132';
$results11 = localAPI($command,$values,$adminuser);


$results = array_merge_recursive($results1,$results2,$results3,$results4,$results5,$results6,$results7,$results8,$results9,$results10,$results11);

$ca->assign('webHosting', $results);

// Define the template filename

$ca->setTemplate('webhosting');
 
$ca->output();
