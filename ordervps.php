<?php
 
use WHMCS\ClientArea;
use WHMCS\Database\Capsule;

define('CLIENTAREA', true);
define('FORCESSL', true);

require __DIR__ . '/init.php';
 
$ca = new ClientArea();
 
$ca->setPageTitle('Order VPS');
 
$ca->addToBreadCrumb('index.php', Lang::trans('globalsystemname'));
$ca->addToBreadCrumb('ordervps.php', 'Order VPS');
 
$ca->initPage();
 
$ca->requireLogin();
 
// vps product
 $command = "getproducts";
 $adminuser = "API_USER";
 $values["gid"] = 4;
 
 $results = localAPI($command,$values,$adminuser);

 $ca->assign('productsvps', $results);
 
  $command = "getproducts";
 $adminuser = "API_USER";
 $values["gid"] = 16;
 
 $results = localAPI($command,$values,$adminuser);

 $ca->assign('productsvpswindows', $results);
// Define the template filename

$ca->setTemplate('ordervps');
 
$ca->output();