<?php
 
use WHMCS\ClientArea;
use WHMCS\Database\Capsule;

define('CLIENTAREA', true);
define('FORCESSL', true);

require __DIR__ . '/init.php';
 
$ca = new ClientArea();
 
$ca->setPageTitle('Your Websites');
 
$ca->addToBreadCrumb('index.php', Lang::trans('globalsystemname'));
$ca->addToBreadCrumb('wordpresshosting.php', 'Web Hosting');
 
$ca->initPage();
 
$ca->requireLogin();

// webhosting product
// set API vars
$command = "getclientsproducts";
$adminuser = "ilyasdeckers";
$values["clientid"] = $_SESSION['uid'];

// pid
//wordpress dedicated
$values["pid"] = '121';
$results1 = localAPI($command,$values,$adminuser);
$values["pid"] = '122';
$results2 = localAPI($command,$values,$adminuser);
$values["pid"] = '123';
$results3 = localAPI($command,$values,$adminuser);
$values["pid"] = '125';
$results4 = localAPI($command,$values,$adminuser);

//Test array
$values["pid"] = array("121","122","123");
$results5 = localAPI($command,$values,$adminuser);
$ca->assign('test', $results5);

//UI vars Dedicated
$resultsDedicated = array_merge_recursive($results1,$results2,$results3,$results4);
// UI vars Shared
//$wordpressShared = array_merge_recursive($results5,$results6,$results7,$results8);
//Assign menu
$hostname = array_merge_recursive($results1,$results2,$results3,$results4);

$ca->assign('hostname', $hostname);
//$ca->assign('wordpressShared', $wordpressShared);
$ca->assign('wordpressDedicated', $wordpressDedicated);

// Define the template filename

$ca->setTemplate('wordpresshosting');
 
$ca->output();
