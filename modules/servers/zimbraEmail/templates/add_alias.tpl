{**********************************************************************
 *  Group Pay addon. Custom developed. (2012-10-01)
 * *
 *
 *  CREATED BY MODULESGARDEN       ->        http://modulesgarden.com
 *  CONTACT                        ->       contact@modulesgarden.com
 *
 *
 *
 *
 * This software is furnished under a license and may be used and copied
 * only  in  accordance  with  the  terms  of such  license and with the
 * inclusion of the above copyright notice.  This software  or any other
 * copies thereof may not be provided or otherwise made available to any
 * other person.  No title to and  ownership of the  software is  hereby
 * transferred.
 *
 *
 **********************************************************************}

<div class="aliases_container">
	<form action="" method="post" id="add_alias_form" >
		<div id="add_alias_area" class="aliases_area">
			<table id="add_alias_table" class="aliases_table" cellspacing="0" cellpadding="0">
				<tr><td><strong>{$lang.aliases.createNewAlias}</strong><span> ({$lang.aliases.required})</span><strong></td></tr>
				<tr><td><input type="text" name="alias_name" />@{$domain}</td></tr>
				<tr>
					<td>
                                                <input type="radio" name="email_address" value="existing" checked>
						{$lang.aliases.mailbox}
						<select name="mailbox" id="add_alias_emails_select">
							{foreach from=$mailboxes item=m}  
								<option>{$m}</option>
							{/foreach}
						</select> 
                                                <!--<button class="btn btn-success" id="add_mailbox">{$lang.aliases.button_add}</button>-->
					</td>
				</tr>
                                <tr><td><input type="radio" name="email_address" value="external">External Address: <input type="text" name="external_mail" /></td></tr>
			</table>
		</div>
                <div id="mailboxes_list"></div>
		<input type="submit" id="add_alias_submit" class="btn" value="{$lang.module.save}" /> 
		<a href="{$link}&page=aliases">{$lang.aliases.cancel}</a>
	</form>    
</div>

<script type="text/javascript">
    //var LANG_NO_MORE_OPTIONS = '{$lang.aliases.no_more_options}';
    //var id = 0;
    {literal}
        
        $(document).ready(function(){
                /*$(document).delegate('[id^=mailboxess] a', 'click', function() {
                    var option = $(this).parent().parent().find("input").val(); 
                    $("#add_alias_emails_select").append("<option>"+option+"</option>");
                    $("#nmo").remove();
                    $(this).parents('div[id^=mailboxess]').first().remove();
                });
                */
                $('#add_alias_form').submit(function(){
                        if ($('input[name=alias_name]').val().length == 0)
                                return false;
                        return true;
                });
                /*
                $('#add_mailbox').click(function(){
                    var select = $("#add_alias_emails_select");
                    if($("option:selected", select).text() == LANG_NO_MORE_OPTIONS) return;
                    var value = $("option:selected", select).val();
                    $("option:selected", select).remove();
                    if($("option", select).length == 0) {
                        select.append('<option id="nmo">'+LANG_NO_MORE_OPTIONS+'</option>');
                    }
                    $("#mailboxes_list").prepend('<div id="mailboxess['+(id)+']"><input type="hidden" name="mailboxes['+(id)+']" value="'+value+'"><h4>'+value+' <a class="rem_mailboxes" style="cursor: pointer;">--</a></h4></div>');
                    
                });*/
        });
    {/literal}
</script>