{**********************************************************************
 *  Group Pay addon. Custom developed. (2012-10-01)
 * *
 *
 *  CREATED BY MODULESGARDEN       ->        http://modulesgarden.com
 *  CONTACT                        ->       contact@modulesgarden.com
 *
 *
 *
 *
 * This software is furnished under a license and may be used and copied
 * only  in  accordance  with  the  terms  of such  license and with the
 * inclusion of the above copyright notice.  This software  or any other
 * copies thereof may not be provided or otherwise made available to any
 * other person.  No title to and  ownership of the  software is  hereby
 * transferred.
 *
 *
 **********************************************************************}

<div class="module-header">
    <i class="icon-header icon-emailsforwarders"></i>
    <h1>{$lang.aliases.pagetitle}</h1>
    <p>{$lang.aliases.pagedesc}</p>
</div>
<div class="module-body">
    <div class="section">
        <h4>{$lang.aliases.createNewAlias}</h4>
        <div class="well">
            <form class="form-horizontal" id="alias-form" method="post" action="{$aliases_link}">
                <div class="form-group">
                    <label class="col-sm-3 control-label">{$lang.aliases.alias}</label>
                    <div class="input-group col-sm-6">
                        <input name="alias_name" class="form-control first" type="text" data-error="{$lang.field_required}" required>
                        <span class="input-group-addon">@</span>
                        <input class="form-control last" type="text" value="{$domain}" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">{$lang.aliases.mailbox}</label>
                    <div class="col-sm-9">
                        <div class="row">
                            <div class="col-sm-6">
                                <select name="mailbox" class="form-control" id="add_alias_emails_select">
                                    {foreach from=$mailboxes item=m}  
                                            <option>{$m}</option>
                                    {foreachelse}
                                            <option>{$lang.mailboxes.noMailboxes}</option>
                                    {/foreach}
                                </select>
                            </div>
                        </div>  
                    </div>
                </div>
                <div class="form-actions">
                    <a id="add_alias_submit" class="btn btn-success" data-act="" data-formid="alias-form" data-refresh {if !$mailboxes}disabled{/if}>{$lang.aliases.button_add}</a>
                </div>
            </form>
        </div>
    </div>

    <div class="section">
        <div class="table-header">
            <div class="header-title">
                <h4>{$lang.aliases.aliases}</h4>
            </div>
            <div class="header-actions">
                <div class="header-search">
                    <div class="input-icon">
                        <i class="fa fa-search"></i>
                        <input class="form-control" type="text" placeholder="{$lang.mailboxes.search}" data-search="alias-table">
                    </div>
                </div>

            </div>
        </div>
            <table class="table" id="alias-table" >
                <thead>
                    <tr>
                        <th>{$lang.aliases.account}</th>
                        <th>{$lang.aliases.alias}</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {if empty($aliases)}
                        <tr><td colspan="6" style="text-align: center; color: gray;">{$lang.aliases.no_aliases}</td></tr>
                    {else}
                        {foreach from=$aliases item="alias"}
                            <tr>
                                <td data-label="{$lang.aliases.account}" class="cell-sm-12">{$alias.name}</td>
                                <td data-label="{$lang.aliases.alias}" class="cell-sm-12">{$alias.alias}</td>
                                <td data-label="" class="cell-sm-12"><a class="alias-del" alias="{$alias.alias}" data-toggle="modal" data-target="#myModal" data-modal-insert="[name=alias]:{$alias.alias}|[name=m_title]:{$lang.aliases.modal_alias_delete_title}|[name=m_message]:{$lang.aliases.modal_alias_delete_message}"><i class="fa fa-trash" style="color: #8A8E99; font-size: 16px;"></i></a></td>
                            </tr>
                        {/foreach}
                    {/if}
                </tbody>
            </table>
    </div>
</div>
                    
<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">{$lang.modal_close}</span></button>
                <h4 class="modal-title"><span name="m_title"></span></h4>
            </div>
            <div class="modal-body">
                <p name="m_message"></p>
                <input type="hidden" value="" name="alias">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{$lang.modal_close}</button>
                <button type="button" class="btn btn-danger" data-act="" data-refresh>{$lang.modal_confirm}</button>
            </div>
        </div>
    </div>
</div>                  

{literal}
    <script type="text/javascript">
        $('.checkAllAliases').on('change', function() {
            $(this).parents('table').first().find(':checkbox:not(:disabled)').prop('checked', $(this).is(':checked'));
        });
        
        $(document).ready(function(){
            $("#add_alias_submit").click(function(){
                $("#alias-form").validator("validate");
            });
        });
    </script>
{/literal}