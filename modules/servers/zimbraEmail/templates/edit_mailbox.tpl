{**********************************************************************
 *  Group Pay addon. Custom developed. (2012-10-01)
 * *
 *
 *  CREATED BY MODULESGARDEN       ->        http://modulesgarden.com
 *  CONTACT                        ->       contact@modulesgarden.com
 *
 *
 *
 *
 * This software is furnished under a license and may be used and copied
 * only  in  accordance  with  the  terms  of such  license and with the
 * inclusion of the above copyright notice.  This software  or any other
 * copies thereof may not be provided or otherwise made available to any
 * other person.  No title to and  ownership of the  software is  hereby
 * transferred.
 *
 *
 **********************************************************************}

<div class="module-header">
    <i class="icon-header icon-emails"></i>
    <h1>{$lang.mailboxes.pagetitle}</h1>
    <p>{$lang.mailboxes.pagedesc}</p>
</div>
<div id="content">
    <div class="section">
        <h4 style="font-size: 16px; margin-bottom: 15px;">{$lang.mailboxes.edit_account}</h4> 
        <div class="well"> 
            <div class="row">
                <div id="edit_mailbox_bookmarks_view" class="col-md-12">
                    <div class="section">
                        <form class="form-horizontal" id="edit_mailbox_form" method="post" action="">
                            <input type="hidden" value="action_edit_mailbox" name="do" />
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{$lang.mailboxes.firstName}</label>
                                <div class="col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input name="firstname" id="firstname" class="form-control" type="text" value="{$mailbox.firstname}">
                                        </div>
                                    </div>    
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{$lang.mailboxes.lastName}</label>
                                <div class="col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input name="lastname" id="lastname" class="form-control" type="text" value="{$mailbox.lastname}">
                                        </div>
                                    </div>    
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{$lang.mailboxes.username}</label>
                                <div class="input-group col-sm-6">
                                    <input name="username" class="form-control first" type="text" disabled="disabled"
                                           data-error="{$lang.field_required}" required value="{$mailbox.mailbox}">
                                    <span class="input-group-addon">@</span>
                                    <input class="form-control last" type="text" value="{$domain}" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{$lang.mailboxes.status}</label>
                                <div class="col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <select name="status" class="form-control">
                                                <option value="active" {if $mailbox.status == "active" }selected{/if}>{$lang.mailboxes.active}</option>
                                                <option value="locked" {if $mailbox.status == "locked" }selected{/if}>{$lang.mailboxes.locked}</option>
                                                <option value="maintenance" {if $mailbox.status == "maintenance" }selected{/if}>{$lang.mailboxes.maintenance}</option>
                                                <option value="closed" {if $mailbox.status == "closed" }selected{/if}>{$lang.mailboxes.closed}</option>
                                                <option value="lockout" {if $mailbox.status == "lockout" }selected{/if}>{$lang.mailboxes.lockout}</option>
                                                <option value="pending" {if $mailbox.status == "pending" }selected{/if}>{$lang.mailboxes.pending}</option>
                                            </select>
                                        </div>
                                    </div>    
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{$lang.mailboxes.password}</label>
                                <div class="col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input name="password" id="password" class="form-control" type="password" 
                                                   data-minlength="8" >
                                            <div class="help-block">{$lang.characters_min_8}</div>
                                        </div>
                                        <div class="col-sm-6">

                                        </div>
                                    </div>    
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">{$lang.mailboxes.password_again}</label>
                                <div class="col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input name="password2" id="password2" class="form-control" type="password" 
                                                   data-match="#password" data-error="{$lang.field_required}" data-match-error="{$lang.field_not_match}" placeholder="Confirm">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                        <div class="col-sm-6">

                                        </div>
                                    </div>  
                                </div>
                            </div>
                            <div class="form-group" style="margin-bottom: 30px;">
                                <label class="col-sm-3 control-label"></label>
                                <div class="col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-6">

                                        </div>
                                    </div>    
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{$lang.mailboxes.company}</label>
                                <div class="col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input name="company" id="company" class="form-control" type="text" value="{$mailbox.company}">
                                        </div>
                                    </div>    
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{$lang.mailboxes.title}</label>
                                <div class="col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input name="title" id="title" class="form-control" type="text" value="{$mailbox.title}">
                                        </div>
                                    </div>    
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{$lang.mailboxes.phone}</label>
                                <div class="col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input name="phone" id="phone" class="form-control" type="text" value="{$mailbox.phone}">
                                        </div>
                                    </div>    
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{$lang.mailboxes.homePhone}</label>
                                <div class="col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input name="homePhone" id="homePhone" class="form-control" type="text" value="{$mailbox.homePhone}">
                                        </div>
                                    </div>    
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{$lang.mailboxes.mobilePhone}</label>
                                <div class="col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input name="mobile" id="mobile" class="form-control" type="text" value="{$mailbox.mobile}">
                                        </div>
                                    </div>    
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{$lang.mailboxes.fax}</label>
                                <div class="col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input name="fax" id="fax" class="form-control" type="text" value="{$mailbox.fax}">
                                        </div>
                                    </div>    
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{$lang.mailboxes.pager}</label>
                                <div class="col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input name="pager" id="pager" class="form-control" type="text"  value="{$mailbox.pager}">
                                        </div>
                                    </div>    
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{$lang.mailboxes.country}</label>
                                <div class="col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input name="country" id="country" class="form-control" type="text" value="{$mailbox.country}">
                                        </div>
                                    </div>    
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{$lang.mailboxes.state}</label>
                                <div class="col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input name="st" id="st" class="form-control" type="text" value="{$mailbox.st}">
                                        </div>
                                    </div>    
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{$lang.mailboxes.city}</label>
                                <div class="col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input name="city" id="city" class="form-control" type="text" value="{$mailbox.city}">
                                        </div>
                                    </div>    
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{$lang.mailboxes.street}</label>
                                <div class="col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input name="street" id="street" class="form-control" type="text" value="{$mailbox.street}">
                                        </div>
                                    </div>    
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{$lang.mailboxes.postalCode}</label>
                                <div class="col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input name="postalCode" id="postalCode" class="form-control" type="text" value="{$mailbox.postalCode}">
                                        </div>
                                    </div>    
                                </div>
                            </div>                 
                            <input type="hidden" name="mailbox_username" value="{$mailbox.mailbox}" />
                            <div class="form-actions">
                                <a id="edit_mailbox_submit" class="btn btn-success" data-act="" data-formid="edit_mailbox_form" data-refresh>{$lang.module.save}</a> 
                                <a href="{$link}&page=mailboxes" id="hide_new_form" class="btn btn-default">{$lang.mailboxes.cancel}</a>
                            </div>
                        </form>  
                    </div>
                </div>    
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    {literal} 
        $(document).ready(function() { 
            $('#edit_mailbox_submit').click(function(){
                var pass = $('input[name=password]').val();
                var pass_ = $('input[name=confirm]').val();
                if (pass.length && (pass != pass_ || pass.length < 8)){
                    alert('{/literal}{$lang.mailboxes.errPasswords|addslashes}{literal}');
                    return false;
                }
                return true;
            });
        });
    {/literal}
</script>