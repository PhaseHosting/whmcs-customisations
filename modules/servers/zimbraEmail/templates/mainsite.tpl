{**********************************************************************
 *  Group Pay addon. Custom developed. (2012-10-01)
 * *
 *
 *  CREATED BY MODULESGARDEN       ->        http://modulesgarden.com
 *  CONTACT                        ->       contact@modulesgarden.com
 *
 *
 *
 *
 * This software is furnished under a license and may be used and copied
 * only  in  accordance  with  the  terms  of such  license and with the
 * inclusion of the above copyright notice.  This software  or any other
 * copies thereof may not be provided or otherwise made available to any
 * other person.  No title to and  ownership of the  software is  hereby
 * transferred.
 *
 *
 **********************************************************************}
<input type="hidden" id="product_id" value="{$_params.serviceid}" /> 

<!-- <link href="{$_assets_dir}css/bootstrap.min.css" rel="stylesheet"> -->
<!-- <link href="{$_assets_dir}css/font-awesome.min.css" rel="stylesheet"> -->
<!--- ################ -->
<!-- <link href="{$_assets_dir}css/layout.css" rel="stylesheet"> -->
<!-- <link href="{$_assets_dir}css/theme.css" rel="stylesheet"> -->

{literal}
    <script type="text/javascript">
        var bootstrap_src = "{/literal}{$_assets_dir}{literal}js/bootstrap.min.js";
        if (!window.jQuery) { 
            document.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js" type="text/javascript"><\/script>'); 
            document.write('<script src="' + bootstrap_src +'" type="text/javascript"><\/script>'); 
        } else {
            var sp = jQuery.fn.jquery.split(" ")[0].split(".");
            if(sp[0]<2 && sp[1]<9 || 1==sp[0] && 9==sp[1] && sp[2]<1)
                document.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js" type="text/javascript"><\/script>'); 
            jQuery(document).ready(function () {
                if(!jQuery.fn.modal)
                    jQuery('#mg-wrapper').prepend('<script src="' + bootstrap_src +'" type="text/javascript"><\/script>'); 
            });
        }
    </script>


{/literal}

<script src="{$_assets_dir}js/validator.js" type="text/javascript"></script>
<script src="{$_assets_dir}js/app.js" type="text/javascript"></script>
<div class="page-content" style="margin-top: 21px;">
<div class="col-lg-12 col-md-12">
    <div class="profil-content">
<!--<div class="module-main-header">
    <h2 {if $whmcs6}style="line-height: 40px;"{/if}>
    <button onclick="goBack()" type="button" class="btn btn-sm btn-icon btn-rounded btn-primary" rel="popover" ><i class="fa fa-arrow-left"></i></button>
    <!-- {$product_details.product_group} - {$product_details.product_name} {if !empty($product_details.domain)}{$product_details.domain}{/if} --></h2>
<!--</div>-->
<div id="" class="">
    <div class="" {if $hidebar}style="margin-left: 0px!important;"{/if}>

        <div id="mg-error-container">
            {if $page_errors}
                <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button>
                    {foreach from=$page_errors item=error}
                    <p>{$error}</p>
                    {/foreach}
                </div>
            {/if}

            {if $infos}
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button>
                {foreach from=$infos item=info}
                <p>{$info}</p>
                {/foreach}
            </div>
            {/if}

        </div>
        <div id="">
        	<div class="col-lg-12 col-md-12">
        		<br>
      			<div class="profil-content">
			        <div class="col-md-2">
			        	<h2 {if $whmcs6}style="line-height: 40px;"{/if}></h2>
			        	<button onclick="location.href='email_accounts.php';" type="button" class="btn btn-sm btn-icon btn-rounded btn-primary" rel="popover" ><i class="fa fa-arrow-left"></i></button>
			        </div>
			        
		        	<div class="col-md-8">

			            {include file=$pagefile}
			        </div>
		        </div>
		    </div>
		</div>


    </div>
    <div class="row-fluid"> 
        <div id="mg-modal">
        
        </div>
    </div>
    </div>
    </div>
    </div>

</div>