{assign var=i value=1}
<div class="module-header">
    <i class="icon-header icon-distributionlist"></i>
    <h1>{$lang.distribution_lists.pagetitle}</h1>
    <p>{$lang.distribution_lists.pagedesc}</p>
</div>
<div id="new_form">
    <div class="section">
        <h4>{$lang.distribution_lists.edit_list}</h4>
        <br />
        <div class="alert alert-warning" id="emptyListInfo" display="none">
            {$lang.distribution_lists.emptyListInfo}
        </div>
        <ul class="nav nav-tabs">
          <li role="navigation" active-block="1" id="members" class="active"><a>{$lang.distribution_lists.members}</a></li>
          <li role="navigation" active-block="1" id="properties"><a>{$lang.distribution_lists.properties}</a></li>
          <li role="navigation" active-block="1" id="aliases"><a>{$lang.distribution_lists.aliases}</a></li>
          <li role="navigation" active-block="1" id="owners"><a>{$lang.distribution_lists.owners}</a></li>
          <li role="navigation" active-block="1" id="preferences"><a>{$lang.distribution_lists.preferences}</a></li>
        </ul>
        <div class="well">
            <form class="form-horizontal" id="distributionlist-form" method="post" action="{$dist_list_edit_link}">
                <input type="hidden" value="action_add_distribution_list" name="do" />
                
                <div id="section_members" hideable="1">
                    
                    <div class="form-group">
                        <label class="col-sm-3 control-label">{$lang.distribution_lists.email_address}</label>
                        <div class="input-group col-sm-6">
                            <input name="email_address" class="form-control first" type="text" 
                                   data-error="{$lang.field_required}" value="{$list_name}" disabled>
                            <span class="input-group-addon">@</span>
                            <input class="form-control last" type="text" value="{$domain}" disabled>
                        </div>
                    </div>
                        
                    <div class="form-group">
                        <label class="col-sm-3 control-label">{$lang.distribution_lists.display_name}</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-sm-6">
                                    <input name="display_name" id="display_name" class="form-control" type="text" value="{$list.displayName}">
                                </div>
                            </div>    
                        </div>
                    </div>
                        
                    <div class="form-group">
                        <label class="col-sm-3 control-label">{$lang.distribution_lists.description}</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-sm-6">
                                    <textarea name="description" class="form-control" rows="3">{$list.description}</textarea>
                                </div>
                            </div>    
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">   
                            <div class="col-sm-7">
                                <h5 style="font-weight: bold; margin-bottom: 4px;">{$lang.distribution_lists.existing_acc}</h5>
                            </div>
                            <div class="col-sm-5">
                                <h5 style="font-weight: bold; margin-bottom: 4px;">{$lang.distribution_lists.members_list}</h5>
                            </div>
                        </div>
                        <div class="row">
                            <ul class="ms-list col-sm-5" tabindex="-1" id="member_list_from">
                                {foreach from=$mailbox_from item=mailbox}
                                <li class="ms-elem-selectable">
                                    {$mailbox}
                                </li>
                                {/foreach}
                            </ul>
                            <div class="col-sm-2"><img src="{$_assets_dir}img/arrows.png" style="margin-top: 80px; margin-left: 40px;"></img></div>

                            <ul class="ms-list col-sm-5" tabindex="-1" id="member_list_to">
                                {foreach from=$mailbox_to item=mailbox}
                                <li class="ms-elem-selectable" input-id="{$i}">
                                    {$mailbox}
                                </li>
                                <input type="hidden" name="member[{$i}]" value="{$mailbox}" input-id="{$i++}">
                                {/foreach}
                                {foreach from=$mailbox_to_external item=mailbox}
                                <li class="ms-elem-selectable" custom='1' input-id="{$i}">
                                    {$mailbox}
                                </li>
                                <input type="hidden" name="member[{$i}]" value="{$mailbox}" input-id="{$i++}">
                                {/foreach}
                            </ul>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-7"></div>
                        <div class="col-sm-5">
                            <div id="member_container" style="display: inline;">
                                <input type="text" id="new_member_name" msg-error="{$lang.valid_email}" class="form-control" style="width: 200px; margin-right: 10px; display: inline;">   
                                <button id="add_new_member" class="btn btn-success">{$lang.distribution_lists.add_member}</button>
                            </div>
                            <div id="val-error-block"></div>
                        </div>
                    </div>    
                </div>
                        
                <div id="section_properties" style="display:none;" hideable="1">
                    
                    <div class="form-group">
                        <label class="col-sm-3 control-label">{$lang.distribution_lists.can_receive_mail}</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="checkbox">
                                        <label><input name="can_receive_mail" id="can_receive_mail" type="checkbox" {if $list.zimbraMailStatus == 'enabled'}checked{/if}></label>
                                    </div>
                                </div>
                            </div>    
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-3 control-label">{$lang.distribution_lists.hide_in_GAL}</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="checkbox">
                                        <label><input name="hide_in_GAL" id="hide_in_GAL" type="checkbox" {if $list.zimbraHideInGal == 'TRUE'}checked{/if}></label>
                                    </div>
                                </div>
                            </div>    
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-3 control-label">{$lang.distribution_lists.dynamic_group}</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="checkbox">
                                        <label><input name="dynamic_group" id="dynamic_group" type="checkbox" {if $list.dynamic}checked{/if}></label>
                                    </div>
                                </div>
                            </div>    
                        </div>
                    </div>                                        
                        
                    <div class="form-group">
                        <label class="col-sm-3 control-label">{$lang.distribution_lists.subscription_requests}</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-sm-6">
                                    <select name="subscription_requests" class="form-control">
                                        <option value="ACCEPT" {if $list.zimbraDistributionListSubscriptionPolicy == "ACCEPT"}selected{/if}>{$lang.distribution_lists.subscription_requests_opt1}</option>
                                        <option value="APPROVAL" {if $list.zimbraDistributionListSubscriptionPolicy == "APPROVAL"}selected{/if}>{$lang.distribution_lists.subscription_requests_opt2}</option>
                                        <option value="REJECT" {if $list.zimbraDistributionListSubscriptionPolicy == "REJECT"}selected{/if}>{$lang.distribution_lists.subscription_requests_opt3}</option>
                                    </select>
                                </div>
                            </div>    
                        </div>
                    </div>
                                        
                        
                    <div class="form-group">
                        <label class="col-sm-3 control-label">{$lang.distribution_lists.unsubscription_requests}</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-sm-6">
                                    <select name="unsubscription_requests" class="form-control">
                                        <option value="ACCEPT" {if $list.zimbraDistributionListUnsubscriptionPolicy == "ACCEPT"}selected{/if}>{$lang.distribution_lists.unsubscription_requests_opt1}</option>
                                        <option value="APPROVAL" {if $list.zimbraDistributionListUnsubscriptionPolicy == "APPROVAL"}selected{/if}>{$lang.distribution_lists.unsubscription_requests_opt2}</option>
                                        <option value="REJECT" {if $list.zimbraDistributionListUnsubscriptionPolicy == "REJECT"}selected{/if}>{$lang.distribution_lists.unsubscription_requests_opt3}</option>
                                    </select>
                                </div>
                            </div>    
                        </div>
                    </div>
                                        
             
                    
                    <div class="form-group">
                        <label class="col-sm-3 control-label">{$lang.distribution_lists.notify_about_shares}</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="checkbox">
                                        <label><input name="notify_about_shares" id="hide_in_GAL" type="checkbox" {if $list.zimbraDistributionListSendShareMessageToNewMembers == "TRUE"}checked{/if}>{$lang.distribution_lists.notify_about_shares_desc}</label>
                                    </div>
                                </div>
                            </div>    
                        </div>
                    </div>
                </div>   
                <div id="section_aliases" style="display:none;" hideable="1">
                    <h5 style="font-weight: bold; margin-bottom: 10px;">{$lang.distribution_lists.add_aliases}</h5>
                    <div class="row">
                        <div class="col-sm-4" style="margin-bottom: 16px;">
                            <input name="aliases[0]" class="form-control zimbra-email-val" type="text" value="{$first_alias}" msg-error="{$lang.valid_email}">
                            <div class="zimbra-errorbox"></div>
                        </div>
                        <div class="col-sm-2">
                            <a class="btn btn-success" id="add-alias"><i class="fa fa-plus pointer"></i> {$lang.distribution_lists.iadd}</a>
                        </div>
                    </div>
                    {foreach from=$list.zimbraMailAlias item=alias}
                        <div class="row" style="margin-bottom: 16px;">
                            <div class="col-sm-4">
                                <input name="aliases[{$i++}]" class="form-control zimbra-email-val" type="text" value="{$alias}" msg-error="{$lang.valid_email}">
                                <div class="zimbra-errorbox"></div>
                            </div>
                            <div class="col-sm-1">
                                <a class="btn btn-danger rem-row"><i class="fa fa-remove pointer"></i> {$lang.distribution_lists.iremove}</a>
                            </div>
                        </div>  
                    {/foreach}
                </div>   
                <div id="section_owners" style="display:none;" hideable="1">
                    <h5 style="font-weight: bold; margin-bottom: 10px;">{$lang.distribution_lists.add_owners}</h5>
                    <div class="row">
                        <div class="col-sm-4" style="margin-bottom: 16px;">
                            <input name="owners[0]" class="form-control zimbra-email-val" type="text" value="{$first_owner.NAME}" msg-error="{$lang.valid_email}">
                            <div class="zimbra-errorbox"></div>
                        </div>
                        <div class="col-sm-2">
                            <a class="btn btn-success" id="add-owner"><i class="fa fa-plus pointer"></i> {$lang.distribution_lists.iadd}</a>
                        </div>
                    </div>
                    {foreach from=$list.owners item=owner}
                        <div class="row" style="margin-bottom: 16px;">
                            <div class="col-sm-4">
                                <input name="owners[{$i++}]" class="form-control zimbra-email-val" type="text" value="{$owner.NAME}" msg-error="{$lang.valid_email}">
                                <div class="zimbra-errorbox"></div>
                            </div>
                            <div class="col-sm-1">
                                <a class="btn btn-danger rem-row"><i class="fa fa-remove pointer"></i> {$lang.distribution_lists.iremove}</a>
                            </div>
                        </div>  
                    {/foreach}
                </div>   
                <div id="section_preferences" style="display:none;" hideable="1">
                    
                    <div class="form-group">
                        <label class="col-sm-3 control-label">{$lang.distribution_lists.reply_to_field}</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="checkbox">
                                        <label><input name="set_reply_to_field" id="set_reply_to_field" type="checkbox" {if $list.zimbraPrefReplyToEnabled == "TRUE"}checked{/if}></label>
                                    </div>
                                </div>
                            </div>    
                        </div>
                    </div>
                        
                    <div class="form-group">
                        <label class="col-sm-3 control-label">{$lang.distribution_lists.reply_to_display_name}</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-sm-6">
                                    <input name="reply_to_display_name" id="reply_to_display_name" class="form-control" type="text" value="{$list.zimbraPrefReplyToDisplay}">
                                </div>
                            </div>    
                        </div>
                    </div>
                        
                    <div class="form-group">
                        <label class="col-sm-3 control-label">{$lang.distribution_lists.reply_to_address}</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-sm-6">
                                    <input name="reply_to_address" id="reply_to_address" class="form-control" type="text" value="{$list.zimbraPrefReplyToAddress}">
                                </div>
                            </div>    
                        </div>
                    </div>
                        
                </div>
                <div class="form-actions">
                    <input type="submit" name="edit_dist_list" id="edit_distribution_list_submit" class="btn btn-success" value="{$lang.distribution_lists.save}" /> 
                    <a id="hide_new_form" class="btn btn-default" onclick="window.location='{$distribution_lists_link}'; return false;">{$lang.distribution_lists.cancel}</a>
                </div>
            </form>
        </div>
    </div>
                
</div>
       
{literal}
    <script type="text/javascript">
            var member_counter = {/literal}{$i}{literal};
            var general_counter = {/literal}{$i}{literal};
            
            function validateEmail(email) {
                var regexp = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
                return regexp.test(email);
            }
            
            function getHtmlForItem(item) {
                var html = '<div class="row" style="margin-bottom: 16px;">' +
                    '<div class="col-sm-4">' +
                        '<input name="'+item+'['+(general_counter++)+']" class="form-control zimbra-email-val" type="text" msg-error="{/literal}{$lang.valid_email}{literal}">' +
                        '<div class="zimbra-errorbox"></div>' +
                    '</div>' +
                    '<div class="col-sm-1">' +
                        '<a class="btn btn-danger rem-row"><i class="fa fa-remove pointer"></i> {/literal}{$lang.distribution_lists.iremove}{literal}</a>' +
                    '</div>' +
                '</div>';
                return html;
            }
            
            $('*[active-block="1"]').click(function(){
                $('*[hideable="1"]').css("display", "none");
                $('*[active-block="1"]').removeClass("active");
                var a = $(this).attr("id");
                $("#section_" + a).css("display", "block");
                $("#" + a).addClass("active");
            });
            
            $(document).delegate( "#member_list_from li", "click", function() {
                var val = $(this).text();
                $(this).attr("input-id", member_counter);
                $(this).appendTo("#member_list_to");
                $("#member_list_to").append("<input type='hidden' name='member["+member_counter+"]' value='"+val.trim()+"' input-id='"+(member_counter++)+"'>");
                manageEmptyListInfo();
            });
            
            $(document).delegate( "#member_list_to li", "click", function() {
                var custom = $(this).attr("custom");
                var val = $(this).attr("input-id");
                $('input[input-id="'+val+'"]').remove();
                if(custom === '1') {
                    $(this).remove();
                } else {
                    $(this).removeAttr("input-id");
                    $(this).appendTo("#member_list_from");
                }
                manageEmptyListInfo();
            });
            
            $("#add_new_member").click(function(){
                $("#val-error-block").empty();
                var member = $("#new_member_name").val();
                if(validateEmail(member)) {
                    $("#member_list_to").append("<li class='ms-elem-selectable' custom='1' input-id='"+member_counter+"'>"+member+"</li>");
                    $("#member_list_to").append("<input type='hidden' name='member["+member_counter+"]' value='"+member+"' input-id='"+(member_counter++)+"'>");
                    $("#new_member_name").val("");
                    manageEmptyListInfo();
                } else {
                    var data_error = $("#new_member_name").attr("msg-error");
                    $("#val-error-block").append("<p style='color: red'>"+data_error+"</p>");
                }
                return false;
            });
            
            $("#add_distribution_list_submit").click(function(){
                $("#distributionlist-form").validator("validate");
            });
            
            $("#add-alias").click(function(){
                var html = getHtmlForItem('aliases');
                $(this).closest("div.row").parent().append(html);
                return false;
            });
            
            $("#add-owner").click(function(){
                var html = getHtmlForItem('owners');
                $(this).closest("div.row").parent().append(html);
                return false;
            });
            
            $(document).delegate(".rem-row", "click", function(){
                $(this).closest("div.row").remove();
                return false;
            });
            
            $(document).delegate('.zimbra-email-val', 'keyup', function(){
                $(this).next().empty();
                var member = $(this).val();
                if(!validateEmail(member)) {
                    var data_error = $(this).attr("msg-error");
                    $(this).next().append("<p style='color: red'>"+data_error+"</p>");
                }
                return false;
            });
            
            function manageEmptyListInfo()
            {   console.log('sdfasdf');
                if($('#member_list_to').children().length > 0){
                    $('#emptyListInfo').css("display", "none");
                }else{
                    $('#emptyListInfo').css("display", "");
                }
            }
      
            $(manageEmptyListInfo());
    </script>
{/literal}