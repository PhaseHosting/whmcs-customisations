{**********************************************************************
 *  Group Pay addon. Custom developed. (2012-10-01)
 * *
 *
 *  CREATED BY MODULESGARDEN       ->        http://modulesgarden.com
 *  CONTACT                        ->       contact@modulesgarden.com
 *
 *
 *
 *
 * This software is furnished under a license and may be used and copied
 * only  in  accordance  with  the  terms  of such  license and with the
 * inclusion of the above copyright notice.  This software  or any other
 * copies thereof may not be provided or otherwise made available to any
 * other person.  No title to and  ownership of the  software is  hereby
 * transferred.
 *
 *
 **********************************************************************}

<div class="module-header">
    <i class="icon-header icon-distributionlist"></i>
    <h1>{$lang.distribution_lists.pagetitle}</h1>
    <p>{$lang.distribution_lists.pagedesc}</p>
</div>
<div id="content">
    <div class="section">
        <p>
            <a id="show_new_form" class="btn btn-success">{$lang.distribution_lists.add}</a>
        </p>
    </div>
    <div class="section">
        <div class="table-header">
            <div class="header-title">
                <h4>{$lang.distribution_lists.distribution_lists}</h4>
            </div>
            <div class="header-actions">
                <div class="header-search">
                    <div class="input-icon">
                        <i class="fa fa-search"></i>
                        <input class="form-control" type="text" placeholder="{$lang.distribution_lists.search}" data-search="distlist-table">
                    </div>
                </div>

            </div>
        </div>
        <form action="{$distribution_lists_link}" method="post" id="manage_distribution_lists_form">
            <table class="table" id="distlist-table" >
                <thead>
                    <tr>
                        <th>{$lang.distribution_lists.email_address}</th>
                        <th>{$lang.distribution_lists.display_name}</th>
                        <th>{$lang.distribution_lists.status}</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {if empty($distribution_lists)}
                        <tr><td colspan="6" style="text-align: center; color: gray;">{$lang.distribution_lists.no_lists}</td></tr>
                    {else}
                        {foreach from=$distribution_lists item="distribution_list"}
                            <tr>
                                <td data-label="{$lang.distribution_lists.email_address}" class="cell-sm-12">{$distribution_list.name}</td>
                                <td data-label="{$lang.distribution_lists.display_name}" class="cell-sm-12">{$distribution_list.display_name}</td>
                                <td data-label="{$lang.distribution_lists.status}" class="cell-sm-12">{$distribution_list.status}</td>
                                <td data-label="{$lang.distribution_lists.edit}" class="cell-sm-12 cell-actions">
                                    <div class="dropdown">
                                        <a href="#" class="btn btn-icon" data-toggle="dropdown"><i class="fa fa-cogs"></i><span class="caret"></span></a>
                                        <ul class="dropdown-menu dropdown-right">
                                            <li><a href="clientarea.php?action=productdetails&id={$serviceid}&modop=custom&a=management&page=distribution_list_edit&list={$distribution_list.ID}"><i class="fa fa-pencil"></i>{$lang.distribution_lists.edit}</a></li>
                                            <li><a href="#" data-toggle="modal" data-target="#myModal" data-modal-insert="[name=list_to_delete]:{$distribution_list.ID}|[name=m_title]:{$lang.distribution_lists.modal_distribution_list_delete_title}|[name=m_message]:{$lang.distribution_lists.modal_distribution_list_delete_message}"><i class="fa fa-trash"></i>{$lang.distribution_lists.delete}</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        {/foreach}
                    {/if}
                </tbody>
            </table>
        </form>
    </div>
</div>

<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">{$lang.modal_close}</span></button>
                <h4 class="modal-title"><span name="m_title"></span></h4>
            </div>
            <div class="modal-body">
                <p name="m_message"></p>
                <input type="hidden" value="" name="list_to_delete">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{$lang.modal_close}</button>
                <button type="button" class="btn btn-danger" data-act="" data-refresh>{$lang.modal_confirm}</button>
            </div>
        </div>
    </div>
</div>   
        
<div id="new_form" style="display:none;">
    <div class="section">
        <h4>{$lang.distribution_lists.add}</h4>
        <br />
        <div class="alert alert-warning" id="emptyListInfo" display="none">
            {$lang.distribution_lists.emptyListInfo}
        </div>
        <ul class="nav nav-tabs">
          <li role="navigation" active-block="1" id="members" class="active"><a>{$lang.distribution_lists.members}</a></li>
          <li role="navigation" active-block="1" id="properties"><a>{$lang.distribution_lists.properties}</a></li>
          <li role="navigation" active-block="1" id="aliases"><a>{$lang.distribution_lists.aliases}</a></li>
          <li role="navigation" active-block="1" id="owners"><a>{$lang.distribution_lists.owners}</a></li>
          <li role="navigation" active-block="1" id="preferences"><a>{$lang.distribution_lists.preferences}</a></li>
        </ul>
        <div class="well">
            <form class="form-horizontal" id="distributionlist-form" method="post" action="{$mailboxes_link}">
                <input type="hidden" value="action_add_distribution_list" name="do" />
                
                <div id="section_members" hideable="1">
                    
                    <div class="form-group">
                        <label class="col-sm-3 control-label">{$lang.distribution_lists.email_address}</label>
                        <div class="input-group col-sm-6">
                            <input name="email_address" class="form-control first" id="zimbra-list-val" type="text" 
                                   data-error="{$lang.valid_email_minreq}" required>
                            <span class="input-group-addon">@</span>
                            <input class="form-control last" type="text" value="{$domain}" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3"></div>
                        <div class="input-group col-sm-6">
                            <div id="zimbra-list-error"></div>
                        </div>
                    </div>
                        
                    <div class="form-group">
                        <label class="col-sm-3 control-label">{$lang.distribution_lists.display_name}</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-sm-6">
                                    <input name="display_name" id="display_name" class="form-control" type="text">
                                </div>
                            </div>    
                        </div>
                    </div>
                        
                    <div class="form-group">
                        <label class="col-sm-3 control-label">{$lang.distribution_lists.description}</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-sm-6">
                                    <textarea name="description" class="form-control" rows="3"></textarea>
                                </div>
                            </div>    
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">   
                            <div class="col-sm-7">
                                <h5 style="font-weight: bold; margin-bottom: 4px;">{$lang.distribution_lists.existing_acc}</h5>
                            </div>
                            <div class="col-sm-5">
                                <h5 style="font-weight: bold; margin-bottom: 4px;">{$lang.distribution_lists.members_list}</h5>
                            </div>
                        </div>
                        <div class="row">
                            <ul class="ms-list col-sm-5" tabindex="-1" id="member_list_from">
                                {foreach from=$mailboxes item=mailbox}
                                <li class="ms-elem-selectable">
                                    {$mailbox.name}
                                </li>
                                {/foreach}
                            </ul>
                            <div class="col-sm-2"><img src="{$_assets_dir}img/arrows.png" style="margin-top: 80px; margin-left: 40px;"></img></div>

                            <ul class="ms-list col-sm-5" tabindex="-1" id="member_list_to">
                            </ul>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-7"></div>
                        <div class="col-sm-5">
                            <div id="member_container" style="display: inline;">
                                <input type="text" id="new_member_name" msg-error="{$lang.valid_email}" class="form-control" style="width: 200px; margin-right: 10px; display: inline;">   
                                <button id="add_new_member" class="btn btn-success">{$lang.distribution_lists.add_member}</button>
                            </div>
                            <div id="val-error-block"></div>
                        </div>
                    </div>    
                </div>
                        
                <div id="section_properties" style="display:none;" hideable="1">
                    
                    <div class="form-group">
                        <label class="col-sm-3 control-label">{$lang.distribution_lists.can_receive_mail}</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="checkbox">
                                        <label><input name="can_receive_mail" id="can_receive_mail" type="checkbox"></label>
                                    </div>
                                </div>
                            </div>    
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-3 control-label">{$lang.distribution_lists.hide_in_GAL}</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="checkbox">
                                        <label><input name="hide_in_GAL" id="hide_in_GAL" type="checkbox"></label>
                                    </div>
                                </div>
                            </div>    
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-3 control-label">{$lang.distribution_lists.dynamic_group}</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="checkbox">
                                        <label><input name="dynamic_group" id="dynamic_group" type="checkbox"></label>
                                    </div>
                                </div>
                            </div>    
                        </div>
                    </div>                                        
                        
                    <div class="form-group">
                        <label class="col-sm-3 control-label">{$lang.distribution_lists.subscription_requests}</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-sm-6">
                                    <select name="subscription_requests" class="form-control">
                                        <option value="ACCEPT">{$lang.distribution_lists.subscription_requests_opt1}</option>
                                        <option value="APPROVAL">{$lang.distribution_lists.subscription_requests_opt2}</option>
                                        <option value="REJECT">{$lang.distribution_lists.subscription_requests_opt3}</option>
                                    </select>
                                </div>
                            </div>    
                        </div>
                    </div>
                                        
                        
                    <div class="form-group">
                        <label class="col-sm-3 control-label">{$lang.distribution_lists.unsubscription_requests}</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-sm-6">
                                    <select name="unsubscription_requests" class="form-control">
                                        <option value="ACCEPT">{$lang.distribution_lists.unsubscription_requests_opt1}</option>
                                        <option value="APPROVAL">{$lang.distribution_lists.unsubscription_requests_opt2}</option>
                                        <option value="REJECT">{$lang.distribution_lists.unsubscription_requests_opt3}</option>
                                    </select>
                                </div>
                            </div>    
                        </div>
                    </div>
                                        
             
                    
                    <div class="form-group">
                        <label class="col-sm-3 control-label">{$lang.distribution_lists.notify_about_shares}</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="checkbox">
                                        <label><input name="notify_about_shares" id="hide_in_GAL" type="checkbox">{$lang.distribution_lists.notify_about_shares_desc}</label>
                                    </div>
                                </div>
                            </div>    
                        </div>
                    </div>
                </div>   
                <div id="section_aliases" style="display:none;" hideable="1">
                    <h5 style="font-weight: bold; margin-bottom: 10px;">{$lang.distribution_lists.add_aliases}</h5>
                    <div class="row">
                        <div class="col-sm-4" style="margin-bottom: 16px;">
                            <input name="aliases[0]" class="form-control zimbra-email-val" type="text" msg-error="{$lang.valid_email}">
                            <div class="zimbra-errorbox"></div>
                        </div>
                        <div class="col-sm-2">
                            <a class="btn btn-success" id="add-alias"><i class="fa fa-plus pointer"></i> Add</a>
                        </div>
                    </div>
                </div>   
                <div id="section_owners" style="display:none;" hideable="1">
                    <h5 style="font-weight: bold; margin-bottom: 10px;">{$lang.distribution_lists.add_owners}</h5>
                    <div class="row">
                        <div class="col-sm-4" style="margin-bottom: 16px;">
                            <input name="owners[0]" class="form-control zimbra-email-val" type="text" msg-error="{$lang.valid_email}">
                            <div class="zimbra-errorbox"></div>
                        </div>
                        <div class="col-sm-2">
                            <a class="btn btn-success" id="add-owner"><i class="fa fa-plus pointer"></i> Add</a>
                        </div>
                    </div>
                </div>   
                <div id="section_preferences" style="display:none;" hideable="1">
                    
                    <div class="form-group">
                        <label class="col-sm-3 control-label">{$lang.distribution_lists.reply_to_field}</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="checkbox">
                                        <label><input name="set_reply_to_field" id="dynamic_group" type="checkbox"></label>
                                    </div>
                                </div>
                            </div>    
                        </div>
                    </div>
                        
                    <div class="form-group">
                        <label class="col-sm-3 control-label">{$lang.distribution_lists.reply_to_display_name}</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-sm-6">
                                    <input name="reply_to_display_name" id="reply_to_display_name" class="form-control" type="text">
                                </div>
                            </div>    
                        </div>
                    </div>
                        
                    <div class="form-group">
                        <label class="col-sm-3 control-label">{$lang.distribution_lists.reply_to_address}</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-sm-6">
                                    <input name="reply_to_address" id="reply_to_address" class="form-control" type="text">
                                </div>
                            </div>    
                        </div>
                    </div>
                        
                </div>
                <div class="form-actions">
                    <a name="add_dist_list" id="add_distribution_list_submit" class="btn btn-success" data-query="add_dist_list=1" data-act="" data-formid="distributionlist-form" data-refresh>{$lang.distribution_lists.save}</a> 
                    <a id="hide_new_form" class="btn btn-default">{$lang.distribution_lists.cancel}</a>
                </div>
            </form>
        </div>
    </div>
                
</div>
                
                
                    
                    

{literal}
    <script type="text/javascript">
            var member_counter = 0;
            var general_counter = 1;
            
            function validateEmail(email) {
                var regexp = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
                return regexp.test(email);
            }
            
            function validateListName(name) {
                var regexp =/^([\w-]+(?:\.[\w-]+)*){3,}$/i;
                return regexp.test(name);
            }
            
            function getHtmlForItem(item) {
                var html = '<div class="row" style="margin-bottom: 16px;">' +
                    '<div class="col-sm-4">' +
                        '<input name="'+item+'['+(general_counter++)+']" class="form-control zimbra-email-val" type="text" msg-error="{/literal}{$lang.valid_email}{literal}">' +
                        '<div class="zimbra-errorbox"></div>' +
                    '</div>' +
                    '<div class="col-sm-1">' +
                        '<a class="btn btn-danger rem-row"><i class="fa fa-remove pointer"></i> Remove</a>' +
                    '</div>' +
                '</div>';
                return html;
            }
            
            $("#show_new_form").click(function(){
                $("#content").css("display", "none");
                $("#new_form").css("display", "block");
            });
            $("#hide_new_form").click(function(){
                $("#content").css("display", "block");
                $("#new_form").css("display", "none");
                $(this).closest('form').find("input[type=text]:enabled").val("");
                $(this).closest('form').find("input[type=password]").val("");
                $(this).closest('form').find("textarea").val("");
                $(this).closest('form').find("input[type=checkbox]").attr("checked", false);
                $(this).closest('form').find("select").get(0).selectedIndex = 0;
            });
            
            $('*[active-block="1"]').click(function(){
                $('*[hideable="1"]').css("display", "none");
                $('*[active-block="1"]').removeClass("active");
                var a = $(this).attr("id");
                $("#section_" + a).css("display", "block");
                $("#" + a).addClass("active");
            });
            
            $(document).delegate( "#member_list_from li", "click", function() {
                var val = $(this).text();
                $(this).attr("input-id", member_counter);
                $(this).appendTo("#member_list_to");
                $("#member_list_to").append("<input type='hidden' name='member["+member_counter+"]' value='"+val.trim()+"' input-id='"+(member_counter++)+"'>");
                manageEmptyListInfo();
            });
            
            $(document).delegate( "#member_list_to li", "click", function() {
                var custom = $(this).attr("custom");
                var val = $(this).attr("input-id");
                $('input[input-id="'+val+'"]').remove();
                if(custom === '1') {
                    $(this).remove();
                } else {
                    $(this).removeAttr("input-id");
                    $(this).appendTo("#member_list_from");
                }
                manageEmptyListInfo();
            });
            
            $("#add_new_member").click(function(){
                $("#val-error-block").empty();
                var member = $("#new_member_name").val();
                if(validateEmail(member)) {
                    $("#member_list_to").append("<li class='ms-elem-selectable' custom='1' input-id='"+member_counter+"'>"+member+"</li>");
                    $("#member_list_to").append("<input type='hidden' name='member["+member_counter+"]' value='"+member+"' input-id='"+(member_counter++)+"'>");
                    $("#new_member_name").val("");
                    manageEmptyListInfo();
                } else {
                    var data_error = $("#new_member_name").attr("msg-error");
                    $("#val-error-block").append("<p style='color: red'>"+data_error+"</p>");
                }
                return false;
            });
            
            $("#add_distribution_list_submit").click(function(){
                $("#distributionlist-form").validator("validate");
            });
            
            $("#add-alias").click(function(){
                var html = getHtmlForItem('aliases');
                $(this).closest("div.row").parent().append(html);
                return false;
            });
            
            $("#add-owner").click(function(){
                var html = getHtmlForItem('owners');
                $(this).closest("div.row").parent().append(html);
                return false;
            });
            
            $(document).delegate(".rem-row", "click", function(){
                $(this).closest("div.row").remove();
                return false;
            });
            
            $(document).delegate('.zimbra-email-val', 'keyup', function(){
                $(this).next().empty();
                var member = $(this).val();
                if(!validateEmail(member)) {
                    var data_error = $(this).attr("msg-error");
                    $(this).next().append("<p style='color: red'>"+data_error+"</p>");
                }
                return false;
            });
            
            $(document).delegate('#zimbra-list-val', 'keyup', function(){
                $("#zimbra-list-error").empty();
                var name = $(this).val();
                if(!validateListName(name)) {
                    var data_error = $(this).attr("data-error");
                    $("#zimbra-list-error").append("<p style='color: red'>"+data_error+"</p>");
                }
                return false;
            });
            
            function manageEmptyListInfo()
            {   console.log('asdasf');
                if($('#member_list_to').children().length > 0){
                    $('#emptyListInfo').css("display", "none");
                }else{
                    $('#emptyListInfo').css("display", "");
                }
            }
      
            $(manageEmptyListInfo());      
    </script>
{/literal}
