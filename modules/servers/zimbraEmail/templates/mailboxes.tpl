
          <div class="panel" style="margin-bottom: 100px;">
            <div class="panel-header header-line">
              <h3>All Accounts</h3>
              <div class="control-btn">
                <a data-toggle="modal" data-target="#help"><i class="icon-question"></i></a> 
                </div>
            </div>

          <div class="panel-content">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>{$lang.mailboxes.mailbox}</th>
                    <th>{$lang.mailboxes.quota}</th>
                    <th>{$lang.mailboxes.status}</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {if empty($mailboxes)}
                <tr><td colspan="6" style="text-align: center; color: gray;">{$lang.mailboxes.noMailboxes}</td></tr>
                {else}
                {assign var=mg_i value=0}
                {foreach from=$mailboxes item="acc"}
                <tr>
                    <td data-label="{$lang.mailboxes.mailbox}" class="cell-sm-12">{$acc.name}</td>
                    <td data-label="{$lang.mailboxes.quota}" class="cell-sm-12">{$acc.zimbraMailQuota}</td>
                    <td data-label="{$lang.mailboxes.status}" class="cell-sm-12">{$acc.zimbraAccountStatus}</td>
                    <td data-label="" class="cell-sm-12 cell-actions">     
                        <div class="row">
                            <div class="cell-sm-12" >
                                <div class="dropdown">
                                    <a href="#" class="btn btn-default" data-toggle="dropdown"><i class="fa fa-cogs"></i><span class="caret"></span></a>
                                    <ul class="dropdown-menu dropdown-right">
                                        <!-- <li><a href="{$link}{'&page=edit_mailbox&mailbox_name='}{$acc.name}"><i style="color: #8a8e99;" class="fa fa-edit"></i>Edit Details</a></li> -->
                                        <li><a href="https://mail.phasehosting.io" target="_blank"><i class="fa fa-desktop"></i>Webmail</a></li>
                                        <li><a href="" data-toggle="modal" data-target="#help"><i class="fa fa-envelope"></i>Configure Email client</a></li>
                                        <!-- <li><a href="email_logs.php?m={$acc.name}" ><i class="fa fa-file-o"></i>View Logs</a></li> -->
                                        <li><a href="#" data-toggle="modal" data-target="#chgPw" data-modal-insert="[name=m_title]:{$lang.mailboxes.modal_change_password}|[name=account]:{$acc.name}"><i class="fa fa-key"></i> {$lang.mailboxes.change_password}</a></li>
                                        <li><a href="#" data-toggle="modal" data-target="#myModal" data-modal-insert="[name=m_title]:{$lang.aliases.modal_mailbox_delete_title}|[name=m_message]:{$lang.aliases.modal_mailbox_delete_message}"><i class="fa fa-trash"></i>{$lang.mailboxes.button_delete}</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                {/foreach}
                {/if}
            </tbody>
        </table>
    </div>
    <div class="panel-footer clearfix" style="margin-top: 50px;">
      <div class="pull-right" data-toggle="modal" data-target="#addEmail">
        <button type="button" class="btn btn-sm btn-icon btn-rounded btn-primary" rel="popover" data-container="body" data-toggle="popover" data-placement="bottom" data-content="Create a new mailbox." data-original-title="New Mailbox" title=""><i class="fa fa-plus"></i></button>
    </div>
</div>
</div>


<!-- modals -->
<div class="modal fade" id="addEmail" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
        <h4 class="modal-title"><strong>New</strong> Email</h4>
    </div>
    <form class="form-horizontal" id="email-form" method="post" action="{$mailboxes_link}">
      <div class="modal-body">
        <input type="hidden" value="action_add_mailbox" name="do" />
        
        <div class="form-group">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-6">
                        <label class="control-label">{$lang.mailboxes.firstName}</label>
                        <input name="firstname" id="firstname" class="form-control form-white" type="text">
                    </div>
                    <div class="col-sm-6">
                        <label class="control-label">{$lang.mailboxes.lastName}</label>
                        <input name="lastname" id="lastname" class="form-control form-white" type="text">
                    </div>
                </div>    
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-12">
                <label class="control-label">{$lang.mailboxes.username}</label>
                <div class="input-group  col-sm-12">
                    <input name="username" class="form-control form-white first" type="text" data-error="{$lang.field_required}" required>
                    <span class="input-group-addon">@</span>
                    <input class="form-control form-white last" type="text" value="{$domain}" disabled>
                </div>
            </div>
        </div>

        <div class="form-group"> 
            <div class="col-sm-12">
                <label class="control-label">{$lang.mailboxes.status}</label>
                <div class="row">
                    <div class="col-sm-12">
                        <select name="status" class="form-control form-white">
                            <option value="active">{$lang.mailboxes.active}</option>
                            <option value="locked">{$lang.mailboxes.locked}</option>
                            <option value="maintenance">{$lang.mailboxes.maintenance}</option>
                            <option value="closed">{$lang.mailboxes.closed}</option>
                            <option value="lockout">{$lang.mailboxes.lockout}</option>
                            <option value="pending">{$lang.mailboxes.pending}</option>
                        </select>
                    </div>
                </div>    
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-6">
                    <label class="control-label">{$lang.mailboxes.password}</label>
                        <input name="password" id="password" class="form-control form-white" type="password" 
                        data-minlength="8" required>
                        <div class="help-block">{$lang.characters_min_8}</div>
                    </div>
                    <div class="col-sm-6">
                    <label class="control-label">{$lang.mailboxes.password_again}</label>
                        <input name="password2" id="password2" class="form-control form-white" type="password" 
                        data-match="#password" data-error="{$lang.field_required}" data-match-error="{$lang.field_not_match}" placeholder="Confirm" required>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>    
            </div>
        </div>

        <!-- <div class="form-group">
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-6">
                        <label class="control-label">{$lang.mailboxes.company}</label>
                        <input name="company" id="company" class="form-control form-white" type="text">
                    </div>
                    <div class="col-sm-6">
                        <label class="control-label">{$lang.mailboxes.title}</label>
                        <input name="title" id="title" class="form-control form-white" type="text">
                    </div>
                </div>
            </div>    
        </div>
        

        <div class="form-group">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-6">
                        <label class="control-label">{$lang.mailboxes.phone}</label>
                        <input name="phone" id="phone" class="form-control form-white" type="text">
                    </div>
                    <div class="col-sm-6">
                        <label class="control-label">{$lang.mailboxes.homePhone}</label>
                        <input name="homePhone" id="homePhone" class="form-control form-white" type="text">
                    </div>
                </div>    
            </div>
        </div>

        <div class="form-group">
            
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-6">
                        <label class="control-label">{$lang.mailboxes.mobilePhone}</label>
                        <input name="mobile" id="mobile" class="form-control form-white" type="text">
                    </div>
                    <div class="col-sm-6">
                        <label class="control-label">{$lang.mailboxes.fax}</label>
                        <input name="fax" id="fax" class="form-control form-white" type="text">
                    </div>
                </div>    
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-6">
                        <label class="control-label">{$lang.mailboxes.pager}</label>
                        <input name="pager" id="pager" class="form-control form-white" type="text">
                    </div>
                    <div class="col-sm-6">
                        <label class="control-label">{$lang.mailboxes.country}</label>
                        <input name="country" id="country" class="form-control form-white" type="text">
                    </div>
                </div>    
            </div>
        </div>

        <div class="form-group">
            
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-6">
                        <label class="control-label">{$lang.mailboxes.state}</label>
                        <input name="st" id="st" class="form-control form-white" type="text">
                    </div>
                    <div class="col-sm-6">
                        <label class="control-label">{$lang.mailboxes.city}</label>
                        <input name="city" id="city" class="form-control form-white" type="text">
                    </div>
                </div>    
            </div>
        </div>

        <div class="form-group">
            
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-6">
                        <label class="control-label">{$lang.mailboxes.street}</label>
                        <input name="street" id="street" class="form-control form-white" type="text">
                    </div>
                    <div class="col-sm-6">
                        <label class="control-label">{$lang.mailboxes.postalCode}</label>
                        <input name="postalCode" id="postalCode" class="form-control form-white" type="text">
                    </div>
                </div>    
            </div>
        </div>  -->
    </div>
    <div class="modal-footer clearfix">
      <button type="button" class="btn btn-default btn-embossed" data-dismiss="modal">Close</button>
      <button type="submit" class="btn btn-primary btn-embossed">{$lang.module.save}</button>
  </div>
</form>
</div>
</div>
</div>

<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">{$lang.modal_close}</span></button>
                <h4 class="modal-title"><span name="m_title"></span></h4>
            </div>
            <div class="modal-body">
                <p name="m_message"></p>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{$lang.modal_close}</button>
                <button type="button" class="btn btn-danger" data-act="" data-refresh data-formid="emails-table" data-query="mact=delete&page=mailbox_actions">{$lang.modal_confirm}</button>
            </div>
        </div>
    </div>
</div>   

<div class="modal fade" id="chgPw">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">{$lang.modal_close}</span></button>
                <h4 class="modal-title"><span name="m_title"></span></h4>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <p><strong>{$lang.mailboxes.please_enter_password}</strong></p>
                    <form class="form-horizontal" id="chgpw_form" method="post" action="{$mailboxes_link}">
                        <input type="hidden" name="account">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label class="control-label">{$lang.mailboxes.password}</label>
                                        <input name="newpass" id="password_ch" class="form-control form-white" type="password" 
                                        data-minlength="8" required>
                                        <div class="help-block">{$lang.characters_min_8}</div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="control-label">{$lang.mailboxes.password_again}</label>
                                        <input name="renewpass" id="password2" class="form-control form-white" type="password" 
                                        data-match="#password_ch" data-error="{$lang.field_required}" data-match-error="{$lang.field_not_match}" placeholder="Confirm" required>
                                        <div class="help-block with-errors"></div>

                                    </div>
                                </div>    
                            </div>
                        </div>
                        
                    </form>
                </div>          
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{$lang.modal_close}</button>
                <button type="button" class="btn btn-success" data-act="" data-refresh data-formid="chgpw_form" data-query="mact=chgpw&page=mailbox_actions">{$lang.modal_confirm}</button>
            </div>
        </div>
    </div>
</div>   

{literal}
<script type="text/javascript">
    $(document).ready(function(){
        $("#show_new_form").click(function(){
            $("#content").css("display", "none");
            $("#new_form").css("display", "block");
        });
        $("#hide_new_form").click(function(){
            $("#content").css("display", "block");
            $("#new_form").css("display", "none");
            $(this).closest('form').find("input[type=text]:enabled").val("");
            $(this).closest('form').find("input[type=password]").val("");
            $(this).closest('form').find("select").get(0).selectedIndex = 0;
        });
        $("#add_mailbox_submit").click(function(){
            $("#email-form").validator("validate");
        });
        $(document).delegate("#emails-table [data-toggle='modal']", 'click', function () {
            var $checkbox = $(this).parents('tr').first().find(':checkbox').first();
            $checkbox.prop('checked', true);
            $('#emails-table :checkbox').not($checkbox).prop('checked', false);
        });
    });
</script>
{/literal}

<!-- HELP -->
<div class="modal fade" id="help" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
                <h4 class="modal-title"><strong>Help</strong></h4>
            </div>
            <div class="modal-body">
            <!-- step 01 -->
                <div class="col-md-4">
                    <h4><strong>01.</strong> New Email</h4>
                </div>
                <div class="col-md-8">
                    <p>Click the &nbsp;<button type="button" class="btn btn-sm btn-icon btn-rounded btn-primary"><i class="fa fa-plus"></i></button> button to add a new mailbox to your account.</p>
                </div>
            <!-- step 02 -->
                <div class="col-md-4">
                    <h4><strong>02.</strong> Log in to Webmail</h4>
                </div>
                <div class="col-md-8">
                    <p>You can now log in to the <a href="https://mail.phasehosting.io">webmail client</a>. In order to recieve mails you will have to change your MX records.</p>
                    <div style="overflow-x: hidden;">
                    	<table class="table table-striped">
                    		<tr>
                    			<th>Priority</th>
                    			<th>Value</th>
                    		</tr>
                    		<tr>
                    			<td>0</td>
                    			<td>mx0.phasehosting.io</td>
                    		</tr>
                    		<tr>
                    			<td>10</td>
                    			<td>mx1.phasehosting.io</td>
                    		</tr>
                    	</table>
                    </div>
                </div>
            <!-- step 03 -->
                <div class="col-md-4">
                    <h4><strong>03.</strong> Connect With an Email Client</h4>
                </div>
                <div class="col-md-8">
                    <p>It is possible to connect your email accounts to a custom email client (Outlook, Tunderbird,...)</p>
                    <div style="overflow-x: hidden;">
                    <table class="table table-striped">
	                	<tr>
	                		<td><strong>Incomming Server:</strong></td>
	                		<td> mail.phasehosting.io</td>
	                	</tr>
	                	<tr>
	                		<td><strong>Incomming Ports:</strong></td>
	                		<td>IMAP: 143 or 993 <br> POP: 995</td>
	                	</tr>
	                	<tr>
	                		<td><strong>Username:</strong></td>
	                		<td>account@yourdomain.com</td>
	                	</tr>
	                	<tr>
	                		<td><strong>Password:</strong></td>
	                		<td>Your chosen password</td>
	                	</tr>
	                	<tr>
	                		<td><strong>Outgoing Server:</strong></td>
	                		<td>mail.phasehosting.io</td>
	                	</tr>
	                	<tr>
	                		<td><strong>Outgoing Ports:</strong></td>
	                		<td>SMTP: 465 or 587</td>
	                	</tr>
                	</table>
                	</div>
                    
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-embossed" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>