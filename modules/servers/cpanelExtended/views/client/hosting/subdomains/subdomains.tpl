<div class="module-body">
    <div class="section">
        <div class="panel">
            <div class="panel-header header-line">
                <h3>{$lang.main_header}</h3>
            </div>
            <div class="panel-content">
                <div class="header-search">
                    <div class="input-icon">
                        <i class="fa fa-search"></i>
                        <input class="form-control form-white" type="text" placeholder="{$lang.search_placeholder}" data-search="subdomains-table">
                    </div>
                </div>
                <table class="table" id="subdomains-table">
                    <thead>
                        <tr>
                            <th>{$lang.edit_tblhead_domain}</th>
                            <th>{$lang.edit_tblhead_root}</th>
                            <th>{$lang.edit_tblhead_redirect}</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="panel-footer clearfix text-right">
                <button type="button" class="btn btn-sm btn-icon btn-rounded btn-primary" data-toggle="modal" data-target="#addsub"><i class="fa fa-plus"></i></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addsub">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4>{$lang.add_header}</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="subdomain-form">
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.subdomain_label}</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <input class="form-control first" type="text" name="domain" maxlength="64" id="dir-input" required pattern="{$patterns.simple_domain_tld}" data-error="{$validator_lang.invalid}">
                                <span class="input-group-addon">.</span>
                                <select class="form-control last" name="rootdomain" data-no-validate>
                                    {if $domainlist|@count > 0}
                                    {foreach from=$domainlist item="s"}
                                    <option value="{$s->domain}">{$s->domain}</option>
                                    {/foreach}
                                    {else}
                                    <option value="{$domain}">{$domain}</option>
                                    {/if}
                                </select>
                            </div>
                        </div>
                        <div class="col-md-9 col-md-offset-3">
                            <div class="help-block with-errors"></div>
                        </div>                              
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.root_label}</label>
                        <div class="col-sm-9">
                            <div class="form-fluid">
                                <div class="fluid-0">
                                    <span>{$partition}/{$username}/</span>
                                </div>
                                <div class="fluid-100">
                                    <input class="form-control form-white" type="text" name="dir" id="rootdomain-input" required  data-error="{$validator_lang.field_required}">                                                        
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9 col-md-offset-3">
                            <div class="help-block with-errors"></div>
                        </div>                                
                    </div>                                      
                    <div class="form-actions">
                        <a class="btn btn-success" data-act="addsubdomain" data-formid="subdomain-form" data-rtable="subdomains-table">{$lang.add_submit_text}</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

{confirm_modal btn='data-act="removesubdomain" data-rtable="subdomains-table"' id="delete-subdomain-modal" title=$lang.delete_confirmation_title
body="`$lang.delete_confirmation_body`<input type='hidden' name='domain'/>"}

<div class="modal fade" id="manage-redirection-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">{$lang.close}</span></button>
                <h4 class="modal-title">{$lang.manage_redirection}</h4>
            </div>
            <div class="modal-body">
                <input class="subdomain" type="hidden" name="sub" value="" />
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.subdomain_label}</label>
                        <div class="col-sm-9">
                            <span class="subdomain"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.redirects_to}</label>
                        <div class="col-sm-9">
                            <input class="form-control form-white" id="pwd-modal" name="url" type="text" required pattern="{$patterns.no_white_chars}"/>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{$lang.close}</button>
                <a class="btn btn-danger" data-act="disredirectionsubdomain" data-rtable="subdomains-table" data-no-validate>{$lang.dis_redirect_sub_submit_text}</a>
                <a class="btn btn-primary" data-act="redirectionsubdomain" data-rtable="subdomains-table">{$lang.save}</a>
            </div>
        </div>
    </div>
</div>

{literal}
<script>
    (function (jQuery) {
        refreshTable('subdomains-table');

        jQuery("#dir-input").on('keyup change', function () {
            jQuery("#rootdomain-input").val('public_html/' + jQuery(this).val());
        });
    })(jQuery);
</script>
{/literal}