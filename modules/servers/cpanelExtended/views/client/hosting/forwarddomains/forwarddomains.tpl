<!-- <div class="module-header">
    <i class="icon-header icon-forwarddomains"></i>
    <h1>{$lang.main_header}</h1>
    <p>{$lang.main_hint}</p>
</div> -->
<div class="module-body">
    <div class="panel">
        <div class="panel-header header line">
            <div class="header-title">
                <h3>{$lang.main_header}</h3>
            </div>

        </div>
        <div class="panel-content">
            <div class="input-icon">
                <i class="fa fa-search"></i>
                <input class="form-control form-white" type="text" placeholder="{$lang.search_placeholder}" data-search="domain-forwarders-table">
            </div>
            <table class="table" id="domain-forwarders-table">
                <thead>
                    <tr>
                        <th>{$lang.modify_dforward_tblhead_address}</th>
                        <th>{$lang.modify_dforward_tblhead_directory}</th>
                        <th>{$lang.modify_dforward_tblhead_forward}</th>
                        <th>{$lang.modify_dforward_tblhead_type}</th>
                        <th class="text-center-sm">{$lang.modify_dforward_tblhead_matchwww}</th>
                        <th class="text-center-sm">{$lang.modify_dforward_tblhead_wildcard}</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div class="panel-footer clearfix text-right">
                <button type="button" class="btn btn-sm btn-icon btn-rounded btn-primary" data-toggle="modal" data-target="#add"><i class="fa fa-plus"></i></button>
            </div>
    </div>
</div>

<div class="modal fade" id="add">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4>{$lang.add_dforward_sub_main_header}</h4>
            </div>         
            <div class="modal-body">
                <form class="form-horizontal" id="domain-forward-form">
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.lbl_type}</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-sm-12">
                                    <select name="type" class="form-control">
                                        <option value="permanent">{$lang.lbl_pernament}</option>
                                        <option value="temp">{$lang.lbl_temporary}</option>
                                    </select>
                                </div>
                                <div class="col-sm-6">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">http://<b id="rdwww">www.</b></label>

                        <div class="col-sm-9">
                        <div class="input-group ">
                                <select class="form-control first" name="domain" data-no-validate>
                                    {if $domainlist|@count > 0}
                                    {section name=s loop=$domainlist}  
                                    <option value="{$domainlist[s]->domain}">{$domainlist[s]->domain}</option>
                                    {/section}
                                    {else}
                                    <option value="{$domain}">{$domain}</option>
                                    {/if}
                                </select>
                                <span class="input-group-addon">/</span>
                                <input class="form-control form-white last" type="text" name="src" required="" pattern="{$patterns.no_white_chars}" data-error="{$validator_lang.invalid}" >
                                </div>
                        </div>
                        <div class="col-md-9 col-md-offset-3">
                            <div class="help-block with-errors"></div>
                        </div>  
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.lbl_redirects_to}</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-sm-12">
                                    <input class="form-control form-white" type="text" name="url" required="" pattern="{$patterns.no_white_chars}"  data-error="{$validator_lang.field_required}">
                                </div>
                                
                            </div>
                        </div>
                        <div class="col-md-9 col-md-offset-3">
                            <div class="help-block with-errors"></div>
                        </div>   
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.lbl_wwwredirection}</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-sm-12">
                                    <select name="rdwww" class="form-control">
                                        <option value="2" data-copy="www.">{$lang.lbl_only_www}</option>
                                        <option value="0" data-copy="(www.)?">{$lang.lbl_without_www}</option>
                                        <option value="1" data-copy="">{$lang.lbl_not_www}</option>
                                    </select>
                                </div>
                                <div class="col-sm-6">

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.lbl_wildcard}</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="checkbox">
                                        <label><input type="checkbox" name="wildcard">&nbsp;</label>
                                    </div>
                                </div>
                                <div class="col-sm-6">

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-actions text-right">
                        <a class="btn btn-primary" data-act="addforward" data-formid="domain-forward-form" data-rtable="domain-forwarders-table">{$lang.add}</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


    {confirm_modal btn='data-act="delforward" data-rtable="domain-forwarders-table"' id="delete-domain-forward-modal" title=$lang.delete_confirmation_title
    body="`$lang.delete_confirmation_body`<input type='hidden' name='domain'/><input type='hidden' name='src'/><input type='hidden' name='docroot'/>"}

    {literal}
    <script>
        (function (jQuery) {
            refreshTable('domain-forwarders-table');
            
            jQuery('[name=rdwww]').change(function() {
                jQuery('#rdwww').html(jQuery('option:selected', this).data('copy'));
            });
        })(jQuery);
    </script>
    {/literal}