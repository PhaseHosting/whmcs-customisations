<!-- <div class="module-header">
    <i class="icon-header icon-bans"></i>
    <h1>{$lang.main_header}</h1>
    <p>{$lang.main_hint}</p>
</div> -->
<div class="module-body">
    <div class="panel">
        <div class="panel-header header-line">
            <h3>{$lang.main_header}</h3>
        </div>
        <div class="panel-content">
            <form class="form-horizontal" id="unban-form">
                <div class="form-group">
                    <label class="control-label col-sm-3">{$lang.unban_ip_address}</label>
                    <div class="col-sm-9">
                        <div class="row">
                            <div class="col-sm-12">
                                <input class="form-control form-white" type="text" name="queryip" value="{$ip}" required pattern="{$patterns.ip}">
                                <div class="help-block">{$lang.ipv4_or_ipv6}</div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer clearfix">
                <div class="form-actions text-right">
                    <a class="btn btn-primary" data-act="unban" data-formid="unban-form" data-validate="1">{$lang.unban_submit}</a>
                </div>
            </form>
        </div>
    </div>
</div>