<tbody>
    {if empty($list)}
        <tr><td colspan="3" class="text-center">{$lang.modify_dforward_tbl_results_empty}</td></tr>
    {else}
        {foreach from=$list item="domain"}
            <tr>
                <td class="cell-sm-12" data-label="{$lang.modify_dforward_tblhead_address}">{$domain.dest}</td>
                <td class="cell-sm-12" data-label="{$lang.modify_dforward_tblhead_forward}">{$domain.forward}</td>
                <td class="cell-sm-12 cell-actions" data-label="{$lang.modify_dforward_tblhead_action}">
                    <a class="btn btn-icon" data-toggle="modal" data-target="#delete-modal" data-modal-insert=".domain:{$domain.dest|urlencode}" data-rtable="email-domain-forwarders-table" title="{$lang.modify_aforward_delete_button}"><i class="fa fa-trash"></i></a>
                </td>
            </tr>
        {/foreach}
    {/if}
</tbody>