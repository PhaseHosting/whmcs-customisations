<div class="module-header">
     <i class="icon-header icon-ssh"></i>
    <h1>{$lang.main_header}</h1>
    <p>{$lang.main_hint}</p>
</div>
{if $template_six_style}    
    <div style="display: inline-flex;"><h4>{$lang.main_header}</h4>
        <button class="show-sm btn btn-icon show-hint" data-toggle="tooltip" title="{$lang.main_hint}" type="button"> 
            <i class="glyphicon glyphicon-question-sign"></i> 
        </button></div> <br /><br />{/if}
<div class="alert alert-info">
    {$lang.main_description2}
</div>

<div class="module-body">
    <div class="section">
        <h4>{$lang.create_or_import_your_keys}</h4>
        <div class="well">
            <button data-toggle="modal" data-target="#create" class="btn btn-success">{$lang.create}</button>
            <button data-toggle="modal" data-target="#import" class="btn btn-primary">{$lang.import}</button>
        </div>
    </div>

    <div class="section">
        <div class="table-header">
            <div class="header-title">
                <h4>{$lang.public_keys}</h4>
            </div>
            <div class="header-actions">
                <div class="header-search">
                    <div class="input-icon">
                        <i class="fa fa-search"></i>
                        <input class="form-control" type="text" placeholder="{$lang.search_placeholder}" data-search="ssh-public-keys-table">
                    </div>
                </div>

            </div>
        </div>
        <table class="table" id="ssh-public-keys-table">
            <thead>
                <tr>
                    <th>{$lang.name}</th>
                    <th>{$lang.authorization_status}</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <div class="alert alert-info">{$lang.keys_note}</div>
    </div>
    
    <div class="section">
        <div class="table-header">
            <div class="header-title">
                <h4>{$lang.private_keys}</h4>
            </div>
            <div class="header-actions">
                <div class="header-search">
                    <div class="input-icon">
                        <i class="fa fa-search"></i>
                        <input class="form-control" type="text" placeholder="{$lang.search_placeholder}" data-search="ssh-private-keys-table">
                    </div>
                </div>

            </div>
        </div>
        <table class="table" id="ssh-private-keys-table">
            <thead>
                <tr>
                    <th>{$lang.name}</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
         
    <div id="create" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{$lang.ssh_key_generator}</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-sm-3">{$lang.key_name}</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="text" name="name" data-error="{$validator_lang.invalid}" maxlength="48" required pattern="{$patterns.simple_filename}" data-error="{$validator_lang.invalid}">
                            </div>
                            <div class="col-md-9 col-md-offset-3">
                                <div class="help-block with-errors"></div>
                            </div>                              
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{$lang.key_password}</label>
                            <div class="col-sm-9">
                                <input id="password1" class="form-control" type="password" name="password1" data-error="{$validator_lang.field_required}" data-minlength="5" required>
                                <div class="help-block">{$validator_lang.min_5_chars}</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{$lang.key_password_again}</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="password" name="password2" data-match="#password1" data-error="{$validator_lang.field_required}" data-match-error="{$validator_lang.field_not_match}" placeholder="Confirm" required>
                                <div class="help-block"></div>
                            </div>
                            <div class="col-md-9 col-md-offset-3">
                                <div class="help-block with-errors"></div>
                            </div>                                    
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{$lang.key_type}</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="type">
                                    <option value="dsa" selected="">DSA</option>
                                    <option value="rsa">RSA</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{$lang.key_size}</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="size" >
                                    <option value="1024" selected="">1024</option>
                                    <option value="2048">2048</option>
                                    <option value="2048">4096</option>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{$lang.close}</button>
                    <button type="button" class="btn btn-primary" data-act="create_confirm" data-refresh="1">{$lang.create}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    
    <div class="modal fade" id="import">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4>{$lang.import_ssh_key}</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-sm-3">{$lang.key_name}</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="name" data-error="{$validator_lang.invalid}" maxlength="48" required pattern="{$patterns.simple_filename}">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{$lang.key_password}</label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control" name="password" data-error="{$validator_lang.field_required}" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{$lang.key}</label>
                            <div class="col-sm-9">
                                <textarea name="key" class="form-control" style="height: 150px" data-error="{$validator_lang.field_required}" required></textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal" class="btn">{$lang.close}</button>
                    <button class="btn btn-primary" data-act="import_confirm" data-refresh="1">{$lang.import}</button>
                </div>
            </div>
        </div>
    </div>     
</div>
                
{confirm_modal btn='data-act="delete" data-rtable="ssh-public-keys-table"' id="delete-public" title=$lang.delete_confirmation
        body="`$lang.delete_confirmation_text`<input type='hidden' name='key'/><input type='hidden' name='public' value='1'/>"}
{confirm_modal btn='data-act="delete" data-refresh="1"' id="delete-private" title=$lang.delete_confirmation
        body="`$lang.delete_confirmation_text`<input type='hidden' name='key'/><input type='hidden' name='public' value='0'/>"}
{confirm_modal btn='data-act="authorize" data-rtable="ssh-public-keys-table"' id="authorize-modal" title=$lang.authorize_confirmation_title
        body="`$lang.authorize_confirmation_text`<input type='hidden' name='key'/>" type="primary"}
{confirm_modal btn='data-act="deauthorize" data-rtable="ssh-public-keys-table"' id="deauthorize-modal" title=$lang.deauthorize_confirmation_title
        body="`$lang.deauthorize_confirmation_text`<input type='hidden' name='key'/>" type="primary"}

{literal}
    <script>
        function showPPK(data) {
            jQuery('#ppk_key').fadeIn().find('textarea').val(data.key);
        }
        
        (function (jQuery) {
            refreshTable('ssh-public-keys-table');
            refreshTable('ssh-private-keys-table');
        })(jQuery);
    </script>
{/literal}