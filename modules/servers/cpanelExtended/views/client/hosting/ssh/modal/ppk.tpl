<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">{$lang.close}</span></button>
                <h4>{$lang.ppk}</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <input type="hidden" name="name" value="{$name}" />
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.key_name}</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" value="{$name}"  disabled="" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.key_password}</label>
                        <div class="col-sm-9">
                            <input type="password" class="form-control" name="password" {if $pass}value="{$pass}"{/if} id="input02">
                        </div>
                    </div>
                    <div class="form-group" style="display: none;" id="ppk_key">
                        <label class="control-label col-sm-3">{$lang.key}</label>
                        <div class="col-sm-9">
                            <textarea name="key" class="form-control" style="height: 150px">{$ppk_key}</textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">                
                <button class="btn btn-default" data-dismiss="modal">{$lang.close}</button>
                <button class="btn btn-primary" data-act="ppk_confirm" data-dont-close-modal><span>{$lang.convert}</span></button>
            </div>
        </div>
    </div>
</div>