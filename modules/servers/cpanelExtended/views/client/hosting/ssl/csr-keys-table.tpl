<tbody>
    {if empty($listCsrs)}
        <tr>
            <td colspan="2" class="text-center">{$lang.no_keys_in_table}</td>
        </tr>
    {else}
        {foreach from=$listCsrs  item=domain}
            <tr>
                <td data-label="{$lang.domain}" class="cell-sm-12">
                    {$domain->host}
                </td>
                <td data-label="{$lang.action}" class="cell-sm-12 cell-actions">
                    <a class="btn btn-icon" data-toggle="modal" data-target="#delete-csr-modal" data-modal-insert="[name=host]:{$domain->host|urlencode}" title="{$lang.delete}"><i class="fa fa-trash"></i></a>
                </td>
            </tr>
        {/foreach}
    {/if}
</tbody> 
