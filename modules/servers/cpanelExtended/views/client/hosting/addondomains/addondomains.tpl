<!-- <div class="module-header">
    <i class="icon-header icon-addondomains"></i>
    <h1>{$lang.main_header}</h1>
    <p>{$lang.main_hint}</p>
</div> -->
<div class="alert alert-info">
    {$lang.add_addondomain_hint}
</div>
<div class="section">
        <div class="panel">
            <div class="panel-header header-line">
                <h3>{$lang.main_header}</h3>
            </div>
            <div class="panel-content">
                <div class="input-icon">
                    <i class="fa fa-search"></i>
                    <input class="form-control form-white form-white" type="text" placeholder="{$lang.search_placeholder}" data-search="addondomains-table">
                </div>
                <table class="table" id="addondomains-table">
                    <thead>
                        <tr>
                            <th>{$lang.modify_addondomain_tblhead_domain}</th>
                            <th>{$lang.modify_addondomain_tblhead_document}</th>
                            <th>{$lang.modify_addondomain_tblhead_username}</th>
                            <th>{$lang.modify_addondomain_tblhead_redirect}</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="panel-footer clearfix text-right">
                <button type="button" class="btn btn-sm btn-icon btn-rounded btn-primary" data-toggle="modal" data-target="#add"><i class="fa fa-plus"></i></button>
            </div>
        </div>
    </div>

    <div class="modal fade" id="add">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h4>{$lang.add_addondomain_sub_main_header}</h4>
                </div>        
                <div class="modal-body">
                        <form class="form-horizontal" id="add-addon-form">
                            <div class="form-group">
                                <label class="control-label col-sm-3">{$lang.add_addondomain_domain_label}</label>
                                <div class="col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <input id="domain-input" class="form-control form-white" type="text" name="newdomain" required pattern="{$patterns.simple_domain}" data-error="{$validator_lang.invalid}">
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="col-md-9 col-md-offset-3">
                                    <div class="help-block with-errors"></div>
                                </div>                              
                            </div>
                            <div class="form-group" >
                                <label class="control-label col-sm-3">{$lang.add_addondomain_username_label}</label>
                                <div class="col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <input id="subdomain-input" class="form-control form-white" type="text" name="subdomain" required title="{$lang.ftp_account_will_be_created_automatically}" data-error="{$validator_lang.field_required}">
                                        </div>
                                        
                                    </div>    
                                </div>
                                <div class="col-md-9 col-md-offset-3">
                                    <div class="help-block with-errors"></div>
                                </div>                                   
                            </div>
                            <div class="form-group" >
                                <label class="control-label col-sm-3">{$lang.add_addondomain_directory_label}</label>
                                <div class="col-sm-9">
                                    <div class="form-fluid">
                                        <div class="fluid-0">
                                            <span>/{$partition}/{$username}/</span>
                                        </div>
                                        <div class="fluid-100">
                                            <input id="dir-input" class="form-control form-white" type="text" name="dir" required data-error="{$validator_lang.field_required}">                                                        
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-9 col-md-offset-3">
                                    <div class="help-block with-errors"></div>
                                </div>                                   
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">{$lang.add_addondomain_password_label}</label>
                                <div class="col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <input id="password" class="form-control form-white" type="password" name="password" data-minlength="5" required >
                                            <div class="help-block">{$validator_lang.min_5_chars}</div>
                                        </div>
                                        
                                    </div>        
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">{$lang.add_addondomain_password2_label}</label>
                                <div class="col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <input id="password2" class="form-control form-white" type="password" name="password2" data-match="#password" data-error="{$validator_lang.field_required}" data-match-error="{$validator_lang.field_not_match}" required>
                                        </div>
                                        
                                    </div>        
                                </div>
                                <div class="col-md-9 col-md-offset-3">
                                    <div class="help-block with-errors"></div>
                                </div>                                   
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">{$lang.strength}</label>
                                <div class="col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-7">
                                            <div id="pwstrengthbox" class="">{$validator_lang.very_weak}</div>
                                        </div>
                                        <div class="col-sm-6 col-xs-5">
                                            <button type="button" class="btn btn-sm btn-default" data-genpwd="password,password2,pwstrengthbox"><i class="fa fa-key"></i><span class="hide-sm">{$lang.generate_password}</span></button>
                                        </div>
                                    </div>     
                                </div>
                            </div>
                            <div class="form-actions text-right">
                                <button type="button" class="btn btn-success" data-act="addaddondomain" data-formid="add-addon-form" data-rtable="addondomains-table">{$lang.create_account}</button>
                            </div>
                        </form>
                    
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="delete-addondomain-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">{$lang.close}</span></button>
                    <h4 class="modal-title">{$lang.delete_confirmation_title}</h4>
                </div>
                <div class="modal-body">
                    <input class="domain" type="hidden" name="domain" value="" />
                    <input class="subdomain" type="hidden" name="subdomain" value="" />
                    <form class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-sm-3">{$lang.add_addondomain_domain_label}</label>
                            <div class="col-sm-9">
                                <span class="domain"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3"></label>
                            <div class="col-sm-9">
                                <div class="checkbox">
                                    <label>
                                        <input class="ftp" name="ftpuser" type="checkbox" value="" checked=""/> {$lang.remove_ftp} "<span class="ftp"></span>"
                                    </label>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{$lang.close}</button>
                    <a class="btn btn-danger" data-act="deladdondomain" data-rtable="addondomains-table">{$lang.confirm}</a>
                </div>
            </div>
        </div>
    </div>  

    <div class="modal fade" id="manage-redirection-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">{$lang.close}</span></button>
                    <h4 class="modal-title">{$lang.manage_redirection}</h4>
                </div>
                <div class="modal-body">
                    <input class="sub1" type="hidden" name="sub1" value="" />
                    <input class="sub2" type="hidden" name="sub2" value="" />
                    <form class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-sm-3">{$lang.addondomain_label}</label>
                            <div class="col-sm-9">
                                <span class="sub1"></span>, <span class="sub2"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{$lang.redirects_to}</label>
                            <div class="col-sm-9">
                                <input class="form-control form-white" name="url" type="text" required pattern="{$patterns.no_white_chars}"  data-error="{$validator_lang.invalid}"/>
                            </div>
                            <div class="col-md-9 col-md-offset-3">
                                <div class="help-block with-errors"></div>
                            </div>                          
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{$lang.close}</button>
                    <a class="btn btn-danger" data-act="disredirectaddondomain" data-rtable="addondomains-table" data-no-validate>{$lang.dis_redirect_sub_submit_text}</a>
                    <a class="btn btn-primary" data-act="redirectaddondomain" data-rtable="addondomains-table">{$lang.save}</a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="change-root-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">{$lang.close}</span></button>
                    <h4 class="modal-title">{$lang.change_document_root}</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="rootdomain" value="" />
                    <input type="hidden" name="subdomain" value="" />
                    <form class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-sm-3">{$lang.document_root}</label>
                            <div class="col-sm-9">
                                <input class="form-control form-white" name="dir" type="text" required  data-error="{$validator_lang.field_required}" />
                            </div>
                            <div class="col-md-9 col-md-offset-3">
                                <div class="help-block with-errors"></div>
                            </div>                          
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{$lang.close}</button>
                    <a class="btn btn-primary" data-act="changedocroot" data-rtable="addondomains-table">{$lang.save}</a>
                </div>
            </div>
        </div>
    </div>

    {literal}
    <script>
        (function (jQuery) {
            refreshTable('addondomains-table');

            jQuery("#domain-input").on('keyup change', function () {
                var ex = jQuery(this).val().split('.');
                jQuery("#subdomain-input").val(ex[0]);
                jQuery("#dir-input").val('' + jQuery(this).val());
            });
        })(jQuery);
    </script>
    {/literal}