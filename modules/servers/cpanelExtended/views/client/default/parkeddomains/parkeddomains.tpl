<div class="module-header">
     <i class="icon-header icon-parkeddomains"></i>
    <h1>{$lang.main_header}</h1>
    <p>{$lang.main_hint}</p>
</div>
<div class="module-body">
    <div class="section">
        <div style="display: inline-flex;"><h4>{$lang.add_parkeddomain_sub_main_header}</h4>{if $template_six_style}
            <button class="show-sm btn btn-icon show-hint" data-toggle="tooltip" title="{$lang.main_hint}" type="button"> 
            <i class="glyphicon glyphicon-question-sign"></i> 
            </button>{/if}</div>         
        <div class="well">
            <form class="form-horizontal" id="parked-domain-form">
                <div class="form-group">
                    <label class="control-label col-sm-3">{$lang.add_parkeddomain_domain_label}</label>
                    <div class="col-sm-9">
                        <div class="row"> 
                            <div class="col-sm-6">
                                <input class="form-control" type="text" name="domain" required pattern="{$patterns.simple_domain}"  data-error="{$validator_lang.invalid}" >
                            </div>
                            <div class="col-sm-6">

                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 col-md-offset-3">
                        <div class="help-block with-errors"></div>
                    </div>                              
                </div>
                <div class="alert alert-info">
                    {$lang.add_parkeddomain_add_hint}
                </div>
                <div class="form-actions">
                    <a class="btn btn-success" data-act="addparkeddomain" data-validate="1" data-formid="parked-domain-form" data-rtable="parked-domains-table">{$lang.add_parkeddomain_submit_text}</a>
                </div>
            </form>
        </div>
    </div>
    <div class="section">
        <div class="table-header">
            <div class="header-title">
                <h4>{$lang.search_parkeddomain_header}</h4>
            </div>
            <div class="header-actions">
                <div class="header-search">
                    <div class="input-icon">
                        <i class="fa fa-search"></i>
                        <input class="form-control" type="text" placeholder="{$lang.search_placeholder}" data-search="parked-domains-table">
                    </div>
                </div>

            </div>
        </div>
        <table class="table" id="parked-domains-table">
            <thead>
                <tr>
                    <th>{$lang.modify_parkeddomain_tblhead_domain}</th>
                    <th>{$lang.modify_parkeddomain_tblhead_droot}</th>
                    <th>{$lang.modify_parkeddomain_tblhead_redirect}</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
                
{confirm_modal btn='data-act="delparkeddomain" data-rtable="parked-domains-table"' id="delete-parked-domain-modal" title=$lang.delete_confirmation_title
        body="`$lang.delete_confirmation_body`<input type='hidden' name='domain'/>"}

<div class="modal fade" id="manage-redirection-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">{$lang.close}</span></button>
                <h4 class="modal-title">{$lang.modify_parkeddomain_tbl_redirect_link}</h4>
            </div>
            <div class="modal-body">
                <input class="domain" type="hidden" name="domain" value="" />
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.parked_domain}</label>
                        <div class="col-sm-9">
                            <input class="form-control domain" type="text" disabled="" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.modify_parkeddomain_tblhead_redirect}</label>
                        <div class="col-sm-9">
                            <input class="form-control" id="pwd-modal" name="url" type="url" required  data-error="{$validator_lang.invalid}" />
                        </div>
                        <div class="col-md-9 col-md-offset-3">
                            <div class="help-block with-errors"></div>
                        </div>                          
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{$lang.close}</button>
                <a class="btn btn-danger" data-act="disredirectparkeddomain" data-rtable="parked-domains-table" data-no-validate>{$lang.disable_redirection}</a>
                <a class="btn btn-primary" data-act="redirectparkeddomain" data-rtable="parked-domains-table">{$lang.save}</a>
            </div>
        </div>
    </div>
</div>
                
{literal}
    <script>
        (function (jQuery) {
            refreshTable('parked-domains-table');
        })(jQuery);
    </script>
{/literal}