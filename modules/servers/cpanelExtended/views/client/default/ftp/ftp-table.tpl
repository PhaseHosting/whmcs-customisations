<tbody>
    {if empty($list)}
        <tr><td colspan="4" class="text-center">{$lang.search_results_empty}</td></tr>
    {else}
        {foreach from=$list item="acc"}
            <tr>
                <td data-label="{$lang.edit_tblhead_login}" class="cell-sm-12">{$acc.login}</td>
                <td data-label="{$lang.directory_label}" class="cell-sm-12">{$acc.dir}</td>
                <td data-label="{$lang.usage_quota}" class="cell-sm-12">{$acc.diskused}/{if $acc.diskquota eq 'unlimited'}∞{else}{$acc.diskquota}{/if}MB</td>
                <td data-label="{$lang.edit_tblhead_actions}" class="cell-sm-12 cell-actions">
                    <a class="btn btn-icon" data-toggle="modal" data-target="#change-pwd-modal" data-modal-insert=".acc:{$acc.login|urlencode}" title="{$lang.chg_password_bookmark}"><i class="fa fa-lock"></i></a>
                    <a class="btn btn-icon" data-toggle="modal" data-target="#change-quota-modal" data-modal-insert=".acc:{$acc.login|urlencode}" data-quota="{$acc.diskquota}" title="{$lang.chg_quota_bookmark}"><i class="fa fa-hdd-o"></i></a>
                    <a class="btn btn-icon" data-toggle="modal" data-target="#delete-modal" data-modal-insert=".acc:{$acc.login|urlencode}" title="{$lang.del_account_bookmark}"><i class="fa fa-trash"></i></a>
                </td>
            </tr>
        {/foreach}
    {/if}
</tbody>



