<div class="module-header">
    <i class="icon-header icon-ftp"></i>
    <h1>{$lang.main_header}</h1>
    <p>{$lang.main_hint}</p>
</div>
<div class="module-body">
    <div class="section">
        <div style="display: inline-block;"><h4>{$lang.add_header}</h4>{if $template_six_style}
            <button class="show-sm btn btn-icon show-hint" data-toggle="tooltip" title="{$lang.main_hint}" type="button"> 
            <i class="glyphicon glyphicon-question-sign"></i> 
            </button>{/if}</div>     
        <div class="well">
            <form class="form-horizontal" id="ftp-form">
                <div class="form-group">
                    <label class="control-label col-sm-3">{$lang.login_label}</label>
                    <div class="col-sm-9 input-group">
                        <input class="form-control first" type="text" name="login" data-error="{$lang.field_required}" required>
                        <span class="input-group-addon">@</span>
                        <input class="form-control last" type="text" readonly value="{$_params.domain}" data-no-validate>
                    </div>
                    <div class="col-sm-9 col-sm-offset-3">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">{$lang.password_label}</label>
                    <div class="col-sm-9">
                        <div class="row">
                            <div class="col-sm-6">
                                <input id="password" class="form-control" type="password" name="password" data-minlength="5" required>
                                <div class="help-block">{$validator_lang.min_5_chars}</div>
                            </div>
                            <div class="col-sm-6">

                            </div>
                        </div>    
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">{$lang.password2_label}</label>
                    <div class="col-sm-9">
                        <div class="row">
                            <div class="col-sm-6">
                                <input id="password2" class="form-control" type="password" name="password2" data-match="#password" data-error="{$validator_lang.field_required}" data-match-error="{$validator_lang.field_not_match}" placeholder="" required>
                                <div class="help-block with-errors" ></div>
                            </div>
                            <div class="col-sm-6">

                            </div>
                        </div>        
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">{$lang.strength}</label>
                    <div class="col-sm-9">
                        <div class="row">
                            <div class="col-xs-6">
                                <div id="pwstrengthbox" class="">{$validator_lang.very_weak}</div>
                            </div>
                            <div class="col-xs-6">
                                <button id="generate_pwd" type="button" class="btn btn-sm btn-default" data-genpwd="password,password2,pwstrengthbox"><i class="fa fa-key"></i><span class="hide-sm">{$lang.generate_password}</span></button>
                            </div>
                        </div>     
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">{$lang.directory_label}</label>
                    <div class="col-sm-9">
                        <div class="form-fluid">
                            <div class="fluid-0">
                                <span>/{$partition}/{$username}/</span>
                            </div>
                            <div class="fluid-100">
                                <input name="homedir" class="form-control" type="text" data-error="{$validator_lang.field_required}" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">{$lang.quota_label}</label>
                    <div class="col-sm-9">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="radio">
                                            <label>
                                                <div class="form-fluid">
                                                    <div class="fluid-0">
                                                        <input name="quota" type="radio" value="limited" data-no-validate> 
                                                    </div>
                                                    <div class="fluid-100">
                                                        <input class="form-control" name="quota_value" disabled="disabled" type="number" min="1" max="999999999" value="1" required data-error="{$validator_lang.invalid}">
                                                    </div>
                                                    <div class="fluid-0">
                                                        <span>MB</span>
                                                    </div>
                                                </div> 
                                            </label>
                                        </div>    
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="radio">
                                            <label>
                                                <input name="quota" type="radio" checked="" value="unlimited" data-no-validate>
                                                {$lang.quota_unlimited}
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-9 col-sm-offset-3">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="form-actions">
                    <a class="btn btn-success" data-act="addftpaccount" data-formid="ftp-form" data-rtable="ftp-table">{$lang.add_submit_text}</a>
                </div>
            </form>
        </div>
    </div>
    <div class="section">
        <div class="table-header">
            <div class="header-title">
                <h4>{$lang.search_header}</h4>
            </div>
            <div class="header-actions">
                <div class="header-search">
                    <div class="input-icon">
                        <i class="fa fa-search"></i>
                        <input class="form-control" type="text" placeholder="{$lang.search_placeholder}" data-search="ftp-table">
                    </div>
                </div>

            </div>
        </div>
        <table class="table" id="ftp-table">
            <thead>
                <tr>
                    <th>{$lang.edit_tblhead_login}</th>
                    <th>{$lang.directory_label}</th>
                    <th>{$lang.usage_quota}</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>

    <div class="modal fade" id="change-pwd-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">{$lang.close}</span></button>
                    <h4 class="modal-title">{$lang.chg_password_bookmark}</h4>
                </div>
                <div class="modal-body">
                    <input class="acc" type="hidden" name="acc" value="{$account}" />
                    <form class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-sm-3">{$lang.chg_account_label}</label>
                            <div class="col-sm-9">
                                <span class="acc" style="padding: 0px;"></span>@{$_params.domain}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{$lang.password_label}</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="pwd-modal" name="pwd" type="password" data-minlength="5" required/>
                                <div class="help-block">{$validator_lang.min_5_chars}</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{$lang.password2_label}</label>
                            <div class="col-sm-9">
                                <input class="form-control" name="pwdc" type="password" data-match="#pwd-modal" data-error="{$validator_lang.field_required}" data-match-error="{$validator_lang.field_not_match}" placeholder="" required/>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{$lang.chg_account_cancel_text}</button>
                    <button type="button" class="btn btn-primary" data-act="chpwd"><span>{$lang.save_changes}</span></button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="change-quota-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">{$lang.close}</span></button>
                    <h4 class="modal-title">{$lang.chg_quota_bookmark}</h4>
                </div>
                <div class="modal-body">
                    <input class="acc" type="hidden" name="acc" value="{$account}" />
                    <form class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-sm-3">{$lang.chg_account_label}</label>
                            <div class="col-sm-9">
                                <span class="acc" style="padding: 0px;"></span>@{$_params.domain}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{$lang.quota_label}</label>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="radio">
                                            <label>
                                                <div class="form-fluid">
                                                    <div class="fluid-0">
                                                        <input name="quota" type="radio" value="limited" data-no-validate>
                                                    </div>
                                                    <div class="fluid-100">
                                                        <input class="form-control" name="quota_value" type="number" max="999999999" min="1" required>
                                                    </div>
                                                    <div class="fluid-0">
                                                        <span class="ml-5">MB</span>
                                                    </div>
                                                </div> 
                                            </label>
                                        </div>    
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="radio">
                                            <label>
                                                <input name="quota" type="radio" checked="" value="unlimited" data-no-validate>
                                                {$lang.quota_unlimited}
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{$lang.close}</button>
                    <button type="button" class="btn btn-primary" data-act="chgftpaccountquota" data-rtable="ftp-table"><span>{$lang.save_changes}</span></button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="delete-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">{$lang.close}</span></button>
                    <h4 class="modal-title">{$lang.delete_confirmation}</h4>
                </div>
                <div class="modal-body">
                    <input class="acc" type="hidden" name="acc" value="{$account}" />
                    <p>{$lang.acc_delete_confirmation}<span class="acc"></span>@{$_params.domain}?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{$lang.close}</button>
                    <button type="button" class="btn btn-danger" data-act="deleteftpaccount" data-query="destroy=0" data-rtable="ftp-table"><span>{$lang.del_account_submit_text}</span></button>
                    <button type="button" class="btn btn-danger" data-act="deleteftpaccount" data-query="destroy=1" data-rtable="ftp-table"><span>{$lang.del_files_submit_text}</span></button>
                </div>
            </div>
        </div>
    </div>
</div>
{literal}
    <script>
        refreshTable('ftp-table');

        (function (jQuery) {
            jQuery('#ftp-form [name=login]').change(function () {
                var val = jQuery(this).val();
                jQuery('#ftp-form [name=homedir]').val(val);
            });
            
            jQuery(document).delegate('[name="quota"]', 'change', function() {
                if(jQuery(this).val() == 'unlimited'){
                    jQuery(this).parents('.form-group').first().find('[name="quota_value"]').first().attr('data-no-validate','');
                }else{
                    jQuery(this).parents('.form-group').first().find('[name="quota_value"]').first().removeAttr('data-no-validate').trigger('change');
                }
                if(jQuery(this).val() == 'unlimited'){
                    jQuery(this).parents('.form-group').first().find('[name="quota_value"]').first().attr('disabled','disabled');
                }else{
                    jQuery(this).parents('.form-group').first().find('[name="quota_value"]').first().removeAttr('disabled');
                }
            });

            jQuery(document).delegate('[data-target="#change-quota-modal"]', 'click', function () {
                var quota = jQuery(this).data('quota');
                if (quota == 'unlimited') {
                    jQuery('#change-quota-modal input[value=limited]').prop('checked', false);
                    jQuery('#change-quota-modal input[value=unlimited]').prop('checked', true);
                    jQuery('#change-quota-modal input[name=quota_value]').val(1).attr('data-no-validate', '');
                    jQuery('#change-quota-modal input[name=quota_value]').attr('disabled','disabled');                    
                } else {
                    jQuery('#change-quota-modal input[value=unlimited]').prop('checked', false);
                    jQuery('#change-quota-modal input[value=limited]').prop('checked', true);
                    jQuery('#change-quota-modal input[name=quota_value]').val(quota).removeAttr('data-no-validate');
                }
            });
        })(jQuery);
    </script>
{/literal}