<tbody>
    {if empty($list)}
        <tr><td class="text-center">{$lang.no_available}</td></tr>
    {else}
        {section name=m loop=$list}
            <tr>
                <td data-label="{$lang.backup}" class="cell-sm-12">
                    <a data-act="getfilelink" data-query="dtype=d_sql&filename={$list[m]->db|urlencode}">{$list[m]->db}</a>
                </td>
            </tr>
        {/section}
    {/if}
</tbody>