<div class="module-header">
    <i class="icon-header icon-backups"></i>
    <h1>{$lang.main_header}</h1>
    <p>{$lang.main_hint}</p>
</div>
{if $template_six_style}    
    <div style="display: inline-flex;"><h3>{$lang.main_header}</h3>
        <button class="show-sm btn btn-icon show-hint" data-toggle="tooltip" title="{$lang.main_hint}" type="button"> 
            <i class="glyphicon glyphicon-question-sign"></i> 
        </button></div> <br /><br />{/if}
<div class="module-body">
    {if $automaticBackups}
        <div class="section">
            <div class="table-header">
                <div class="header-title">
                    <h4>{$lang.available_restore_points}</h4>
                </div>
                <div class="header-actions">
                    <div class="header-search">
                        <div class="input-icon">
                            <i class="fa fa-search"></i>
                            <input class="form-control" type="text" placeholder="{$lang.search_placeholder}" data-search="restore-points-table">
                        </div>
                    </div>

                </div>
            </div>
            <table class="table" id="restore-points-table">
                <thead>
                    <tr>
                        <th>{$lang.restore_points}</th>
                        <th>{$lang.subdomains}</th>
                        <th>{$lang.mail}</th>
                        <th>{$lang.mysql}</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>

        <div class="section">
            <div class="table-header">
                <div class="header-title">
                    <h4>{$lang.history}</h4>
                </div>
                <div class="header-actions">
                    <div class="header-search">
                        <div class="input-icon">
                            <i class="fa fa-search"></i>
                            <input class="form-control" type="text" placeholder="{$lang.search_placeholder}" data-search="history-table">
                        </div>
                    </div>

                </div>
            </div>
            <table class="table" id="history-table">
                <thead>
                    <tr>
                        <th>{$lang.restore_points}</th>
                        <th>{$lang.subdomains}</th>
                        <th>{$lang.mail}</th>
                        <th>{$lang.mysql}</th>
                        <th>{$lang.status}</th>
                        <th>{$lang.finished}</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    {elseif $abdisabled}
        <div class="section">
            <div class="alert alert-danger">
                {$lang.automatic_backup_disabled}
            </div>
        </div>
    {/if}
    {if $enablebackups}
        <div class="module-sub-header">
            <h2>{$lang.fullbackups_label}</h2>
            <p>{$lang.fullbackups_s_hint}</p>
        </div>
        <div class="section">
            <div class="table-header">
                <div class="header-title">
                    <h4>{$lang.available_backups}</h4>
                </div>
                <div class="header-actions">
                    <button  data-toggle="modal" data-target="#generate" class="btn btn-primary pull-right">{$lang.backups_full}</button>
                </div>
            </div>
            <table class="table" id="backups-table">
                <thead>
                    <tr>
                        <th>{$lang.backups_full_tblhead_backup}</th>
                        <th>{$lang.backups_full_tblhead_status}</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div class="module-sub-header">
            <h2>{$lang.backups_partial}</h2>
        </div>
        <div class="section">
            <div class="table-header">
                <div class="header-title">
                    <h4>{$lang.home_backup_download}</h4>
                </div>
                <div class="header-actions">
                    <button class="btn btn-primary pull-right" data-act="getfilelink"data-query="dtype=d_homedir">{$lang.download}</button>
                </div>
            </div>
        </div>
        <div class="section">
            <div class="table-header">
                <div class="header-title">
                    <h4>{$lang.mysql_backup_download}</h4>
                </div>
                <div class="header-actions">

                </div>
            </div>
            <table class="table" id="mysql-backups-table">
                <thead>
                    <tr>
                        <th>{$lang.mysql_backup_table}</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div class="section">
            <div class="table-header">
                <div class="header-title">
                    <h4>{$lang.emailF_backup_download}</h4>
                </div>
                <div class="header-actions">

                </div>
            </div>
            <table class="table" id="email-backups-table">
                <thead>
                    <tr>
                        <th>{$lang.emailF_backup_table}</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    {/if}
</div>

<div id="generate" class="modal fade modal-large">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{$lang.backups_full}</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.create_backup_destination}</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="fullbackup_destenation" name="dest">
                                <option value="homedir">{$lang.create_backup_opt_homedir}</option>
                                <option value="ftp">{$lang.create_backup_opt_ftp}</option>
                                <option value="passiveftp">{$lang.create_backup_opt_ftppass}</option>
                                <option value="scp">{$lang.create_backup_opt_scp}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.create_backup_email}</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-fluid">
                                        <div class="radio fluid-0">                                                        
                                            <label>
                                                <input name="backup_send_email_radio" value="1" type="radio" checked="" data-no-validate>
                                            </label>                                                        
                                        </div>
                                        <div class="fluid-100">
                                            <input class="form-control" type="email" name="backup_send_email" required data-error="{$validator_lang.invalid}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="radio"><label><input name="backup_send_email_radio" type="radio" value="0" data-no-validate>{$lang.dont_send_email}</label></div>
                                </div>
                            </div>    
                        </div>
                        <div class="col-md-9 col-md-offset-3">
                            <div class="help-block with-errors"></div>
                        </div>                                 
                    </div>
                    <div id="backup_advanced" style="display: none">
                        <div class="form-group">
                            <label class="control-label col-sm-3">{$lang.backup_server}</label>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <input class="form-control" type="text" name="server" required  data-error="{$validator_lang.field_required}" >
                                    </div>
                                    <div class="col-sm-6"></div>
                                </div>    
                            </div>
                            <div class="col-md-9 col-md-offset-3">
                                <div class="help-block with-errors"></div>
                            </div>                             
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{$lang.backup_server_user}</label>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <input class="form-control" type="text" name="user" required  data-error="{$validator_lang.field_required}" >
                                    </div>
                                    <div class="col-sm-6"></div>
                                </div>    
                            </div>
                            <div class="col-md-9 col-md-offset-3">
                                <div class="help-block with-errors"></div>
                            </div>                                 
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{$lang.backup_server_pass}</label>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <input class="form-control" type="password" name="pass" required  data-error="{$validator_lang.field_required}" >
                                    </div>
                                    <div class="col-sm-6"></div>
                                </div>    
                            </div>
                            <div class="col-md-9 col-md-offset-3">
                                <div class="help-block with-errors"></div>
                            </div>                                 
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{$lang.backup_server_port}</label>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <input class="form-control" type="number" name="port" min="0" max="65535" required  data-error="{$validator_lang.invalid}" >
                                    </div>
                                    <div class="col-sm-6"></div>
                                </div>    
                            </div>
                            <div class="col-md-9 col-md-offset-3">
                                <div class="help-block with-errors"></div>
                            </div>                                 
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{$lang.backup_server_dir}</label>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <input class="form-control" type="text" name="rdir" required  data-error="{$validator_lang.field_required}" >
                                    </div>
                                    <div class="col-sm-6"></div>
                                </div>    
                            </div>
                            <div class="col-md-9 col-md-offset-3">
                                <div class="help-block with-errors"></div>
                            </div>                                 
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{$lang.close}</button>
                <button type="button" class="btn btn-primary" data-act="generate_backup" data-rtable="backups-table">{$lang.backup_generate_btn}</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

{literal}
    <script>
        function linkCallback(output) {
            window.location.href = output.link;
        }

        (function (jQuery) {
            jQuery(document).ready(function () {
                refreshTable('email-backups-table');
                refreshTable('restore-points-table');
                refreshTable('history-table');
                refreshTable('backups-table');
                refreshTable('mysql-backups-table');

                jQuery("#fullbackup_destenation").change(function () {
                    if (jQuery(this).val() == 'homedir') {
                        jQuery("#backup_advanced").slideUp();
                    } else {
                        jQuery("#backup_advanced").slideDown();
                    }
                });

                jQuery("[name='backup_send_email_radio']").change(function () {
                    if (jQuery(this).val() == '1')
                        jQuery("[name='backup_send_email']").removeAttr('disabled');
                    else
                        jQuery("[name='backup_send_email']").attr('disabled', '');
                });
            });
        })(jQuery);
    </script>
{/literal}