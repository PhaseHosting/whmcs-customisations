<tbody>
    {if empty($list)}
        <tr><td colspan="6" class="text-center">{$lang.no_history}</td></tr>
    {else}    
        {foreach from=$list item="backup"}
            <tr>
                <td data-label="{$lang.restore_point}" class="cell-sm-12">{$backup.restore_point}</td>
                <td data-label="{$lang.subdomains}" class="cell-sm-12"><input type="checkbox" {if $backup.subdomains}checked=""{/if} disabled/></td>
                <td data-label="{$lang.mail}" class="cell-sm-12"><input type="checkbox" {if $backup.mailconfig}checked=""{/if} disabled/></td>
                <td data-label="{$lang.mysql}" class="cell-sm-12"><input type="checkbox" {if $backup.mysql}checked=""{/if} disabled/></td>
                <td data-label="{$lang.status}" class="cell-sm-12">{if $backup.status != '' && $backup.status == 1}{if file_exists("assets/img/statusok.gif")}<img src="assets/img/statusok.gif" alt="yes" />{else}<img src="images/statusok.gif" alt="yes" />{/if}
                    {elseif $backup.status != '' && $backup.status == 0}{if file_exists("assets/img/statusfailed.gif")}<img src="assets/img/statusfailed.gif" width="16px" height="16px" alt="no" />{else}<img src="images/statusfailed.gif" width="16px" height="16px" alt="no" />{/if}
                    {elseif $backup.status == '' && $backup.type == 'active'}{if file_exists("assets/img/spinner.gif")}<img src="assets/img/spinner.gif" width="16px" height="16px" alt="In Progress" />{else}<img src="images/spinner.gif" width="16px" height="16px" alt="In Progress" />{/if}
                    {else}{$backup.status}{/if}   
                </td>
                <td data-label="{$lang.finished}" class="cell-sm-12 table-actions">{if $backup.status == '' && $backup.type == 'active'}<button type="button" class="btn btn-icon" data-act="justEmptyAction" data-rtable="history-table"><i class="fa fa-refresh"></i></button>
                    {elseif !empty($backup.finished)}{$backup.finished}
                    {else}-{/if}
                </td>
            </tr>
        {/foreach}
    {/if}
</tbody>