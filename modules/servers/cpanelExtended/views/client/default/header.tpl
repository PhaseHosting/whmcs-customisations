<input type="hidden" id="product_id" value="{$_params.serviceid}" /> 

<link href="{$_assets_dir}css/bootstrap.min.css" rel="stylesheet">
<link href="{$_assets_dir}css/font-awesome.min.css" rel="stylesheet">
<!--- ################ -->
<link href="{$_assets_dir}css/layout.css" rel="stylesheet">
<link href="{$_assets_dir}css/theme.css" rel="stylesheet">

{literal}
    <script type="text/javascript">
        var bootstrap_src = "{/literal}{$_assets_dir}{literal}js/bootstrap.min.js";
        if (!window.jQuery) { 
            document.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js" type="text/javascript"><\/script>'); 
            document.write('<script src="' + bootstrap_src +'" type="text/javascript"><\/script>'); 
        } else {
            var sp = jQuery.fn.jquery.split(" ")[0].split(".");
            if(sp[0]<2 && sp[1]<9 || 1==sp[0] && 9==sp[1] && sp[2]<1)
                document.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js" type="text/javascript"><\/script>'); 
            jQuery(window).load(function () {
                if(!jQuery.fn.modal)
                    jQuery('#mg-wrapper').prepend('<script src="' + bootstrap_src +'" type="text/javascript"><\/script>'); 
            });
        }
    </script>
{/literal}

{if !$template_six_style}<!-- <script src="{$_assets_dir}js/bootstrap.min.js" type="text/javascript"></script> -->{/if}
<script src="{$_assets_dir}js/bootsrap.tooltip.js" type="text/javascript"></script>

<script src="{$_assets_dir}js/validator.js" type="text/javascript"></script>
<script src="{$_assets_dir}js/app.js" type="text/javascript"></script>
<script src="{$_assets_dir}js/zxcvbn.js" type="text/javascript"></script>

{literal} 
<script type="text/javascript">
    var lang = new Object();
    var validator_lang = new Object();{/literal}
    {foreach from=$lang item=l key=k}
        {literal}lang['{/literal}{$k|replace:"'":""}{literal}'] = '{/literal}{$l|replace:"'":""}{literal}';{/literal}
    {/foreach}
    {foreach from=$validator_lang item=l key=k}
        {literal}validator_lang['{/literal}{$k|replace:"'":""}{literal}'] = '{/literal}{$l|replace:"'":""}{literal}';{/literal}
    {/foreach}

    {literal} 
    </script>
{/literal}    
<div class="module-main-header">
    <a href="clientarea.php?action=productdetails&id={$serviceid}" class="btn btn-back btn-icon"><i class="fa fa-arrow-left"></i></a><h2 {if $WHMCS6}style="line-height: 36px;"{/if}>{$product_details.product_group} - {$product_details.product_name} {if !empty($userdomain)}{$userdomain}{/if}</h2>
</div>
<div id="mg-wrapper" class="module-container {if $sidebar_closed}sidebar-closed{/if}">
    {if !$template_six_style}<div class="module-sidebar">
        <div class="sidebar-header">
            <button class="btn btn-icon cp-sidebar-toggle" data-target=".module-menu" data-toggle="collapse" type="button"> 
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <h3>
                <span class="hide-sm">{$main_lang.main_header}</span>
                <span class="show-sm">{$lang.main_header}</span>
            </h3>
            <button class="show-sm btn btn-icon show-hint" data-toggle="tooltip" title="{$lang.main_hint}" type="button"> 
                <i class="fa fa-question-circle"></i> 
            </button>
        </div>
        <ul class="module-menu collapse">
            {if $manageinstallapp == "on"}<li {if $_current_page eq 'installatron' || $_current_page eq 'softaculous'}class="active"{/if}><a href="{$_burl}&page=installapp"><i class="icon-menu icon-installapp"></i><span>{$main_lang.install_app_link}</span></a></li>{/if}
            {if $manageftp == "on"}<li {if $_current_page eq 'ftp'}class="active"{/if}><a href="{$_burl}&page=ftp"><i class="icon-menu icon-ftp"></i><span>{$main_lang.ftp_link}</span></a></li>{/if}
            {if $manageemails == "on"}<li {if $_current_page eq 'emails'}class="active"{/if}><a href="{$_burl}&page=emails"><i class="icon-menu icon-emails"></i><span>{$main_lang.emails_link}</span></a></li>{/if}
            {if $manageemailsforwarders == "on"}<li {if $_current_page eq 'emailsforwarders'}class="active"{/if}><a href="{$_burl}&page=emailsforwarders"><i class="icon-menu icon-emailsforwarders"></i><span>{$main_lang.emails_forwarders_link}</span></a></li>{/if}
            {if $managedatabases == "on"}<li {if $_current_page eq 'databases'}class="active"{/if}><a href="{$_burl}&page=databases"><i class="icon-menu icon-databases"></i><span>{$main_lang.databases_link}</span></a></li>{/if}
            {if $managecron == "on"}<li {if $_current_page eq 'cron'}class="active"{/if}><a href="{$_burl}&page=cron"><i class="icon-menu icon-cron"></i><span>{$main_lang.cron_link}</span></a></li>{/if}
            {if $managesubdomains == "on"}<li {if $_current_page eq 'subdomains'}class="active"{/if}><a href="{$_burl}&page=subdomains"><i class="icon-menu icon-subdomains"></i><span>{$main_lang.subdomains_link}</span></a></li>{/if}
            {if $manageaddondomains == "on"}<li {if $_current_page eq 'addondomains'}class="active"{/if}><a href="{$_burl}&page=addondomains"><i class="icon-menu icon-addondomains"></i><span>{$main_lang.addon_domains_link}</span></a></li>{/if}
            {if $manageforwarddomains == "on"}<li {if $_current_page eq 'forwarddomains'}class="active"{/if}><a href="{$_burl}&page=forwarddomains"><i class="icon-menu icon-forwarddomains"></i><span>{$main_lang.forward_domains_link}</span></a></li>{/if}
            {if $manageparkeddomains == "on"}<li {if $_current_page eq 'parkeddomains'}class="active"{/if}><a href="{$_burl}&page=parkeddomains"><i class="icon-menu icon-parkeddomains"></i><span>{$main_lang.parked_domains_link}</span></a></li>{/if}
            {if $dnsmanagement == "on"}<li><a href="clientarea.php?managedns={$dnsdomain}"><i class="icon-menu icon-managedns"></i><span>{$main_lang.dns_management_link}</span></a></li>{/if}
            {if $managegraphsstats == "on"}<li {if $_current_page eq 'graphsstats'}class="active"{/if}><a href="{$_burl}&page=graphsstats"><i class="icon-menu icon-graphsstats"></i><span>{$main_lang.graphsstats}</span></a></li>{/if}
            {if $managessl == "on"}<li {if $_current_page eq 'ssl'}class="active"{/if}><a href="{$_burl}&page=ssl"><i class="icon-menu icon-ssl"></i><span>{$main_lang.managessl}</span></a></li>{/if}
            {if $managebackups == "on"}<li {if $_current_page eq 'backups'}class="active"{/if}><a href="{$_burl}&page=backups"><i class="icon-menu icon-backups"></i><span>{$main_lang.managebackups}</span></a></li>{/if}
            {if $managebans == "on"}<li {if $_current_page eq 'bans'}class="active"{/if}><a href="{$_burl}&page=bans"><i class="icon-menu icon-bans"></i><span>{$main_lang.managebans}</span></a></li>{/if}
            {if $managemx == "on"}<li {if $_current_page eq 'mx'}class="active"{/if}><a href="{$_burl}&page=mx"><i class="icon-menu icon-mx"></i><span>{$main_lang.mx}</span></a></li>{/if}
            {if $managefiles == "on"}<li {if $_current_page eq 'files'}class="active"{/if}><a href="{$_burl}&page=files"><i class="icon-menu icon-files"></i><span>{$main_lang.files}</span></a></li>{/if}
            {if $managessh == "on"}<li {if $_current_page eq 'ssh'}class="active"{/if}><a href="{$_burl}&page=ssh"><i class="icon-menu icon-ssh"></i><span>{$main_lang.ssh}</span></a></li>{/if}

            {if $managephpmyadmin == "on"}<li><a {if $currpage=="phpmyadmin"}onclick="return false" class="selected"{/if} target="_new"  href="clientarea.php?action=productdetails&id={$_params.serviceid}&modop=custom&a=management&page=phpmyadmin"><i class="icon-menu icon-phpmyadmin"></i><span>{$main_lang.phpmyadmin}</span></a></li>{/if}
            {if $managervsitebuilderlogin == "on"}<li><a {if $currpage=="rvsitebuilderlogin"}onclick="return false" class="selected"{/if} target="_new"  href="clientarea.php?action=productdetails&id={$_params.serviceid}&modop=custom&a=management&page=rvsitebuilderlogin"><i class="icon-menu icon-rv"></i><span>{$main_lang.clientarearvsitebuilderlink}</span></a></li>{/if}
                        {if $cpanellogin == "on"}
                            {if $producttype == "reselleraccount"}
                    <li>  
                        <form style="margin: 0px; padding: 0px;" name="form_whm" action="http{if $serversecure}s{/if}://{if $serverhostname}{$serverhostname}{else}{$serverip}{/if}:{if $serversecure}2087{else}2086{/if}/login/" method="post" target="_blank">
                            <input type="hidden" name="user" value="{$username}" />
                            <input type="hidden" name="pass" value="{$password}" />
                            <a href="#" onmousedown="form_whm.submit();
                                    return false;"><i class="icon-menu icon-cpanel"></i><span>{if $LANG.cpanelwhmlogin}{$LANG.cpanelwhmlogin}{else}{$LANG.clientareawhmlink}{/if}</span></a>
                        </form>
                    </li>
                {else}
                    <li> 
                        <form style="margin: 0px; padding: 0px;" name="form_cpanel" action="http{if $serversecure}s{/if}://{if $serverhostname}{$serverhostname}{else}{$serverip}{/if}:{if $serversecure}2083{else}2082{/if}/login/" method="post" target="_blank">
                            <input type="hidden" name="user" value="{$username}" />
                            <input type="hidden" name="pass" value="{$password}" />
                            <a href="#" onmousedown="form_cpanel.submit();
                                    return false;"><i class="icon-menu icon-cpanel"></i><span>{if $LANG.cpanellogin}{$main_lang.clientareacpanellink}{else}{$main_lang.clientareacpanellink}{/if}</span></a>
                        </form>
                    </li>
                {/if}
            {/if}
            {if $webmaillogin == "on"}<li><a href="http{if $serversecure}s{/if}://{if $serverhostname}{$serverhostname}{else}{$serverip}{/if}:{if $serversecure}2096{else}2095{/if}" target="_blank"><i class="icon-menu icon-webmail"></i><span>{if $main_lang.cpanelwebmaillogin}{$main_lang.cpanelwebmaillogin}{else}{$main_lang.clientareawebmaillink}{/if}</span></a></li>{/if}
            {if $managefileslogin == "on"}<li><a {if $currpage=="phpmyadmin"}onclick="return false" class="selected"{/if} target="_new"  href="clientarea.php?action=productdetails&id={$_params.serviceid}&modop=custom&a=management&page=fileslogin"><i class="icon-menu icon-files"></i><span>{$main_lang.fileslogin}</span></a></li>{/if}

        </ul>
    </div>{/if}
    <div class="module-content" {if $template_six_style}style="margin-left: initial;"{/if}>
        <div id="mg-error-container">
            {include file='info.tpl'}
            {include file='error.tpl'}
        </div>
        <div id="mg-container">



