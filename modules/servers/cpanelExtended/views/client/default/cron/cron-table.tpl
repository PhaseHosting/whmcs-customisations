<tbody>
{if $tablen < 1}
    <tr><td colspan="8" class="text-center">{$lang.current_tblheader_empty}</td></tr> 
{else}        
    {section name=c loop=$joblist max=$tablen}
        <tr id="line{$joblist[c]->count}">
            <td data-label="ID" class="cell-sm-6">{$joblist[c]->count}</td>
            <td data-label="{$lang.current_tblheader_minute}" class="cell-sm-6">{$joblist[c]->minute}</td>
            <td data-label="{$lang.current_tblheader_hour}" class="cell-sm-6">{$joblist[c]->hour}</td>
            <td data-label="{$lang.current_tblheader_day}" class="cell-sm-6">{$joblist[c]->day}</td>
            <td data-label="{$lang.current_tblheader_month}" class="cell-sm-6">{$joblist[c]->month}</td>
            <td data-label="{$lang.current_tblheader_weekday}" class="cell-sm-6">{$joblist[c]->weekday}</td>
            <td data-label="{$lang.current_tblheader_command}" class="cell-sm-12">{$joblist[c]->command}</td>
            <td data-label="{$lang.current_tblheader_actions}" class="cell-sm-12 cell-actions">
                <a class="btn btn-icon" data-toggle="modal" data-target="#edit-job-modal" title="{$lang.current_bookmark_edit}"
                   data-modal-insert="[name=line]:{$joblist[c]->count}|[name=minute]:{$joblist[c]->minute|urlencode}|[name=hour]:{$joblist[c]->hour|urlencode}|[name=day]:{$joblist[c]->day|urlencode}|[name=month]:{$joblist[c]->month|urlencode}|[name=weekday]:{$joblist[c]->weekday|urlencode}|[name=command]:{$joblist[c]->command|rawurlencode}"><i class="fa fa-pencil"></i></a>
                <a class="btn btn-icon" data-toggle="modal" data-target="#delete-job-modal" title="{$lang.current_bookmark_delete}" data-modal-insert="[name=line]:{$joblist[c]->count}"><i class="fa fa-trash"></i></a>
            </td>
        </tr>		
    {/section}
{/if}     

</tbody>