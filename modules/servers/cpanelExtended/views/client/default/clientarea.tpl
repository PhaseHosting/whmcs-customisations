<link href="{$_assets_dir}css/bootstrap.min.css" rel="stylesheet">
<link href="{$_assets_dir}css/font-awesome.min.css" rel="stylesheet">
<!--- ################ -->
<link href="{$_assets_dir}css/layout.css" rel="stylesheet">
<link href="{$_assets_dir}css/theme.css" rel="stylesheet">

<input type="hidden" id="product_id"  value="{$productid}" />
<input type="hidden" id="isActive"    value="{$isactive}" />
<input type="hidden" id="createzones" value="{$createzones}"/>
<input type="hidden" id="dnsdomain"   value="{$dnsdomain}"/>
<div id="mg-wrapper" class="module-container">   
    {if $manageftp == "on" || $manageemails == "on" || $manageemailsforwardes == "on" || $managedatabases == "on" || $managecron == "on" || $managesubdomains == "on" || $manageaddondomains == "on" || $manageforwarddomains == "on" || $manageparkeddomains == "on" || $dnsmanagement == "on" || $graphsstats == "on" || $managessl == "on" || $managebackups == "on" || $managebans == "on" || $manageinstallapp == "on"}
        <div class="well buttons-content">
            <h4 class="text-center mb-20">{$lang.main_header}</h4>
            <div class="row">
            {if $manageinstallapp == "on"}{print_button page='installapp' icon='installapp' label=$lang.install_app_link}{/if}
        {if $manageftp == "on"}{print_button page='ftp' icon='ftp' label=$lang.ftp_link}{/if}
    {if $manageemails == "on"}{print_button page='emails' icon='emails' label=$lang.emails_link}{/if}
{if $manageemailsforwardes == "on"}{print_button page='emailsforwarders' icon='emailsforwarders' label=$lang.emails_forwarders_link}{/if}
{if $managedatabases == "on"}{print_button page='databases' icon='databases' label=$lang.databases_link}{/if}
{if $managecron == "on"}{print_button page='cron' icon='cron' label=$lang.cron_link}{/if}
{if $managesubdomains == "on"}{print_button page='subdomains' icon='subdomains' label=$lang.subdomains_link}{/if}
{if $manageaddondomains == "on"}{print_button page='addondomains' icon='addondomains' label=$lang.addon_domains_link}{/if}
{if $manageforwarddomains == "on"}{print_button page='forwarddomains' icon='forwarddomains' label=$lang.forward_domains_link}{/if}
{if $manageparkeddomains == "on"}{print_button page='parkeddomains' icon='parkeddomains' label=$lang.parked_domains_link}{/if}
{if $dnsmanagement == "on"}
    <div class="col-sm-4 col-xs-6">
        <a class="big-button" href="clientarea.php?managedns={$dnsdomain}">
            <div class="button-wrapper">
                <i class="icon-btn icon-managedns"></i>
                <span>{$lang.dns_management_link}</span>
            </div>
        </a>
    </div>
{/if}
{if $graphsstats == "on"}{print_button page='graphsstats' icon='graphsstats' label=$lang.graphsstats}{/if}
{if $managessl == "on"}{print_button page='ssl' icon='ssl' label=$lang.managessl}{/if}
{if $managebackups == "on"}{print_button page='backups' icon='backups' label=$lang.managebackups}{/if}
{if $managebans == "on"}{print_button page='bans' icon='bans' label=$lang.managebans}{/if}
{if $managemx == "on"}{print_button page='mx' icon='mx' label=$lang.mx}{/if}
{if $managefiles == "on"}{print_button page='files' icon='files' label=$lang.files}{/if}
{if $managessh == "on"}{print_button page='ssh' icon='ssh' label=$lang.ssh}{/if}
</div>
</div>
{else}
    {assign var=no_manage_button value=1}
{/if}

{if $cpanellogin == "on" || $webmaillogin == "on" || $phpmyadmin == "on" || $rvsitebuilderlogin == "on" || $managefileslogin == "on"}
    <div class="well buttons-content">
        <h4 class="text-center mb-20">{$lang.main_header2}</h4>
        <div class="row">
            {if $cpanellogin == "on"}
                {if $producttype == "reselleraccount"}
                    <div class="col-lg-4 col-xs-6">
                        <form  action="http{if $serversecure}s{/if}://{if $serverhostname}{$special.serverhostname}{else}{$special.serverip}{/if}:{if $serversecure}2087{else}2086{/if}/login/" method="post" target="_blank">
                            <input type="hidden" name="user" value="{$special.username}" />
                            <input type="hidden" name="pass" value="{$special.password}" />
                            <button class="big-button" onmousedown="jQuery(this).parent().submit();"> 
                                <div class="button-wrapper">
                                    <i class="icon-btn icon-cpanel"></i>
                                    <span>{$lang.clientareawhmlink}</span>
                                </div>
                            </button>
                        </form>
                    </div>
                {else}
                    <div class="col-lg-4 col-xs-6">
                        <form action="http{if $serversecure}s{/if}://{if $serverhostname}{$special.serverhostname}{else}{$special.serverip}{/if}:{if $serversecure}2083{else}2082{/if}/login/" method="post" target="_blank">
                            <input type="hidden" name="user" value="{$special.username}" />
                            <input type="hidden" name="pass" value="{$special.password}" />
                            <button class="big-button" onmousedown="jQuery(this).parent().submit();">
                                <div class="button-wrapper">
                                    <i class="icon-btn icon-cpanel"></i>
                                    <span>{$lang.clientareacpanellink}</span>
                                </div>
                            </button>
                        </form>
                    </div>
                {/if}
            {/if}
            {if $webmaillogin == "on"}
                <div class="col-lg-4 col-xs-6">
                    <a class="big-button" onclick="window.open('http{if $serversecure}s{/if}://{if $serverhostname}{$special.serverhostname}{else}{$special.serverip}{/if}:{if $serversecure}2096{else}2095{/if}');">
                        <div class="button-wrapper">
                            <i class="icon-btn icon-webmail"></i>
                            <span>{$lang.clientareawebmaillink}</span>
                        </div>
                    </a>
                </div>
            {/if}

            {if $phpmyadmin == "on"}
                <div class="col-lg-4 col-xs-6">
                    <a class="big-button" onclick="win = window.open('clientarea.php?action=productdetails&id={$serviceid}&modop=custom&a=management&page=phpmyadmin', '_blank');
                            win.focus();
                            return false;">
                        <div class="button-wrapper">
                            <i class="icon-btn icon-phpmyadmin"></i>
                            <span>{$lang.phpmyadmin}</span>
                        </div>
                    </a>
                </div>
            {/if}

            {if $rvsitebuilderlogin == "on"}
                <div class="col-lg-4 col-xs-6">
                    <a class="big-button" onclick="win = window.open('clientarea.php?action=productdetails&id={$serviceid}&modop=custom&a=management&page=rvsitebuilderlogin', '_blank');
                            win.focus();
                            return false;">
                        <div class="button-wrapper">
                            <i class="icon-btn icon-rv"></i>
                            <span>{$lang.rvsitebuilderlink}</span>
                        </div>
                    </a>
                </div>
            {/if}

            {if $managefileslogin == "on"}
                <div class="col-lg-4 col-xs-6">
                    <a class="big-button" onclick="win = window.open('clientarea.php?action=productdetails&id={$serviceid}&modop=custom&a=management&page=fileslogin', '_blank');
                            win.focus();
                            return false;">
                        <div class="button-wrapper">
                            <i class="icon-btn icon-files"></i>
                            <span>{$lang.fileslogin}</span>
                        </div>
                    </a>
                </div>
            {/if}
        </div>
    </div> 
{else}
    {if $no_manage_button}
        <div class="alert alert-info" style="margin-bottom: 0px;">
            {$lang.no_features}
        </div>
    {/if}
{/if}
</div>
