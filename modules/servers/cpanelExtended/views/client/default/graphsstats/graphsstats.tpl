<div class="module-header">
    <i class="icon-header icon-graphsstats"></i>
    <h1>{$lang.main_header}</h1>
    <p>{$lang.main_hint}</p>
</div>
{if $template_six_style}    
    <div style="display: inline-flex;"><h4>{$lang.main_header}</h4>
        <button class="show-sm btn btn-icon show-hint" data-toggle="tooltip" title="{$lang.main_hint}" type="button"> 
            <i class="glyphicon glyphicon-question-sign"></i> 
        </button></div> <br /><br />{/if}
<div class="module-body">
    <div class="section">
        <input type="hidden" id="product_id" value="{$productid}" />
            <table class="table" id="ftp-table">
                <thead>
                    <tr>
                        <th>{$main_lang.domains}</th>
                        {foreach from=$avaible item=s}  
                            <th>{$s}</th>
                        {/foreach}
                    </tr>
                </thead>
                <tbody>
                {foreach from=$list key=key item=s}  
                    <tr>
                        <td class="cell-sm-12" data-label="{$main_lang.domains}">{$key}</td>
                        {foreach from=$s key=type item=link}  
                            <td class="cell-sm-6" data-label="{$avaible.$type}">
                                {if $link}
                                    <a href="{$link}" target="_blank"><img src="{$_assets_dir}img/stats/{$type}.png" alt="{$type}"/></a>
                                {else}
                                    <span><img src="{$_assets_dir}img/stats/{$type}_gray.png" alt="{$type}"/></span>
                                {/if}
                            </td>
                        {/foreach}
                    </tr>
                {foreachelse}
                    <td class="text-center" colspan="5">{$main_lang.statsnotavaible}</td>
                {/foreach}    
                </tbody>
            </table>
    </div>
</div>