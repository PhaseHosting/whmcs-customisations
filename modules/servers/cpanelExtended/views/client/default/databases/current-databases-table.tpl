<tbody>	
    {if empty($dblist)}
        <tr><td colspan="4" class="text-center">{$lang.current_db_search_empty}</td></tr> 
        {else}
            {section name=d loop=$dblist}
            <tr>
                <td data-label="{$lang.current_db_head_db}" class="cell-sm-12">{$dblist[d]->db}</td>
                <td data-label="{$lang.current_db_head_size}" class="cell-sm-12 no-wrap">{($dblist[d]->size/1048576)|round:2} MB</td>
                <td data-label="{$lang.current_db_head_users}" class="cell-sm-12 no-wrap">
                    <ul>
                        {foreach from=$dblist[d]->userlist item="u"}
                            <li>{$u->user}&nbsp;<button class="btn btn-icon" data-act="setuserprivilages" data-query="dbname={$dblist[d]->db|urlencode}&dbuser={$u->user|urlencode}" title="{$lang.manage_user_privileges}"><i class="fa fa-pencil" style="color: #8A8E99; font-size: 16px; line-height: 22px;"></i></button></li>
                        {/foreach}
                    </ul>
                </td>
                <td data-label="{$lang.current_db_head_actions}" class="cell-sm-12 cell-actions">
                    <a class="btn btn-icon" data-toggle="modal" data-target="#delete-database-modal" data-modal-insert="[name=dbname]:{$dblist[d]->db|urlencode}" title="{$lang.current_db_bookmark_delete}"><i class="fa fa-trash"></i></a>
                </td>
            </tr>
        {/section}
    {/if}
</tbody>