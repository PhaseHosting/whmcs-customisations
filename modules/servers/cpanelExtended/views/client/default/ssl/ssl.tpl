<div class="module-header">
     <i class="icon-header icon-ssl"></i>
    <h1>{$lang.main_header}</h1>
    <p>{$lang.main_hint}</p>
</div>
{if $template_six_style}    
    <div style="display: inline-flex;"><h3>{$lang.main_header}</h3>
        <button class="show-sm btn btn-icon show-hint" data-toggle="tooltip" title="{$lang.main_hint}" type="button"> 
            <i class="glyphicon glyphicon-question-sign"></i> 
        </button></div> <br /><br />{/if}
<div class="module-body"> 
    <div class="module-sub-header">
        <h2>{$lang.key_on_server}</h2>
        <p>{$lang.key_desc}</p>
    </div>
    <div class="section">
        <div class="table-header">
            <div class="header-title">
                <h4>{$lang.your_private_keys}</h4>
            </div>
            <div class="header-actions">
                <div class="header-search">
                    <button data-toggle="modal" data-target="#upload" class="btn btn-success btn-block-mobile">{$lang.keys_upload}</button>
                    <button data-toggle="modal" data-target="#generate" class="btn btn-primary btn-block-mobile">{$lang.keys_generate}</button>
                </div>
            </div>
        </div>
        <table class="table" id="private-keys-table">
            <thead>
                <tr>
                    <th>{$lang.domain}</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <div class="module-sub-header">
        <h2>{$lang.csr_on_server}</h2>
        <p>{$lang.csr_desc}</p>
    </div>
    <div class="section">
        <div class="table-header">
            <div class="header-title">
                <h4>{$lang.your_csr_keys}</h4>
            </div>
            <div class="header-actions">
                <div class="header-search">                                           
                    <button data-toggle="modal" data-target="#csr" class="btn btn-primary btn-block-mobile">{$lang.csr_generate}</button>
                </div>
            </div>
        </div>
        <table class="table" id="csr-keys-table">
            <thead>
                <tr>
                    <th>{$lang.domain}</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <div class="module-sub-header">
        <h2>{$lang.crt_on_server}</h2>
        <p>{$lang.crt_desc}</p>
    </div>
    <div class="section">
        <div class="table-header">
            <div class="header-title">
                <h4>{$lang.your_ssl_certificates}</h4>
            </div>
            <div class="header-actions">
                <div class="header-search">                                           
                    <button data-toggle="modal" data-target="#ssl-upload" class="btn btn-success btn-block-mobile">{$lang.crt_upload}</button>
                    <button data-toggle="modal" data-target="#ssl-generate" class="btn btn-primary btn-block-mobile">{$lang.crt_generate}</button>
                    <button data-toggle="modal" data-target="#installssl" class="btn btn-success btn-block-mobile">{$lang.crt_install}</button>
                </div>
            </div>
        </div>
        <table class="table" id="ssl-certificates-table">
            <thead>
                <tr>
                    <th>{$lang.domain}</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<div id="upload" class="modal fade">
    <form class="form-horizontal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{$lang.keys_upload}</h4>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.fields.key4domain}</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="host" id="key-upload-domain" required pattern="{$patterns.simple_domain}"  data-error="{$validator_lang.invalid}">
                        </div>
                        <div class="col-md-9 col-md-offset-3">
                            <div class="help-block with-errors"></div>
                        </div>                          
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.fields.or}</label>
                        <div class="col-sm-9">

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.fields.select_domain}</label>
                        <div class="col-sm-9">
                            <select name="host_select" class="form-control" data-copy-to="key-upload-domain">
                                <option value="">{$lang.fields.select_domain}</option>
                                {foreach from=$domainList item=value}  
                                    <option value="{$value}">{$value}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.fields.paste_key}</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" rows="5" name="keyArea" required  data-error="{$validator_lang.field_required}"></textarea>
                        </div>
                        <div class="col-md-9 col-md-offset-3">
                            <div class="help-block with-errors"></div>
                        </div>                            
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{$lang.close}</button>
                    <button type="button" class="btn btn-primary" data-act="keys_upload" data-refresh="">{$lang.upload}</button> <!-- data-rtable="private-keys-table" -->
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </form>
</div><!-- /.modal -->

<div id="generate" class="modal fade">
    <form class="form-horizontal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{$lang.keys_generate}</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.fields.key4domain}</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="host" id="key-generate-domain" required pattern="{$patterns.simple_domain}"  data-error="{$validator_lang.invalid}">
                        </div>
                        <div class="col-md-9 col-md-offset-3">
                            <div class="help-block with-errors"></div>
                        </div>                          
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.fields.or}</label>
                        <div class="col-sm-9">

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.fields.select_domain}</label>
                        <div class="col-sm-9">
                            <select name="host_select" class="form-control" data-copy-to="key-generate-domain">
                                <option value="">{$lang.fields.select_domain}</option>
                                {foreach from=$domainList item=value}  
                                    <option value="{$value}">{$value}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.fields.key_size}</label>
                        <div class="col-sm-9">
                            <select name="keySize" class="form-control" required>
                                {foreach from=$lang.key_sizes item=name key=key}
                                    {if $key > 1024}<option value="{$key}">{$name}</option>{/if}
                                {/foreach}
                            </select>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{$lang.close}</button>
                    <button type="button" class="btn btn-primary" data-act="keys_generate" data-refresh="1">{$lang.generate}</button> <!-- data-rtable="private-keys-table" -->
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </form>
</div><!-- /.modal -->

<div id="csr" class="modal fade">
    <form class="form-horizontal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{$lang.csr_generate}</h4>
                </div>
                <div class="modal-body">   
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.fields.select_key_domain}</label>
                        <div class="col-sm-9">
                            <select name="host_select" class="form-control" pattern="{$patterns.simple_domain}" required=""  data-error="{$validator_lang.invalid}" >
                                <option value="">{$lang.fields.select_domain}</option>
                                {foreach from=$listKeys item=value}  
                                    <option value="{$value->host}">{$value->host}</option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="col-md-9 col-md-offset-3">
                            <div class="help-block with-errors"></div>
                        </div>                              
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.fields.country}</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="country" value="{$clientsdetails.country}" required  data-error="{$validator_lang.field_required}" />
                        </div>
                        <div class="col-md-9 col-md-offset-3">
                            <div class="help-block with-errors"></div>
                        </div>                         
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.fields.state}</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="state" value="{$clientsdetails.state}" required  data-error="{$validator_lang.field_required}" >
                        </div>
                        <div class="col-md-9 col-md-offset-3">
                            <div class="help-block with-errors"></div>
                        </div>                            
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.fields.city}</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="city" value="{$clientsdetails.city}" required  data-error="{$validator_lang.field_required}" >
                        </div>
                        <div class="col-md-9 col-md-offset-3">
                            <div class="help-block with-errors"></div>
                        </div>                            
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.fields.company}</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="company" value="{$clientsdetails.companyname}" required  data-error="{$validator_lang.field_required}" >
                        </div>
                        <div class="col-md-9 col-md-offset-3">
                            <div class="help-block with-errors"></div>
                        </div>                            
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.fields.comp_divi}</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="comp_divi" value="" id="comp_divi">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.fields.email}</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="email" name="email" value="{$clientsdetails.email}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.fields.pass}</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="pass" value="" data-minlength="4">
                            <div class="help-block">{$lang.min_4_chars}</div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{$lang.close}</button>
                    <button type="button" class="btn btn-primary" data-act="csr_generate" data-rtable="csr-keys-table">{$lang.generate}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </form>
</div><!-- /.modal -->

<div id="ssl-generate" class="modal fade">
    <form class="form-horizontal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{$lang.crt_generate}</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.fields.select_key_domain}</label>
                        <div class="col-sm-9">
                            <select name="host" class="form-control" pattern="{$patterns.simple_domain}" required=""  data-error="{$validator_lang.invalid}" >
                                <option value="">{$lang.fields.select_domain}</option>
                                {foreach from=$listKeys item=value}  
                                    <option value="{$value->host}">{$value->host}</option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="col-md-9 col-md-offset-3">
                            <div class="help-block with-errors"></div>
                        </div>                              
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.fields.country}</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="country" value="{$clientsdetails.country}" required  data-error="{$validator_lang.field_required}" />
                        </div>
                        <div class="col-md-9 col-md-offset-3">
                            <div class="help-block with-errors"></div>
                        </div>                           
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.fields.state}</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="state" value="{$clientsdetails.state}" required  data-error="{$validator_lang.field_required}" >
                        </div>
                        <div class="col-md-9 col-md-offset-3">
                            <div class="help-block with-errors"></div>
                        </div>                           
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.fields.city}</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="city" value="{$clientsdetails.city}" required  data-error="{$validator_lang.field_required}" >
                        </div>
                        <div class="col-md-9 col-md-offset-3">
                            <div class="help-block with-errors"></div>
                        </div>                           
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.fields.company}</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="company" value="{$clientsdetails.companyname}" required  data-error="{$validator_lang.field_required}" >
                        </div>
                        <div class="col-md-9 col-md-offset-3">
                            <div class="help-block with-errors"></div>
                        </div>                           
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.fields.comp_divi}</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" name="comp_divi" value="" id="comp_divi">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.fields.email}</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="email" name="email" value="{$clientsdetails.email}">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{$lang.close}</button>
                    <button type="button" class="btn btn-primary" data-act="crt_generate" data-rtable="ssl-certificates-table">{$lang.generate}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </form>
</div><!-- /.modal -->

<div id="ssl-upload" class="modal fade">
    <form class="form-horizontal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{$lang.crt_upload}</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.fields.paste_key}</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" rows="5" name="keyArea" required=""  data-error="{$validator_lang.field_required}" ></textarea>
                        </div>
                        <div class="col-md-9 col-md-offset-3">
                            <div class="help-block with-errors"></div>
                        </div>                         
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{$lang.close}</button>
                    <button type="button" class="btn btn-primary" data-act="crt_upload" data-rtable="ssl-certificates-table">{$lang.upload}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </form>
</div><!-- /.modal -->

{confirm_modal btn='data-act="key_delete" data-refresh="1"' id="delete-key-modal" title=$lang.delete_confirmation_title
        body="`$lang.delete_key_confirmation_body`<input type='hidden' name='host'/>"} {*data-rtable="private-keys-table"*}
{confirm_modal btn='data-act="csr_delete" data-rtable="csr-keys-table"' id="delete-csr-modal" title=$lang.delete_confirmation_title
        body="`$lang.delete_csr_confirmation_body`<input type='hidden' name='host'/>"}
{confirm_modal btn='data-act="crt_delete" data-rtable="ssl-certificates-table"' id="delete-crt-modal" title=$lang.delete_confirmation_title
        body="`$lang.delete_crt_confirmation_body`<input type='hidden' name='host'/>"}

<div class="modal fade modal-large" id="installssl">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">{$lang.close}</span></button>
                <h4 class="modal-title">{$lang.crt_install}</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.fields.select_key_domain}</label>
                        <div class="col-sm-9" id="installssl-domain">
                            <div class="form-fluid">
                                <div class="fluid-100">
                                    <select name="domain" class="form-control">
                                        <option value="">{$lang.fields.select_domain}</option>
                                        {foreach from=$listKeys item=value}  
                                            <option value="{$value->host}">{$value->host}</option>
                                        {/foreach}
                                    </select>
                                </div>
                                <div class="fluid-0">
                                    <a class="btn btn-default ml-10" data-act="installssl_data" data-formid="installssl-domain" data-dont-close-modal="1">{$lang.autofill_by_domain}</a>
                                </div>
                            </div>    
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.certificate_crt}</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" rows="6" name="crt" required  data-error="{$validator_lang.field_required}" ></textarea>
                        </div>
                        <div class="col-md-9 col-md-offset-3">
                            <div class="help-block with-errors"></div>
                        </div>                              
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.private_key_key}</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" rows="6" name="key" required  data-error="{$validator_lang.field_required}" ></textarea>
                        </div>
                        <div class="col-md-9 col-md-offset-3">
                            <div class="help-block with-errors"></div>
                        </div>                              
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.certificate_authority_bundle_cabundle}</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" rows="6" name="cabundle"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{$lang.close}</button>
                <button type="button" class="btn btn-primary" data-act="installssl" data-validate="1">{$lang.install_ssl}</button>
            </div>
        </div>
    </div>
</div>

{literal}
    <script>
        function setInstallSSLData(data) {
            jQuery('#installssl textarea[name="crt"]').val(data.crt);
            jQuery('#installssl textarea[name="key"]').val(data.key);
        }

        (function (jQuery) {
            refreshTable('csr-keys-table');
            refreshTable('private-keys-table');
            refreshTable('ssl-certificates-table');

        })(jQuery);
    </script>
{/literal}