<div class="module-header ftp-header">
    <i class="icon-header icon-subdomains"></i>
    <h1>{$lang.main_header}</h1>
    <p>{$lang.main_hint}</p>
</div>
<div class="module-body">
    <div class="section">
        <div style="display: inline-flex;"><h4>{$lang.add_header}</h4>{if $template_six_style}
            <button class="show-sm btn btn-icon show-hint" data-toggle="tooltip" title="{$lang.main_hint}" type="button"> 
            <i class="glyphicon glyphicon-question-sign"></i> 
            </button>{/if}</div>
        <div class="well">
            <form class="form-horizontal" id="subdomain-form">
                <div class="form-group">
                    <label class="control-label col-sm-3">{$lang.subdomain_label}</label>
                    <div class="col-sm-9">
                        <div class="input-group">
                            <input class="form-control first" type="text" name="domain" maxlength="64" id="dir-input" required pattern="{$patterns.simple_domain_tld}" data-error="{$validator_lang.invalid}">
                            <span class="input-group-addon">.</span>
                            <select class="form-control last" name="rootdomain" data-no-validate>
                                {if $domainlist|@count > 0}
                                    {foreach from=$domainlist item="s"}
                                        <option value="{$s->domain}">{$s->domain}</option>
                                    {/foreach}
                                {else}
                                    <option value="{$domain}">{$domain}</option>
                                {/if}
                            </select>
                        </div>
                    </div>
                    <div class="col-md-9 col-md-offset-3">
                        <div class="help-block with-errors"></div>
                    </div>                              
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">{$lang.root_label}</label>
                    <div class="col-sm-9">
                        <div class="form-fluid">
                            <div class="fluid-0">
                                <span>{$partition}/{$username}/</span>
                            </div>
                            <div class="fluid-100">
                                <input class="form-control" type="text" name="dir" id="rootdomain-input" required  data-error="{$validator_lang.field_required}">                                                        
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 col-md-offset-3">
                        <div class="help-block with-errors"></div>
                    </div>                                
                </div>                                      
                <div class="form-actions">
                    <a class="btn btn-success" data-act="addsubdomain" data-formid="subdomain-form" data-rtable="subdomains-table">{$lang.add_submit_text}</a>
                </div>
            </form>
        </div>
    </div>
    <div class="section">
        <div class="table-header">
            <div class="header-title">
                <h4>{$lang.search_header}</h4>
            </div>
            <div class="header-actions">
                <div class="header-search">
                    <div class="input-icon">
                        <i class="fa fa-search"></i>
                        <input class="form-control" type="text" placeholder="{$lang.search_placeholder}" data-search="subdomains-table">
                    </div>
                </div>

            </div>
        </div>
        <table class="table" id="subdomains-table">
            <thead>
                <tr>
                    <th>{$lang.edit_tblhead_domain}</th>
                    <th>{$lang.edit_tblhead_root}</th>
                    <th>{$lang.edit_tblhead_redirect}</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

{confirm_modal btn='data-act="removesubdomain" data-rtable="subdomains-table"' id="delete-subdomain-modal" title=$lang.delete_confirmation_title
        body="`$lang.delete_confirmation_body`<input type='hidden' name='domain'/>"}

<div class="modal fade" id="manage-redirection-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">{$lang.close}</span></button>
                <h4 class="modal-title">{$lang.manage_redirection}</h4>
            </div>
            <div class="modal-body">
                <input class="subdomain" type="hidden" name="sub" value="" />
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.subdomain_label}</label>
                        <div class="col-sm-9">
                            <span class="subdomain"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.redirects_to}</label>
                        <div class="col-sm-9">
                            <input class="form-control" id="pwd-modal" name="url" type="text" required pattern="{$patterns.no_white_chars}"/>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{$lang.close}</button>
                <a class="btn btn-danger" data-act="disredirectionsubdomain" data-rtable="subdomains-table" data-no-validate>{$lang.dis_redirect_sub_submit_text}</a>
                <a class="btn btn-primary" data-act="redirectionsubdomain" data-rtable="subdomains-table">{$lang.save}</a>
            </div>
        </div>
    </div>
</div>

{literal}
    <script>
        (function (jQuery) {
            refreshTable('subdomains-table');

            jQuery("#dir-input").on('keyup change', function () {
                jQuery("#rootdomain-input").val('public_html/' + jQuery(this).val());
            });
        })(jQuery);
    </script>
{/literal}