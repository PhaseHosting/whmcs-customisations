<tbody>
    {if empty($list)}
        <tr><td colspan="3" class="text-center">{$lang.modify_aforward_tbl_results_empty}</td></tr>
    {else}
        {foreach from=$list item="email"}
            <tr>
                <td class="cell-sm-12" data-label="{$lang.modify_aforward_tblhead_address}">{$email.dest}</td>
                <td class="cell-sm-12" data-label="{$lang.modify_aforward_tblhead_forward}">{$email.forward}</td>
                <td class="cell-sm-12 cell-actions" data-label="{$lang.modify_aforward_tblhead_action}">
                    <a class="btn btn-icon" data-toggle="modal" data-target="#delete-forwarder-modal" data-modal-insert=".email:{$email.uri_dest}|.emaildest:{$email.uri_forward}" data-rtable="email-domain-forwarders-table" title="{$lang.modify_aforward_tbl_delete_link}"><i class="fa fa-trash"></i></a>
                </td>
            </tr>
        {/foreach}
    {/if}
</tbody>