<div class="module-header">
    <i class="icon-header icon-emails"></i>
    <h1>{$lang.main_header}</h1>
    <p>{$lang.main_hint}</p>
</div>
<div class="module-body">
    <div class="section">
        <div style="display: inline-flex;"><h4>{$lang.add_header}</h4>{if $template_six_style}
            <button class="show-sm btn btn-icon show-hint" data-toggle="tooltip" title="{$lang.main_hint}" type="button"> 
            <i class="glyphicon glyphicon-question-sign"></i> 
            </button>{/if}</div>
        <div class="well">
            <form class="form-horizontal" id="email-form"> 
                <div class="form-group">
                    <label class="col-sm-3 control-label">{$lang.email_label}</label>
                    <div class="input-group col-sm-9">
                        <input name="email" class="form-control first" type="text" 
                               data-error="{$validator_lang.field_required}" required maxlength="50">
                        <span class="input-group-addon">@</span>
                        <select name="domain" class="form-control last" data-no-validate>
                            {if $domainlist|@count > 0}
                                {section name=s loop=$domainlist}  
                                    <option value="{$domainlist[s]->domain}">{$domainlist[s]->domain}</option>
                                {/section}
                            {else}
                                <option value="{$domain}">{$domain}</option>
                            {/if}
                        </select>
                    </div>
                    <div class="col-sm-9 col-sm-offset-3">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">{$lang.password_label}</label>
                    <div class="col-sm-9">
                        <div class="row">
                            <div class="col-sm-6">
                                <input name="password" id="password" class="form-control" type="password" 
                                       data-minlength="5" required>
                                <div class="help-block">{$validator_lang.min_5_chars}</div>
                            </div>
                            <div class="col-sm-6">

                            </div>
                        </div>    
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">{$lang.password2_label}</label>
                    <div class="col-sm-9">
                        <div class="row">
                            <div class="col-sm-6">
                                <input name="password2" id="password2" class="form-control" type="password" 
                                       data-match="#password" data-error="{$validator_lang.field_required}" data-match-error="{$validator_lang.field_not_match}" placeholder="" required>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="col-sm-6">

                            </div>
                        </div>  
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">{$lang.strength}</label>
                    <div class="col-sm-9">
                        <div class="row">
                            <div class="col-sm-6 col-xs-7">
                                <div id="pwstrengthbox" class="">{$validator_lang.very_weak}</div>
                            </div>
                            <div class="col-sm-6 col-xs-5">
                                <button id="generate_pwd" type="button" class="btn btn-sm btn-default" data-genpwd="password,password2,pwstrengthbox"><i class="fa fa-key"></i><span class="hide-sm">{$lang.generate_password}</span></button>
                            </div>
                        </div>   
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">{$lang.quota_label}</label>
                    <div class="col-sm-9">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="radio">
                                            <label>
                                                <div class="form-fluid">
                                                    <div class="fluid-0">
                                                        <input name="quota" type="radio" value="limited" data-no-validate>
                                                    </div>
                                                    <div class="fluid-100">
                                                        <input class="form-control" name="quota_value" disabled="disabled" type="number" min="1" max="4194304" value="1" required="" data-no-validate  required data-error="{$validator_lang.invalid}">
                                                    </div>
                                                    <div class="fluid-0">
                                                        <span class="ml-5">MB</span>
                                                    </div>
                                                </div> 
                                            </label>
                                        </div>    
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="radio">
                                            <label>
                                                <input name="quota" selected="selected" type="radio" value="unlimited" checked="" data-no-validate>
                                                {$lang.quota_unlimited}
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-9 col-sm-offset-3">
                        <div class="help-block with-errors"></div>
                    </div>                                            
                </div>
                <div class="form-actions">
                    <a class="btn btn-success" data-act="addemailaccount" data-formid="email-form" data-validate="1" data-rtable="emails-table">{$lang.add_submit_text}</a>
                </div>
            </form>
        </div>
    </div>
    <div class="section">
        <div class="table-header">
            <div class="header-title">
                <h4>{$lang.emails_accounts}</h4>
            </div>
            <div class="header-actions">
                <div class="header-search">
                    <div class="input-icon">
                        <i class="fa fa-search"></i>
                        <input class="form-control" type="text" placeholder="{$lang.search_placeholder}" data-search="emails-table">
                    </div>
                </div>

            </div>
        </div>
        <table class="table" id="emails-table" >
            <thead>
                <tr>
                    <th>{$lang.edit_tblhead_account}</th>
                    <th>{$lang.edit_tblhead_quota}</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

    <div class="modal fade" id="change-pwd-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">{$lang.close}</span></button>
                    <h4 class="modal-title">{$lang.chg_password_bookmark}</h4>
                </div>
                <div class="modal-body">
                    <input class="acc" type="hidden" name="acc" value="{$account}" />
                    <form class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-sm-3">{$lang.chg_account_label}</label>
                            <div class="col-sm-9">
                                <span class="acc" style="padding: 0px;"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{$lang.password_label}</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="pwd-modal" name="pwd" type="password" data-minlength="5" required/>
                                <div class="help-block">{$validator_lang.min_5_chars}</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{$lang.password2_label}</label>
                            <div class="col-sm-9">
                                <input class="form-control" name="pwdc" type="password" data-match="#pwd-modal" data-error="{$validator_lang.field_required}" data-match-error="{$validator_lang.field_not_match}" placeholder="" required/>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{$lang.close}</button>
                    <button type="button" class="btn btn-primary" data-act="chgemailsaccountpasswd">{$lang.save_changes}</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="change-quota-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">{$lang.close}</span></button>
                    <h4 class="modal-title">{$lang.chg_quota_bookmark}</h4>
                </div>
                <div class="modal-body">
                    <input class="acc" type="hidden" name="acc" value="{$account}" />
                    <form class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-sm-3">{$lang.chg_account_label}</label>
                            <div class="col-sm-9">
                                <span class="acc" style="padding: 0px;"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{$lang.edit_tblhead_quota}</label>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="radio">
                                            <label>
                                                <div class="form-fluid">
                                                    <div class="fluid-0">
                                                        <input name="quota" type="radio" value="limited" data-no-validate>
                                                    </div>
                                                    <div class="fluid-100">
                                                        <input class="form-control" name="quota_value" type="number" min="1" max="4194304" value="1" required>
                                                    </div>
                                                    <div class="fluid-0">
                                                        <span class="ml-5">MB</span>
                                                    </div>
                                                </div> 
                                            </label>
                                        </div>    
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="radio">
                                            <label>
                                                <input name="quota" type="radio" value="unlimited" data-no-validate>
                                                {$lang.quota_unlimited}
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>    
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{$lang.close}</button>
                    <button type="button" class="btn btn-primary" data-act="chgemailsaccountquota" data-rtable="emails-table">{$lang.save_changes}</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="delete-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">{$lang.close}</span></button>
                    <h4 class="modal-title">{$lang.delete_confirmation}</h4>
                </div>
                <div class="modal-body">
                    <input class="acc" type="hidden" name="acc" value="{$account}" />
                    <p>{$lang.remove_confirmation}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{$lang.close}</button>
                    <button type="button" class="btn btn-danger" data-act="deleteemailaccount" data-rtable="emails-table">{$lang.confirm}</button>
                </div>
            </div>
        </div>
    </div>
</div>

{literal}
    <script>
        refreshTable('emails-table');

        (function (jQuery) {
            jQuery(document).delegate('[name="quota"]', 'change', function() {
                if(jQuery(this).val() == 'unlimited'){
                    jQuery(this).parents('.form-group').first().find('[name="quota_value"]').first().attr('data-no-validate','');
                }else{
                    jQuery(this).parents('.form-group').first().find('[name="quota_value"]').first().removeAttr('data-no-validate').trigger('change');
                }
                if(jQuery(this).val() == 'unlimited'){
                    jQuery(this).parents('.form-group').first().find('[name="quota_value"]').first().attr('disabled','disabled');
                }else{
                    jQuery(this).parents('.form-group').first().find('[name="quota_value"]').first().removeAttr('disabled');
                }                
            });
            
            jQuery(document).delegate('[data-target="#change-quota-modal"]', 'click', function () {
                var quota = jQuery(this).data('quota');
                if (quota == 'unlimited') {
                    jQuery('#change-quota-modal input[value=limited]').prop('checked', false);
                    jQuery('#change-quota-modal input[value=unlimited]').prop('checked', true);
                    jQuery('#change-quota-modal input[name=quota_value]').val(1).attr('data-no-validate', '');
                    jQuery('#change-quota-modal input[name=quota_value]').attr('disabled','disabled');
                } else {
                    jQuery('#change-quota-modal input[value=unlimited]').prop('checked', false);
                    jQuery('#change-quota-modal input[value=limited]').prop('checked', true);
                    jQuery('#change-quota-modal input[name=quota_value]').val(quota).removeAttr('data-no-validate');
                }
            });
        })(jQuery);
    </script>
{/literal}
