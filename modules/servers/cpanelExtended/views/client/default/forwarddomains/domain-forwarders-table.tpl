<tbody>
{if empty($aforwardslist)}
    <tr><td colspan="7" class="text-center">{$lang.modify_dforward_tbl_results_empty}</td></tr> 
{else}
    {section name=f loop=$aforwardslist}  
        <tr>
            <td class="cell-sm-12" data-label="{$lang.modify_dforward_tblhead_address}">{$aforwardslist[f]->domain}</td>
            <td class="cell-sm-12" data-label="{$lang.modify_dforward_tblhead_directory}">{$aforwardslist[f]->source}</td>
            <td class="cell-sm-12" data-label="{$lang.modify_dforward_tblhead_forward}">{$aforwardslist[f]->destination}</td>
            <td class="cell-sm-12 no-wrap" data-label="{$lang.modify_dforward_tblhead_type}">{$aforwardslist[f]->type}</td>
            <td class="cell-sm-12 text-center-sm" data-label="{$lang.modify_dforward_tblhead_matchwww}">{if $aforwardslist[f]->matchwww == 1}<i class="fa fa-check"></i>{/if}</td>
            <td class="cell-sm-12 text-center-sm" data-label="{$lang.modify_dforward_tblhead_wildcard}">{if $aforwardslist[f]->wildcard == 1}<i class="fa fa-check"></i>{/if}</td>
            <td class="cell-sm-12 cell-actions" data-label="{$lang.modify_dforward_tblhead_action}">
                <a class="btn btn-icon" data-toggle="modal" data-target="#delete-domain-forward-modal" title="{$lang.modify_dforward_tbl_delete_link}"
                   data-modal-insert="[name=domain]:{$aforwardslist[f]->domain|urlencode}|[name=src]:{$aforwardslist[f]->source|rawurlencode}|[name=docroot]:{$aforwardslist[f]->docroot|rawurlencode}"><i class="fa fa-trash"></i></a>
            </td>
        </tr>

    {/section}
{/if}        
</tbody>