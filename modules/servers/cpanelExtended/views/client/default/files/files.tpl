<div class="module-header">
    <i class="icon-header icon-files"></i>
    <h1>{$lang.main_header}</h1>
    <p>{$lang.main_hint}</p>
</div>
{if $template_six_style}    
    <div style="display: inline-flex;"><h4>{$lang.main_header}</h4>
        <button class="show-sm btn btn-icon show-hint" data-toggle="tooltip" title="{$lang.main_hint}" type="button"> 
            <i class="glyphicon glyphicon-question-sign"></i> 
        </button></div> <br /><br />{/if}
<div class="module-body">
    <div class="section">
        <div class="file-manager">
            <div class="pull-left">
                <a class="btn btn-default action-upload" data-modal="#modal-upload" data-filereq="0"><i class="fa fa-upload"></i>{$lang.upload}</a>
                <a class="btn btn-default action-create_dir" data-modal="#modal-create_dir" data-filereq="0"><i class="fa fa-folder-o"></i>{$lang.create_dir}</a>
            </div>
            <div class="dropdown dropdown-mobile pull-left">
                <a href="#" class="btn btn-default show-md" data-toggle="dropdown"><i class="fa fa-cogs"></i>{$lang.with_selected}<span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a class="btn btn-default action-zip" data-modal="#modal-zip" data-filereq="1"><i class="fa fa-file-archive-o"></i>{$lang.zip}</a></li>
                    <li><a class="btn btn-default action-unzip" data-modal="#modal-unzip" data-filereq="1"><i class="fa fa-file-archive-o"></i>{$lang.unzip}</a></li>
                    <li><a class="btn btn-default action-copy" data-modal="#modal-copy" data-filereq="1"><i class="fa fa-files-o"></i>{$lang.copy}</a></li>
                    <li><a class="btn btn-default action-move" data-modal="#modal-move" data-filereq="1"><i class="fa fa-arrows-alt"></i>{$lang.move}</a></li>
                    <li><a class="btn btn-default action-delete" data-modal="#modal-delete" data-filereq="1"><i class="fa fa-trash-o"></i>{$lang.delete}</a></li>
                    <li><a class="btn btn-default action-chmod" data-modal="#modal-chmod" data-filereq="1"><i class="fa fa-key"></i>{$lang.chmod}</a></li>
                </ul>
            </div>
        </div>
        <div class="clearfix mb-20"></div>
        <div class="table-header">
            <div class="header-title">
                <h4>{$lang.your_files}</h4>
            </div>
            <div class="header-actions">
                <div class="header-search">
                    <div class="input-icon">
                        <i class="fa fa-search"></i>
                        <input class="form-control" type="text" placeholder="{$lang.search_placeholder}" data-search="files-table-main">
                    </div>
                </div>

            </div>
        </div>
          <div class="breadcrumb">
            <i class="fa fa-folder-o"></i>
            {foreach from=$path item="p"}
                <i class="fa fa-angle-right"></i>
                <a href="clientarea.php?action=productdetails&id={$_params.serviceid}&modop=custom&a=management&page=files&dir={$p.path}">{$p.name}</a>
            {/foreach}
        </div>
        <table class="table table-checkbox" id="files-table-main">
            <thead>
                <tr>
                    <th><input type="checkbox" id="select-all-files" /></th>
                    <th>{$lang.file_name}</th>
                    <th>{$lang.file_size}</th>
                    <th>{$lang.file_modified}</th>
                    <th>{$lang.chmod}</th>
                    <th></th>
                </tr>
            </thead>
            <tbody id="files-table">
                {if $files && !$files->isEmpty()}
                    {foreach from=$files item="file"}
                        <tr>
                            <td class="cell-checkbox"><input type="checkbox" name="files[]" value="{$file->getName()}"</td>
                            <td data-label="File Name" class="cell-sm-12">
                                {if $file->isDir()}
                                <a href="clientarea.php?action=productdetails&id={$_params.serviceid}&modop=custom&a=management&page=files&dir={$dir|rawurlencode}%2F{$file->getName()|rawurlencode}">
                                    <i class="fa fa-folder-o mr-5"></i>&nbsp;{$file->getName()}
                                </a>
                                {else}
                                    <i class="fa fa-file-o mr-5"></i><a target="_blank" href="clientarea.php?action=productdetails&id={$_params.serviceid}&modop=custom&a=management&page=files&file={$dir|rawurlencode}%2F{$file->getName()|rawurlencode}&act=download">&nbsp;{$file->getName()}</a>
                                {/if}
                            </td>
                            <td data-label="File Size" class="cell-sm-12">{if !$file->isDir()}{$file->getHumanSize()}{/if}</td>
                            <td data-label="Modified" class="cell-sm-12">{$file->getModificationDate()}</td>
                            <td data-label="File Permissions" class="cell-sm-12">{$file->getPermissionsValue()}</td>
                            <td data-label="Actions" class="cell-sm-12 cell-actions">
                                <div class="dropdown">
                                    <a href="#" class="btn btn-icon" data-toggle="dropdown"><i class="fa fa-cogs"></i><span class="caret"></span></a>
                                    <ul class="dropdown-menu dropdown-right">
                                        <li><a href="#" data-modal="#modal-zip" data-filereq="1"><i class="fa fa-file-archive-o"></i>{$lang.zip}</a></li>
                                        <li><a href="#" data-modal="#modal-unzip" data-filereq="1"><i class="fa fa-file-archive-o"></i>{$lang.unzip}</a></li>
                                        <li><a href="#" data-modal="#modal-copy" data-filereq="1"><i class="fa fa-files-o"></i>{$lang.copy}</a></li>
                                        <li><a href="#" data-modal="#modal-move" data-filereq="1"><i class="fa fa-arrows-alt"></i>{$lang.move}</a></li>
                                        <li><a href="#" data-modal="#modal-delete" data-filereq="1"><i class="fa fa-trash-o"></i>{$lang.delete}</a></li>
                                        <li>
                                            <a  href="#"class="action-rename" data-modal="#modal-rename" data-file="{$dir}/{$file->getName()}" data-filename="{$file->getName()}">
                                                <i class="fa fa-file-text-o"></i>{$lang.rename}
                                            </a>
                                        </li>
                                        <li><a href="#"data-modal="#modal-chmod" data-filereq="1"><i class="fa fa-key"></i>{$lang.chmod}</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    {/foreach}
                {else}
                    <tr>
                        <td colspan="6" class="text-center">{$lang.no_files}</td>
                    </tr>
                {/if}
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="modal-upload">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{$lang.upload}</h4>
            </div>
            <form class="form-horizontal" enctype="multipart/form-data" action="clientarea.php?action=productdetails&id={$_params.serviceid}&modop=custom&a=management&page=files&dir={$dir|rawurlencode}" method="post">
                <div class="modal-body">
                    <input type="hidden" name="act" value="upload">
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.select_file}</label>
                        <div class="col-sm-9">
                            <input type="file" name="file" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.upload_path}</label>
                        <div class="col-sm-9 input-group">                    
                            <span class="input-group-addon first">home</span>
                            <input type="text" class="form-control last" name="path" value="{$dir}">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{$lang.cancel}</button>
                    <input class="btn btn-primary" type="submit" value="{$lang.upload}" />
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-create_dir">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{$lang.create_dir}</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <input type="hidden" name="perm" value="0755" />
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.dir_name}</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="name" required data-error="{$validator_lang.field_required}">
                        </div>
                        <div class="col-md-9 col-md-offset-3">
                            <div class="help-block with-errors"></div>
                        </div>                          
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.create_in}</label>
                        <div class="col-sm-9 input-group">                    
                            <span class="input-group-addon first">home</span>
                            <input type="text" class="form-control last" name="path" value="{$dir}">
                        </div>
                    </div>
                </form>          
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{$lang.cancel}</button>
                <a class="btn btn-primary" data-act="create_dir" data-refresh="1">{$lang.create}</a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<div id="modal-zip" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">{$lang.zip}</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.compression_type}</label>
                        <div class="col-sm-9">
                            <select name="metadata" class="form-control">
                                <option value="zip">zip</option>
                                <option value="tar.gz">tar.gz</option>
                                <option value="tar.bz2">tar.bz2</option>
                                <option value="tar">tar</option>
                                <option value="gz">gz</option>
                                <option value="bz2">bz2</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.destination_file}</label>
                        <div class="col-sm-9 input-group">                    
                            <span class="input-group-addon first">home</span>
                            <input type="text" class="form-control last" name="name" value="{$dir}/filename" required>
                        </div>
                    </div>
                </form>            
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{$lang.cancel}</button>
                <a class="btn btn-primary" data-act="zip" data-refresh="1" data-formid="files-table" data-with-modal data-raw="1">{$lang.compress}</a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<div id="modal-filereq" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">{$lang.file_req_title}</h4>
            </div>
            <div class="modal-body">
                {$lang.file_req}
            </div>
            <div class="modal-footer">
                <a class="btn btn-primary" data-dismiss="modal" aria-hidden="true">{$lang.ok}</a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>    

<div id="modal-delete" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">{$lang.delete_confirmation}</h4>
            </div>
            <div class="modal-body">
                {$lang.are_you_sure_delete}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{$lang.close}</button>
                <a class="btn btn-danger" data-act="delete" data-refresh="1" data-formid="files-table" data-raw="1">{$lang.confirm}</a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<div id="modal-move" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">{$lang.move}</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger">{$lang.replace_warning_text}</div>
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.move_to}</label>
                        <div class="col-sm-9 input-group">
                            <span class="input-group-addon first">home</span>                                                        
                            <input type="text" class="form-control last" name="move_to" value="{$dir}">
                        </div>
                    </div>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{$lang.cancel}</button>
                <a class="btn btn-primary" data-act="move" data-refresh="1" data-formid="files-table" data-with-modal data-raw="1">{$lang.move}</a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<div id="modal-copy" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">{$lang.copy}</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger">{$lang.replace_warning_text}</div>
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.copy_to}</label>
                        <div class="col-sm-9 input-group">
                            <span class="input-group-addon first">home</span>
                            <input type="text" class="form-control last" name="copy_to" value="{$dir}">
                        </div>
                    </div>
                </form>      
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{$lang.cancel}</button>
                <a class="btn btn-primary" data-act="copy" data-refresh="1" data-formid="files-table" data-with-modal data-raw="1">{$lang.copy}</a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<div id="modal-unzip" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">{$lang.unzip}</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger">{$lang.replace_warning_text}</div>
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.extract_to}</label>
                        <div class="col-sm-9 input-group">
                            <span class="input-group-addon first">home</span> 
                            <input type="text" class="form-control last" name="unzip_to" value="{$dir}">
                        </div>
                    </div>
                </form>           
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{$lang.cancel}</button>
                <a class="btn btn-primary" data-act="unzip" data-refresh="1" data-formid="files-table" data-with-modal data-raw="1">{$lang.unzip}</a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<div id="modal-chmod" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">{$lang.change_permissions}</h4>
            </div>
            <div class="modal-body">                                            
                <div class="row">
                    <div class="col-xs-2 text-right"></div>
                    <div class="col-xs-3 text-center"><label>{$lang.owner}</label></div>
                    <div class="col-xs-3 text-center"><label>{$lang.group}</label></div>
                    <div class="col-xs-3 text-center"><label>{$lang.others}</label></div>
                </div>
                <div class="row">
                    <div class="col-xs-2 text-right"><label>{$lang.read}</label></div>
                    <div class="col-xs-3 text-center"><input type="checkbox" value="100" checked=""></div>
                    <div class="col-xs-3 text-center"><input type="checkbox" value="10" checked=""></div>
                    <div class="col-xs-3 text-center"><input type="checkbox" value="1" checked=""></div>
                </div>
                <div class="row">
                    <div class="col-xs-2 text-right"><label>{$lang.write}</label></div>
                    <div class="col-xs-3 text-center"><input type="checkbox" value="200" checked=""></div>
                    <div class="col-xs-3 text-center"><input type="checkbox" value="20" ></div>
                    <div class="col-xs-3 text-center"><input type="checkbox" value="2" ></div>
                </div>
                <div class="row">
                    <div class="col-xs-2 text-right"><label>{$lang.execute}</label></div>
                    <div class="col-xs-3 text-center"><input type="checkbox" value="400" checked=""></div>
                    <div class="col-xs-3 text-center"><input type="checkbox" value="40" checked=""></div>
                    <div class="col-xs-3 text-center"><input type="checkbox" value="4" checked=""></div>
                </div>
                <input type="hidden" name="chmod" value="0755" />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{$lang.cancel}</button>
                <a class="btn btn-primary" data-act="chmod" data-refresh="1" data-formid="files-table" data-with-modal data-raw="1">{$lang.change}</a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<div id="modal-rename" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">{$lang.rename}</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <input type="hidden" name="file" value="" />
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.rename_to}</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="new_name" value="{$dir}" required data-error="{$validator_lang.field_required}">
                        </div>
                        <div class="col-md-9 col-md-offset-3">
                            <div class="help-block with-errors"></div>
                        </div>                          
                    </div>
                </form>            
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{$lang.cancel}</button>
                <a class="btn btn-primary" data-act="rename" data-refresh="1">{$lang.rename}</a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
                
{literal}
    <script>
        jQuery(document).ready(function () {
            jQuery('#files-table-main th:not(:last-child)').each( function() {
                //jQuery(this).width(jQuery(this).width());
            });
                
            jQuery(document).delegate("#select-all-files", 'change', function () {
                if (jQuery(this).is(':checked')) {
                    jQuery('#files-table :checkbox').prop('checked', true);
                } else {
                    jQuery('#files-table :checkbox').prop('checked', false);
                }
            });

            jQuery(document).delegate("#files-table [data-modal]", 'click', function () {
                var $checkbox = jQuery(this).parents('tr').first().find(':checkbox').first();
                $checkbox.prop('checked', true);
                jQuery('#files-table :checkbox').not($checkbox).prop('checked', false);
            });
            
            jQuery(document).delegate(".action-rename", 'click', function () {
                var file = jQuery(this).data('file');
                var filename = jQuery(this).data('filename');
                jQuery("#modal-rename input[name=file]").val(file);
                jQuery("#modal-rename input[name=new_name]").val(filename);
            });

            jQuery(document).delegate("[data-modal]", 'click', function (e) {
                var modal = 0;
                if (jQuery(this).data('filereq') == 1 && !isAnyFileSelected())
                    modal = '#modal-filereq';
                else
                    modal = jQuery(this).data('modal');

                jQuery(modal).modal('show');
                //$modal.modal('show');
                e.preventDefault();
            });

            jQuery(document).delegate('[data-modal="#modal-zip"]', 'click', function (e) {
                var val = jQuery('#modal-zip input[name="name"]').val();
                var val = val.substring(0, val.lastIndexOf('/'));

                jQuery('#modal-zip input[name="name"]').val(val + '/' + jQuery('#files-table :checkbox:checked').first().val());
            });
            
            jQuery(document).delegate("#modal-chmod :checkbox", 'change', function () {
                var val = 0;
                jQuery('#modal-chmod :checkbox').each(function () {
                    if (jQuery(this).is(':checked')) {
                        val += parseInt(jQuery(this).val());
                    }
                });

                var pad = "0000";
                var n = val.toString();
                var result = (pad + n).slice(-pad.length);
                jQuery('#modal-chmod input[name=chmod]').val(result);
            });

            function isAnyFileSelected() {
                return jQuery('#files-table :checkbox:checked').length > 0;
            }

        })

    </script>
{/literal}
