<div class="module-header">
    <i class="icon-header icon-bans"></i>
    <h1>{$lang.main_header}</h1>
    <p>{$lang.main_hint}</p>
</div>
{if $template_six_style}    
    <div style="display: inline-flex;"><h4>{$lang.main_header}</h4>
        <button class="show-sm btn btn-icon show-hint" data-toggle="tooltip" title="{$lang.main_hint}" type="button"> 
            <i class="glyphicon glyphicon-question-sign"></i> 
        </button></div> <br /><br />{/if}
<div class="module-body">
    <div class="section">
        <div class="well">
            <form class="form-horizontal" id="unban-form">
                <div class="form-group">
                    <label class="control-label col-sm-3">{$lang.unban_ip_address}</label>
                    <div class="col-sm-9">
                        <div class="row">
                            <div class="col-sm-6">
                                <input class="form-control" type="text" name="queryip" value="{$ip}" required pattern="{$patterns.ip}">
                                <div class="help-block">{$lang.ipv4_or_ipv6}</div>
                            </div>
                            <div class="col-sm-6">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <a class="btn btn-success" data-act="unban" data-formid="unban-form" data-validate="1">{$lang.unban_submit}</a>
                </div>
            </form>
        </div>
    </div>
</div>