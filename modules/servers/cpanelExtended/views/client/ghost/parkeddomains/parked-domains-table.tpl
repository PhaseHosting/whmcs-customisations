<tbody>
{if empty($domainlist)}
    <tr><td colspan="4" class="text-center">{$lang.modify_parkeddomain_tbl_results_empty}</td></tr> 
{else}
    {section name=d loop=$domainlist}  
        <tr>
            <td class="cell-sm-12" data-label="{$lang.modify_parkeddomain_tblhead_domain}">{$domainlist[d]->domain}</td>
            <td class="cell-sm-12" data-label="{$lang.modify_parkeddomain_tblhead_droot}">{$domainlist[d]->dir}</td>
            {if $domainlist[d]->status == 'not redirected'}
                <td class="cell-sm-12" data-label="{$lang.modify_parkeddomain_tblhead_redirect}">{$domainlist[d]->status}</td>
            {else}
                <td class="cell-sm-12" data-label="{$lang.modify_parkeddomain_tblhead_redirect}"><a href="{$domainlist[d]->status}">{$domainlist[d]->status}</a></td>
            {/if}
            <td class="cell-sm-12 cell-actions" data-label="{$lang.modify_parkeddomain_tblhead_action}">
                {if $manageforwarddomains}<a class="btn btn-icon" data-toggle="modal" data-target="#manage-redirection-modal" title="{$lang.modify_parkeddomain_tbl_redirect_link}"
                   data-modal-insert=".domain:{$domainlist[d]->domain|urlencode}|[name=url]:{if $domainlist[d]->status != 'not redirected'}{$domainlist[d]->status|urlencode}{else}http%3A%2F%2F{/if}"><i class="fa fa-reply"></i></a>{/if}
                <a class="btn btn-icon" data-toggle="modal" data-target="#delete-parked-domain-modal" data-modal-insert="[name=domain]:{$domainlist[d]->domain|urlencode}" title="{$lang.modify_parkeddomain_tbl_remove_link}"><i class="fa fa-trash"></i></a>
            </td>
        </tr>	
    {/section}
{/if}           
</tbody>