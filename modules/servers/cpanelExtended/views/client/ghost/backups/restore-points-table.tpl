<tbody>
    {if empty($list)}
        <tr><td colspan="5" class="text-center">{$lang.no_restore_point}</td></tr>
    {else}
        {foreach from=$list item="val" name="loop"}
        <tr id="restorepoint{$smarty.foreach.loop.index}">
            <td data-label="{$lang.restore_point}" class="cell-sm-12">{$val}<input type="hidden" name="rspointf" id="rspointf" value="{$val}"/></td>
            <td data-label="{$lang.subdomains}" class="cell-sm-12"><input type="checkbox" name="subdomains" value="1"></td>
            <td data-label="{$lang.mail}" class="cell-sm-12"><input type="checkbox" name="mailconfig" value="1"></td>
            <td data-label="{$lang.mysql}" class="cell-sm-12"><input type="checkbox" name="mysql" value="1"></td>
            <td data-label="{$lang.actions}" class="cell-sm-12 cell-actions"><a class="btn btn-icon" data-act="rebackups" data-formid="restorepoint{$smarty.foreach.loop.index}" data-rtable="history-table">{$lang.restore}</a></td>     
        </tr>
        {/foreach}
    {/if}
</tbody>