{literal}
    <script>
        function callback(output) {
            jQuery("#output_mxs").html(output.table).fadeIn();
            jQuery('#lspinner').hide();
            jQuery("#domain_select").removeAttr('disabled');
        }
    
        jQuery(document).ready(function() {
            jQuery("#domain_select").change(function() {
                if(jQuery(this).val() == 'select')
                    return ;
                
                jQuery("#output_mxs").fadeOut();
                
                jQuery(this).attr('disabled','');
                jQuery('#lspinner').show();
                
                var act = jQuery('option:selected',this).first().data("act");
                if(typeof act != 'undefined') {
                    doAction(jQuery('option:selected',this).first());
                }
            });
        });
    </script>
    
{/literal}

<!-- {if $template_six_style}    
    <div style="display: inline-flex;"><h4>{$lang.main_header}</h4>
        <button class="show-sm btn btn-icon show-hint" data-toggle="tooltip" title="{$lang.main_hint}" type="button"> 
            <i class="glyphicon glyphicon-question-sign"></i> 
        </button></div> <br /><br />{/if} -->
            <div class="panel">

                <div class="panel-header">
                    <h3>{$lang.main_header} - {$lang.select_your_domain}</h3>
                </div>
                <div class="panel-content">
                    <div class="row">
                        <div class="col-sm-12">
                            <select id="domain_select" class="select2-container form-control form-white">
                                <option value="select">{$lang.please_select_a_domain}</option>
                                {foreach from=$domains item="domain"}
                                <option data-act="data" data-query="domain={$domain->domain|urlencode}" data-noloader="1">{$domain->domain}</option>
                                {/foreach}
                            </select>
                            
                        </div>
                    </div>

                </div>
            </div>

            <center><i id="lspinner" class="fa fa-spinner fa-pulse" style="display: none;"></i></center>
            <div id="output_mxs"></div>

        </div>


<div class="modal fade" id="edit-mx-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">{$lang.close}</span></button>
                <h4 class="modal-title">{$lang.edit_mx}</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <input type="hidden" name="domain" />
                    <input class="priority" type="hidden" name="oldpriority" />
                    <input class="mx" type="hidden" name="oldmx" />
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.priority}</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-sm-12">
                                    <input class="form-control form-white priority" name="priority" type="number" min="0" max="999" required data-error="{$validator_lang.invalid}">
                                </div>
                                
                            </div>
                        </div>
                        <div class="col-md-9 col-md-offset-3">
                            <div class="help-block with-errors"></div>
                        </div>                                  
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">{$lang.destination}</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-sm-12">
                                    <input class="form-control form-white mx" type="text" name="mx" required pattern="{$patterns.simple_domain}" data-error="{$validator_lang.invalid}">
                                </div>
                                
                            </div>    
                        </div>
                        <div class="col-md-9 col-md-offset-3">
                            <div class="help-block with-errors"></div>
                        </div>                                  
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{$lang.close}</button>
                <button type="button" class="btn btn-primary" data-act="edit">{$lang.save_changes}</button>
            </div>
        </div>
    </div>
</div>                      
                        
{confirm_modal btn='data-act="remove"' id="delete-entry-modal" title=$lang.delete_confirmation
        body="`$lang.remove_confirm_text`<input type='hidden' name='domain'/><input type='hidden' name='exchange'/><input type='hidden' name='preference'/>"}
        
