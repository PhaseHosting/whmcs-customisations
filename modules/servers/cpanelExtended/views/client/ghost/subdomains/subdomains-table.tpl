<tbody>	
{if empty($subdomainlist)}
    <tr><td colspan="5" class="text-center">{$lang.search_results_empty}</td></tr> 
{else}
    {section name=s loop=$subdomainlist}  
        <tr>
            <td data-label="{$lang.edit_tblhead_domain}" data-label="" class="cell-sm-12">{$subdomainlist[s]->domain}</td>
            <td data-label="{$lang.edit_tblhead_root}" class="cell-sm-12">{$subdomainlist[s]->dir}</td>
         {if $subdomainlist[s]->status == 'not redirected'}
            <td data-label="{$lang.edit_tblhead_redirect}" class="cell-sm-12">{$lang.not_redirected_text}</td>
         {else}
             <td data-label="{$lang.edit_tblhead_redirect}" class="cell-sm-12"><a href="{$subdomainlist[s]->status}" target="_blank">{$subdomainlist[s]->status}</a></td>
         {/if}
            <td data-label="{$lang.edit_tblhead_actions}" class="cell-sm-12 cell-actions">
                {if $manageforwarddomains}<a class="btn btn-icon" data-toggle="modal" data-target="#manage-redirection-modal" title="{$lang.redirect_sub_submit_text}"
                   data-modal-insert=".subdomain:{$subdomainlist[s]->domain|urlencode}|[name=url]:{if $subdomainlist[s]->status != 'not redirected'}{$subdomainlist[s]->status|urlencode}{else}http%3A%2F%2F{/if}"><i class="fa fa-reply"></i></a>{/if}
                <a class="btn btn-icon" data-toggle="modal" data-target="#delete-subdomain-modal" data-modal-insert="[name=domain]:{$subdomainlist[s]->domain|urlencode}" title="{$lang.remove_bookmark}"><i class="fa fa-trash"></i></a>
            </td>
        </tr>	
    {/section}
{/if}        
</tbody>