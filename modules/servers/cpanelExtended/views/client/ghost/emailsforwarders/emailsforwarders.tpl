<div class="module-header">
     <i class="icon-header icon-emailsforwarders"></i>
    <h1>{$lang.main_header}</h1>
    <p>{$lang.main_hint}</p>
</div>
<div class="module-body">
    <div class="section">
        <div style="display: inline-flex;"><h4>{$lang.add_aforward_sub_main_header}</h4>{if $template_six_style}
            <button class="show-sm btn btn-icon show-hint" data-toggle="tooltip" title="{$lang.main_hint}" type="button"> 
            <i class="glyphicon glyphicon-question-sign"></i> 
            </button>{/if}</div>        
        <div class="well">
            <form class="form-horizontal" id="add-forwarder-form">
                <div class="form-group">
                    <label class="control-label col-md-3">{$lang.add_aforward_label}</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <input class="form-control first" type="text" name="email" data-error="{$validator_lang.field_required}" required>
                            <span class="input-group-addon">@</span>
                            <select class="form-control last" name="domain" data-no-validate>
                                {if $domainlist|@count > 0}
                                    {section name=s loop=$domainlist}  
                                        <option value="{$domainlist[s]->domain}">{$domainlist[s]->domain}</option>
                                    {/section}
                                {else}
                                    <option value="{$domain}">{$domain}</option>
                                {/if}
                            </select>
                        </div>
                    </div>
                    <div class="col-md-9 col-md-offset-3">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3">{$lang.add_aforward_dest_header}</label>
                    <div class="col-md-9">
                        <div class="form-fluid-sm">
                            <div class="radio fluid-0">                                                        
                                <label>
                                    <input name="fwdopt" type="radio" value="fwd" checked data-no-validate><span>{$lang.add_aforward_dest_email}</span>
                                </label>  
                                <div class="help-block"></div>
                            </div>
                            <div class="fluid-100">
                                <input class="form-control" type="email" name="fwdemail" data-error="{$validator_lang.invalidemail}" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 col-md-offset-3">
                        <div class="help-block with-errors"></div>
                    </div>                                
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3"></label>
                    <div class="col-md-9">
                        <div class="form-fluid-sm">
                            <div class="radio fluid-0">                                                        
                                <label>
                                    <input name="fwdopt" type="radio" value="fail" data-no-validate><span>{$lang.add_aforward_dest_discard1}</span>
                                </label>  
                                <div class="help-block"></div>
                            </div>
                            <div class="fluid-100">
                                <input class="form-control" type="text" name="failmsgs" title="{$lang.add_aforward_dest_discard2}" data-error="{$validator_lang.field_required}" >
                            </div>
                        </div> 
                    </div>
                    <div class="col-md-9 col-md-offset-3">
                        <div class="help-block with-errors"></div>
                    </div>                             
                </div>
                            
                <div class="advanced-options collapse">
                    <div class="form-group">
                        <label class="control-label col-md-3"></label>
                        <div class="col-md-9">
                            <div class="form-fluid-sm">
                                <div class="radio fluid-0">                                                        
                                    <label>
                                        <input name="fwdopt" type="radio" value="system" data-no-validate><span>{$lang.add_aforward_advanced_system}</span>
                                    </label>  
                                    <div class="help-block"></div>
                                </div>
                                <div class="fluid-100">
                                    <input class="form-control" type="text" value="{$username}" name="fwdsystem">
                                </div>
                            </div> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3"></label>
                        <div class="col-md-9">
                            <div class="form-fluid-sm">
                                <div class="radio fluid-0">                                                        
                                    <label>
                                        <input name="fwdopt" type="radio" value="pipe" data-no-validate><span>{$lang.add_aforward_advanced_pipe} /{$partition}/{$username}/</span>
                                    </label>  
                                    <div class="help-block"></div>
                                </div>
                                <div class="fluid-100">
                                    <input class="form-control" type="text" name="pipefwd" pattern="{$patterns.cpanel_dir}" data-error="{$validator_lang.invalid}" >
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9 col-md-offset-3">
                            <div class="help-block with-errors"></div>
                        </div>                                
                    </div>
                     
                    <div class="form-group"> 
                        <label class="control-label col-md-3"></label>
                        <div class="col-md-9">
                            <div class="alert alert-info" style="margin-bottom: 0px;">{$lang.add_aforward_advanced_pipe_hint}</div>
                        </div>
                    </div>    
                        
                    <div class="form-group">
                        <label class="control-label col-md-3"></label>
                        <div class="col-md-9">
                            <div class="form-fluid-sm">
                                <div class="radio fluid-0">                                                        
                                    <label>
                                        <input name="fwdopt" type="radio" value="blackhole" data-no-validate><span>{$lang.add_aforward_advanced_discard}</span>
                                    </label>  
                                    <div class="help-block"></div>
                                </div>
                                <div class="fluid-100">
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <a class="btn btn-success" data-act="addforward" data-validate="1" data-formid="add-forwarder-form" data-rtable="email-forwarders-table">{$lang.add_aforward_submit_text}</a>
                    <button class="btn btn-primary pull-right" data-target=".advanced-options" data-toggle="collapse" type="button">{$lang.add_aforward_advanced_link}</button>
                </div>
            </form>
        </div>
    </div>
    <div class="section">
        <div class="table-header">
            <div class="header-title">
                <h4>{$lang.email_forwarders}</h4>
            </div>
            <div class="header-actions">
                <div class="header-search">
                    <div class="input-icon">
                        <i class="fa fa-search"></i>
                        <input class="form-control" type="text" placeholder="{$lang.search_placeholder}" data-search="email-forwarders-table">
                    </div>
                </div>

            </div>
        </div>
        <table class="table" id="email-forwarders-table">
            <thead>
                <tr>
                    <th>{$lang.modify_aforward_tblhead_address}</th>
                    <th>{$lang.modify_aforward_tblhead_forward}</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    
                </tr>
            </tbody>
        </table>
    </div>

    <div class="module-sub-header">
        <h2>{$lang.add_dforward_main_header}</h2>
        <p>{$lang.add_dforward_hint1}</p>

    </div>
    <div class="alert alert-warning">
        {$lang.add_dforward_hint2} {$lang.add_dforward_hint3}
    </div>
    <div class="section">
        <h4>{$lang.add_dforward_sub_main_header}</h4>
        <div class="well">
            <form class="form-horizontal" id="addddomain-form">
                <div class="form-group">
                    <label class="control-label col-md-3">{$lang.add_dforward_label}</label>

                    <div class="col-md-9">
                        <div class="input-group">
                            <select name="domain" class="form-control first" data-no-validate>
                            {if $domainlist|@count > 0}
                                {section name=s loop=$domainlist}  
                                    <option value="{$domainlist[s]->domain}">{$domainlist[s]->domain}</option>
                                {/section}
                             {else}
                                 <option value="{$domain}">{$domain}</option>
                             {/if}
                            </select>
                            <span class="input-group-addon">{$lang.modify_aforward_tbl_to_text}</span>
                            <input name="destdomain" type="text" class="form-control last" required pattern="{$patterns.simple_domain}" data-error="{$validator_lang.invalid}" >
                        </div>
                        <div class="col-md-9 col-md-offset-3">
                            <div class="help-block with-errors"></div>
                        </div>                           
                    </div>
                </div>
                <div class="form-actions">
                    <a class="btn btn-success" data-act="addddomain" data-formid="addddomain-form" data-validate="1" data-rtable="email-domain-forwarders-table">{$lang.add_dforward_submit_text}</a>
                </div>
            </form>
        </div>
    </div>
    <div class="section">
        <div class="table-header">
            <div class="header-title">
                <h4>{$lang.add_dforward_main_header}</h4>
            </div>
            <div class="header-actions">
                <div class="header-search">
                    <div class="input-icon">
                        <i class="fa fa-search"></i>
                        <input class="form-control" type="text" placeholder="{$lang.search_placeholder}" data-search="email-domain-forwarders-table">
                    </div>
                </div>

            </div>
        </div>
        <table class="table" id="email-domain-forwarders-table">
            <thead>
                <tr>
                    <th>{$lang.modify_dforward_tblhead_address}</th>
                    <th>{$lang.modify_dforward_tblhead_forward}</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
                
    {confirm_modal btn='data-act="delaforward" data-rtable="email-forwarders-table"' id="delete-forwarder-modal" title=$lang.delete_confirmation_title
        body="`$lang.delete_confirmation_body`<input class='email' type='hidden' name='email'/><input class='emaildest' type='hidden' name='emaildest'/>"}            
    {confirm_modal btn='data-act="deldforward" data-rtable="email-domain-forwarders-table"' id="delete-modal" title=$lang.delete_confirmation_title
        body="`$lang.delete_confirmation_body`<input class='domain' type='hidden' name='domain'/>"}
</div>

{literal}
    <script>
        refreshTable('email-domain-forwarders-table');
        refreshTable('email-forwarders-table');
        (function (jQuery) {
            jQuery(document).delegate('#add-forwarder-form input[name="fwdopt"]', 'change', function() {
                jQuery('#add-forwarder-form input[name="fwdemail"]').removeAttr('required','').attr('data-no-validate','');
                jQuery('#add-forwarder-form input[name="failmsgs"]').removeAttr('required','').attr('data-no-validate','');
                jQuery('#add-forwarder-form input[name="fwdsystem"]').removeAttr('required','').attr('data-no-validate','');
                jQuery('#add-forwarder-form input[name="pipefwd"]').removeAttr('required','').attr('data-no-validate','');
                
                if(jQuery(this).val() == 'fwd') jQuery('#add-forwarder-form input[name="fwdemail"]').attr('required','').removeAttr('data-no-validate','');
                else if(jQuery(this).val() == 'fail') jQuery('#add-forwarder-form input[name="failmsgs"]').attr('required','').removeAttr('data-no-validate','');
                else if(jQuery(this).val() == 'system') jQuery('#add-forwarder-form input[name="fwdsystem"]').attr('required','').removeAttr('data-no-validate','');
                else if(jQuery(this).val() == 'pipe') jQuery('#add-forwarder-form input[name="pipefwd"]').attr('required','').removeAttr('data-no-validate','');
            });
        })(jQuery);
    </script>
{/literal}
