<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4>{$data->name}</h4>
            </div>
            <div class="modal-body">
                <textarea class="form-control" rows="10">{$data->key}</textarea>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal" class="btn">{$lang.close}</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->