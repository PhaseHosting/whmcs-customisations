<tbody>
{if $list}
    {foreach from=$list item="key" key="i"}
        {if $key->haspub}
            <tr>
                <td data-label="{$lang.name}" class="cell-sm-12">{$key->name}</td>
                <td data-label="{$lang.authorization_status}" class="cell-sm-12">{$key->auth}{if $key->authstatus}{$lang.authorized}{else}{$lang.not_authorized}{/if}</td>
                <td data-label="{$lang.actions}" class="cell-sm-12 cell-actions" id="public{$i}">
                    <input type="hidden" name="key" value="{$key->name}">
                    <input type="hidden" name="public" value="1">
                    
                    <button class="btn btn-icon" data-act="view" data-formid="public{$i}" title="{$lang.view}"><i class="fa fa-eye"></i></button>
                    {if $key->auth}<a class="btn btn-icon" data-toggle="modal" data-target="#deauthorize-modal" title="{$lang.deauthorize}" data-modal-insert="[name=key]:{$key->name|urlencode}"><i class="fa fa-close"></i></a>
                    {else}<a class="btn btn-icon" data-toggle="modal" data-target="#authorize-modal" title="{$lang.authorize}" data-modal-insert="[name=key]:{$key->name|urlencode}"><i class="fa fa-check"></i></a>{/if}
                    <a class="btn btn-icon" href="clientarea.php?action=productdetails&id={$_params.serviceid}&modop=custom&a=management&page=ssh&act=download&key={$key->name|urlencode}&public=1" title="{$lang.download}"><i class="fa fa-download"></i></a>
                    <a class="btn btn-icon" data-toggle="modal" data-target="#delete-public" title="{$lang.delete}" data-modal-insert="[name=key]:{$key->name|urlencode}"><i class="fa fa-trash"></i></a>
                </td>
            </tr>
        {/if}
    {/foreach}
{else}
    <tr><td colspan="3" class="text-center">{$lang.no_data_available}</td></tr>
{/if}
</tbody>