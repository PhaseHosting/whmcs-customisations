<tbody>
{if $domainlist|@count > 0}
    {section name=d loop=$domainlist}  
        <tr>
            <td data-label="{$lang.addon_domain}" class="cell-sm-12">{$domainlist[d]->domain}</td>
            <td data-label="{$lang.modify_addondomain_tblhead_document}" class="cell-sm-12">{$domainlist[d]->basedir}</td>
            <td data-label="{$lang.modify_addondomain_tblhead_username}" class="cell-sm-12">{$domainlist[d]->subdomain}@{$domainlist[d]->rootdomain}</td>
            {if $domainlist[d]->status == 'not redirected'}
                <td data-label="{$lang.modify_addondomain_tblhead_redirect}" class="cell-sm-12">{$lang.not_redirected_text}</td>
            {else}
                <td data-label="{$lang.modify_addondomain_tblhead_redirect}" class="cell-sm-12"><a href="{$domainlist[d]->status}">{$domainlist[d]->status}</a></td>
            {/if}
            <td class="cell-actions cell-sm-12" data-label="{$lang.action}">
                <a class="btn btn-icon" title="{$lang.change_document_root}" data-toggle="modal" data-target="#change-root-modal" data-modal-insert="[name=rootdomain]:{$domainlist[d]->rootdomain|urlencode}|[name=subdomain]:{$domainlist[d]->subdomain|urlencode}|[name=dir]:{$domainlist[d]->basedir|urlencode}"><i class="fa fa-home"></i></a>
                {if $manageforwarddomains}<a class="btn btn-icon" title="{$lang.manage_redirection}" data-toggle="modal" data-target="#manage-redirection-modal" data-modal-insert=".sub1:{$domainlist[d]->domain|urlencode}|.sub2:{$domainlist[d]->subdomain|urlencode}_{$domainlist[d]->rootdomain|urlencode}|[name=url]:{if $domainlist[d]->status != 'not redirected'}{$domainlist[d]->status|urlencode}{else}http%3A%2F%2F{/if}"><i class="fa fa-reply"></i></a>{/if}
                <a class="btn btn-icon" title="{$lang.delete}" data-toggle="modal" data-target="#delete-addondomain-modal" data-modal-insert=".domain:{$domainlist[d]->domain|urlencode}|.subdomain:{$domainlist[d]->subdomain|urlencode}_{$domainlist[d]->rootdomain|urlencode}|.ftp:{$domainlist[d]->subdomain|urlencode}"><i class="fa fa-trash"></i></a>            
            </td>
        </tr>	
    {/section}
{else}
    <tr><td colspan="5" class="text-center">{$lang.modify_addondomain_search_results_empty}</td></tr> 
{/if}  
</tbody>