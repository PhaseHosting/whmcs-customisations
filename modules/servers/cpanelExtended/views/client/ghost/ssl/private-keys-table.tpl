<tbody>
    {if empty($listKeys)}
        <tr>
            <td colspan="2" class="text-center">{$lang.no_keys_in_table}</td>
        </tr>
    {else}
        {foreach from=$listKeys  item=domain}
            <tr>
                <td data-label="{$lang.domain}" class="cell-sm-12">
                    {$domain->host}
                </td>
                <td data-label="{$lang.action}" class="cell-sm-12 cell-actions">
                    <button class="btn btn-icon" data-act="getKey" data-query="domain={$domain->host|urlencode}" title="{$lang.view}"><i class="fa fa-eye"></i></button>
                   <a class="btn btn-icon" data-toggle="modal" data-target="#delete-key-modal" data-modal-insert="[name=host]:{$domain->host|urlencode}" title="{$lang.delete}"><i class="fa fa-trash"></i></a>
                </td>
            </tr>
        {/foreach}
    {/if}
</tbody> 
