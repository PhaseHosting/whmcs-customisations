<div class="modal" id="myModal">
    <div class="modal-header">
        <button class="close" data-dismiss="modal">×</button>
        <h3>{$lang.import_ssh_key}</h3>
    </div>
    <div class="modal-body">
        <form class="form-horizontal">
            <fieldset>
                <div class="control-group">
                    <label class="control-label" for="input01">{$lang.key_name}</label>
                    <div class="controls">
                        <input type="text" class="input-xlarge" name="name" id="input01"
                               data-mv-validator="required" data-mv-validator-msg="{$_lang.this_field_is_not_valid}">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="input02">{$lang.key_password}</label>
                    <div class="controls">
                        <input type="password" class="input-xlarge" name="password" id="input02">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="input04">{$lang.key}</label>
                    <div class="controls">
                        <textarea name="key" class="input-xlarge" style="height: 150px"></textarea>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
    <div class="modal-footer">
        <button class="btn btn-success" data-act="create_confirm" data-refresh="1"><span>{$lang.import}</span></button>
        <button class="btn btn-default" data-dismiss="modal" class="btn">{$lang.close}</button>
    </div>
</div>