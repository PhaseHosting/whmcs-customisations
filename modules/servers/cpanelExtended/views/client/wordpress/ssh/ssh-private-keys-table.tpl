<tbody>
{if $list}
    {foreach from=$list item="key" key="i"}
        <tr>
            <td data-label="{$lang.name}" class="cell-sm-12">{$key->name}</td>
            <td data-label="{$lang.actions}" class="cell-sm-12 cell-actions" id="private{$i}">
                <input type="hidden" name="key" value="{$key->name}">
                <input type="hidden" name="public" value="0">
                <input type="hidden" name="has_public" value="{$key->haspub}">
                
                <button class="btn btn-icon" data-act="view" data-formid="private{$i}" title="{$lang.view}"><i class="fa fa-eye"></i></button>
                <button class="btn btn-icon" data-act="ppk" data-formid="private{$i}" title="{$lang.convert_to_ppk}"><i class="fa fa-key"></i></button>
                <a class="btn btn-icon" href="clientarea.php?action=productdetails&id={$_params.serviceid}&modop=custom&a=management&page=ssh&act=download&key={$key->name|urlencode}&public=0" title="{$lang.download}"><i class="fa fa-download"></i></a>
                <button class="btn btn-icon" data-toggle="modal" data-target="#delete-private" title="{$lang.delete}" data-modal-insert="[name=key]:{$key->name|urlencode}"><i class="fa fa-trash"></i></button>
            </td>
        </tr>
    {/foreach}
{else}
    <tr><td colspan="3" class="text-center" class="text-center">{$lang.no_data_available}</td></tr>
{/if}
</tbody>