<div class="panel">
    <div class="panel-header header-line">
    <h3>{$lang.add_new_mx_record}</h3>
    </div>
    <div class="panel-content">
        <div class="">
            <form class="form-horizontal" id="add-mx">
                <input type="hidden" name="domain" value="{$domain}" />
                <div class="form-group">

                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="control-label">{$lang.priority}</label>
                                <input class="form-control form-white" name="priority" type="number" min="0" max="999" required data-error="{$validator_lang.invalid}">
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="col-sm-6">
                                <label class="control-label">{$lang.destination}</label>
                                <input class="form-control form-white" type="text" name="mx" required pattern="{$patterns.simple_domain}" data-error="{$validator_lang.invalid}">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 col-md-offset-3">

                    </div>                             
                </div>
                <div class="form-actions text-right">
                    <a class="btn btn-success" data-act="add" data-formid="add-mx" data-validate="1"><span>{$lang.add}</span></a>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="panel">
    <div class="panel-header header line">
        <h3>{$lang.manage_your_records}</h3>
    </div>
    <div class="panel-content">
        <div class="header-actions">
            <div class="header-search">
                <div class="input-icon">
                    <i class="fa fa-search"></i>
                    <input class="form-control form-white" type="text" placeholder="{$lang.search_placeholder}" data-search="mx-table">
                </div>
            </div>
        </div>
        <table class="table" id="mx-table">
            <thead>
                <tr>
                    <th>{$lang.priority}</th>
                    <th>{$lang.destination}</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {if $mxs}
                {foreach from=$mxs->entries item="mx"}
                <tr>
                    <td data-label="{$lang.priority}" class="cell-sm-5">{$mx->priority}</td>
                    <td data-label="{$lang.destination}" class="cell-sm-7">{$mx->mx}</td>
                    <td data-label="{$lang.actions}" class="cell-sm-12 cell-actions">
                        <a class="btn btn-icon" data-toggle="modal" data-target="#edit-mx-modal" data-modal-insert="[name=domain]:{$domain|urlencode}|.mx:{$mx->mx|urlencode}|.priority:{$mx->priority}" title="{$lang.edit}"><i class="fa fa-pencil"></i></a>
                        <a class="btn btn-icon" data-toggle="modal" data-target="#delete-entry-modal" data-modal-insert="[name=domain]:{$domain|urlencode}|[name=exchange]:{$mx->mx|urlencode}|[name=preference]:{$mx->priority}" title="{$lang.remove}"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
                {/foreach}
                {else}
                <tr>
                    <td colspan="3" class="text-center">{$lang.there_are_no_mx_records_for} "{$domain}". </td>
                </tr>
                {/if}
            </tbody>
        </table>
    </div>
</div>