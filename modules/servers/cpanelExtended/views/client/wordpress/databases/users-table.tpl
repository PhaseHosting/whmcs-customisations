<tbody>	
{if empty($userlist)}
    <tr><td colspan="2" class="text-center">{$lang.current_users_search_empty}</td></tr> 
{else}      
    {section name=u loop=$userlist}  
         <tr>
            <td data-label="Users" class="cell-sm-12">{$userlist[u]->user}</td>
            <td data-label="Actions" class="cell-sm-12 cell-actions">
                <a class="btn btn-icon" data-toggle="modal" data-target="#delete-user-modal" data-modal-insert="[name=user]:{$userlist[u]->user|urlencode}" title="{$lang.current_users_bookmark_delete}"><i class="fa fa-trash"></i></a>
            </td>
        </tr>
    {/section}
{/if}    
</tbody>