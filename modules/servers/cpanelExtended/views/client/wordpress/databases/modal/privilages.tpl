<div class="modal fade" id="change-privilages-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">{$lang.close}</span></button>
                <h4 class="modal-title">{$lang.manage_user_privileges}</h4>
            </div>
            <div class="modal-body">
                <input class="dbname" type="hidden" name="dbname" value="{$dbname}" />
                <input class="dbuser" type="hidden" name="dbuser" value="{$dbuser}" />
                {if $remove}<input type="hidden" name="current" value="1" />{/if}
                {$lang.add_userdb_label_user} <b>{$dbuser}</b><br>
                {$lang.add_userdb_label_database} <b>{$dbname}</b>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="checkbox" style="margin-bottom: -6px;"><label><input class="select-all" type="checkbox" name="all" value="1" {if $all}checked=""{/if}>{$lang.all_privilages}</label></div>
                    </div>
                    <div class="col-sm-6">
                        {foreach from=$privilages[0] item=privilage}
                            <div class="checkbox"><label><input type="checkbox" name="privilages[]" value="{$privilage}" {if $all || in_array($privilage, $user_privilages)}checked=""{/if} {*{if $all}disabled=""{/if}*}>{$privilage}</label></div>
                        {/foreach}
                    </div>
                    <div class="col-sm-6">
                        {foreach from=$privilages[1] item=privilage}
                            <div class="checkbox"><label><input type="checkbox" name="privilages[]" value="{$privilage}" {if $all || in_array($privilage, $user_privilages)}checked=""{/if} {*{if $all}disabled=""{/if}*}>{$privilage}</label></div>
                        {/foreach}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{$lang.close}</button>
                {if $remove}
                    <button type="button" class="btn btn-danger" data-act="deluserfromdb" data-rtable="current-databases-table">{$lang.remove_user_privilages}</button>
                    <button type="button" class="btn btn-primary" data-act="setuserprivilages_confirm" data-raw="1" {if $rtable}data-rtable="current-databases-table"{/if}>{$lang.save_changes}</button>
                {else}
                    <button type="button" class="btn btn-primary" data-act="setuserprivilages_confirm" data-raw="1" {if $rtable}data-rtable="current-databases-table"{/if}>{$lang.add}</button>
                {/if}
            </div>
        </div>
    </div>
</div>
            