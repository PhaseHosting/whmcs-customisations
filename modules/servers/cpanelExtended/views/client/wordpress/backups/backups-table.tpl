<tbody>
    {if empty($list)}
        <tr><td colspan="2" class="text-center">{$lang.no_available}</td></tr>
    {else}
        {foreach from=$list item="backup"}
            <tr>
                <td data-label="{$lang.backup}" class="cell-sm-12">
                    <a alt="{$backup->file}" data-act="getfilelink" data-query="dtype=d_cpanel&filename={$backup->file|urlencode}">{$backup->file}</a>
                    <span>({$backup->localtime})</span>
                </td>
                <td data-label="{$lang.status}" class="cell-sm-12">{$backup->status}</td>
            </tr>
        {/foreach}
    {/if}
</tbody>