<div class="modal" id="myModal">
    <div class="modal-header">
        <button class="close" data-dismiss="modal">×</button>
        <h3>{$lang.ssh_key_generator}</h3>
    </div>
    <div class="modal-body">
        <form class="form-horizontal">
            <fieldset>
                <div class="control-group">
                    <label class="control-label" for="input01">{$lang.key_name}</label>
                    <div class="controls">
                        <input type="text" class="input-xlarge" name="name" id="input01"
                               data-mv-validator="required" data-mv-validator-msg="{$_lang.this_field_is_not_valid}">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="input02">{$lang.key_password}</label>
                    <div class="controls">
                        <input type="password" class="input-xlarge" name="password1" id="input02"
                               data-mv-validator="required" data-mv-validator-msg="{$_lang.this_field_is_not_valid}"
                               data-mv-minlen="5">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="input03">{$lang.key_password_again}</label>
                    <div class="controls">
                        <input type="password" class="input-xlarge" name="password2" id="input03"
                               data-mv-validator="required,same_as" data-mv-same-as="input02" data-mv-validator-msg="{$_lang.this_field_is_not_valid}" >
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="input04">{$lang.key_type}</label>
                    <div class="controls">
                        <select name="type" id="input04">
                            <option value="dsa">DSA</option>
                            <option value="rsa">RSA</option>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="input05">{$lang.key_size}</label>
                    <div class="controls">
                        <select name="size" id="input05">
                            <option value="1024">1024</option>
                            <option value="2048">2048</option>
                            <option value="2048">4096</option>
                        </select>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
    <div class="modal-footer">
        <button class="btn btn-default" data-act="create_confirm" data-refresh="1" class="btn btn-success">{$lang.create}</button>
        <button class="btn btn-default" data-dismiss="modal" class="btn">{$lang.close}</button>
    </div>
</div>