<?php
 
use WHMCS\ClientArea;
use WHMCS\Database\Capsule;

define('CLIENTAREA', true);
define('FORCESSL', true);

require __DIR__ . '/init.php';
 
$ca = new ClientArea();
 
$ca->setPageTitle('Knowledgebase');
 
$ca->addToBreadCrumb('index.php', Lang::trans('globalsystemname'));
$ca->addToBreadCrumb('knowhow.php', 'Knowledgebase');
 
$ca->initPage();
 
//$ca->requireLogin();

 
// Define the template filename

$ca->setTemplate('knowhow');
 
$ca->output();